package zincbeanstalk

import (
	"context"
	"fmt"
	"sync"
	"task/common"
	"task/contrib/conn"
	"task/contrib/helper"
	"time"

	"github.com/valyala/fasthttp"
	"github.com/valyala/fastjson"
)

var (
	clientPool    *sync.Pool
	clientTimeout = 12 * time.Second
	ctx           = context.Background()
	zinBulkURI    string
	zincHeader    = map[string]string{}
	logMap        = map[string][]string{}
)

func Parse(cfg common.Conf, args []string) {

	merchantBean := conn.BeanNew(cfg.Beanstalkd)
	clientPool = &sync.Pool{
		New: func() interface{} {
			return new(fasthttp.Client)
		},
	}

	defer func() {
		merchantBean.Conn.Release()
	}()

	basic := cfg.Zinc.Username + ":" + cfg.Zinc.Password
	auth := helper.Base64Encode([]byte(basic))
	zincHeader = map[string]string{
		"Content-Type":  "application/json; charset=UTF-8",
		"Authorization": "Basic " + auth,
	}
	zinBulkURI = cfg.Zinc.URL + "/api/_bulk"

	st := time.Now()
	et := time.Now()
	topic := []string{"zinc_fluent_log"}

	for {

		et = time.Now()
		difference := et.Sub(st)
		sec := int(difference.Seconds())
		if sec > 30 {
			//every 30s save once
			for k, v := range logMap {
				if v != nil {
					loggerBulk(k, v)
					logMap[k] = nil //此处只是设置为nil，不删除k，因为这个k会一直重复使用
				}
			}
			st = time.Now()
		}
		id, res, err := merchantBean.Reserve(topic, time.Duration(30)*time.Second)
		if err != nil {
			continue
		}

		zincPickData(res)
		merchantBean.Delete(id)

	}
}

func makeCallRequest(baseURL string, byteBody []byte, customHeaders map[string]string) ([]byte, int, error) {

	req := fasthttp.AcquireRequest()
	defer req.Reset()
	req.SetRequestURI(baseURL)

	for key, val := range customHeaders {
		req.Header.Set(key, val)
	}

	req.Header.SetMethod("POST")

	req.SetBody(byteBody)
	resp := fasthttp.AcquireResponse()
	defer resp.Reset()

	client := clientPool.Get().(*fasthttp.Client)

	//if clientTimeout == 0 {
	//	if err := client.Do(req, resp); err != nil {
	//		return nil, 0, err
	//	}
	//} else {
	if err := client.DoTimeout(req, resp, time.Duration(clientTimeout)); err != nil {
		return nil, 0, err
	}
	//}

	clientPool.Put(client)
	statusCode := resp.StatusCode()
	if statusCode != 200 {
		err := fmt.Errorf("status code: %d, body : %s", statusCode, string(resp.Body()))
		return nil, 0, err
	}

	return resp.SwapBody(nil), statusCode, nil
}

func zincPickData(m []byte) error {

	var p fastjson.Parser
	val, err := p.ParseBytes(m)
	if err != nil {
		fmt.Println("fastjson.ParseBytes = ", err.Error())
		return err
	}

	index := string(val.GetStringBytes("_index"))
	val.Del("_index")

	if _, ok := logMap[index]; !ok {
		bulk := []string{val.String()}
		logMap[index] = bulk
	} else {
		logs := append(logMap[index], val.String())
		if len(logs) >= 20 {
			loggerBulk(index, logs)
			logMap[index] = nil
		} else {
			logMap[index] = logs
		}
	}

	return nil
}
