package zincbeanstalk

import (
	"bytes"
	"fmt"
)

// 日志批量提交
func loggerBulk(_index string, bulk []string) {

	var buffer bytes.Buffer
	index := fmt.Sprintf("{\"index\":{\"_index\":\"%s\"}}\r\n", _index)
	for _, v := range bulk {
		buffer.WriteString(index)
		buffer.WriteString(v)
		buffer.WriteString("\r\n")
	}

	defer buffer.Reset()
	body, code, err := makeCallRequest(zinBulkURI, buffer.Bytes(), zincHeader)
	if err != nil {
		fmt.Println("zinc log failed", code, body, zinBulkURI, zincHeader, buffer.String(), err.Error())
		return
	}

	if code != 200 {
		fmt.Println("zinc log code <> 200", code, body, zinBulkURI, zincHeader, buffer.String())
	}
}
