#! /bin/bash

PROJECT="taskbr"
GitReversion=`git rev-parse HEAD`
BuildTime=`date +'%Y.%m.%d.%H%M%S'`
BuildGoVersion=`go version`

CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -ldflags "-X main.gitReversion=${GitReversion}  -X 'main.buildTime=${BuildTime}' -X 'main.buildGoVersion=${BuildGoVersion}'" -o $PROJECT
# upx $PROJECT
# br
scp -i "C:\Users\lijin\Downloads\Telegram Desktop\id_rsa_nginx" -P 2223  $PROJECT brazil@23.234.61.35:/home/brazil/workspace/br/${PROJECT}/${PROJECT}_br
ssh -i "C:\Users\lijin\Downloads\Telegram Desktop\id_rsa_nginx" -p 2223 brazil@23.234.61.35 "sh /home/brazil/workspace/br/${PROJECT}/br.sh"

