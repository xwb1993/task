package common

import (
	"database/sql"
	"errors"
	"fmt"
	g "github.com/doug-martin/goqu/v9"
	"github.com/shopspring/decimal"
	"task/contrib/helper"
	ryrpc "task/rpc"
)

func MemberVipList() (map[int]MemberVip, error) {

	var (
		data []MemberVip
		mp   = make(map[int]MemberVip)
	)
	query, _, _ := dialect.From("tbl_member_vip").Select(colsMemberVip...).Where(g.Ex{}).Order(g.C("vip").Asc()).ToSQL()
	fmt.Println(query)
	err := taskDb.Select(&data, query)
	if err != nil {
		Log("%s,[%s]", err.Error(), query)
		return nil, err
	}

	for _, v := range data {
		mp[v.Vip] = v
	}
	return mp, nil
}

func MemberVipFindOne(vip int) (MemberVip, error) {

	data := MemberVip{}
	query, _, _ := dialect.From("tbl_member_vip").Select(colsMemberVip...).Where(g.Ex{"vip": vip}).Limit(1).ToSQL()
	fmt.Println(query)
	err := taskDb.Get(&data, query)
	if err != nil {
		if err != sql.ErrNoRows {
			Log("%s,[%s]", err.Error(), query)
		}
		return data, err
	}

	return data, nil
}

func MemberBaseList(ex g.Ex) ([]ryrpc.TblMemberBase, error) {

	var (
		data  []ryrpc.TblMemberBase
		page  []ryrpc.TblMemberBase
		count uint
	)

	query, _, _ := dialect.From("tbl_member_base").Select(g.COUNT("uid")).Where(ex).ToSQL()
	fmt.Println(query)
	err := taskDb.Get(&count, query)
	if err != nil {
		return nil, err
	}

	if count == 0 {
		return nil, nil
	}

	p := count / 1000
	l := count % 1000
	if l > 0 {
		p += 1
	}

	for j := uint(1); j <= p; j++ {
		offset := (p - 1) * 1000
		query, _, _ = dialect.From("tbl_member_base").
			Select(colsMemberBase...).Where(ex).Offset(offset).Limit(1000).Order(g.C("uid").Asc()).ToSQL()
		fmt.Println(query)
		err = taskDb.Select(&page, query)
		if err != nil {
			Log("%s,[%s]", err.Error(), query)
			return data, err
		}

		data = append(data, page...)
	}

	return data, nil
}

func MemberBalance(uid string) (decimal.Decimal, error) {

	var b string
	ex := g.Ex{
		"uid": uid,
	}
	query, _, _ := dialect.From("tbl_member_balance").Select("brl").Where(ex).ToSQL()
	fmt.Println(query)
	err := taskDb.Get(&b, query)
	if err != nil {
		return decimal.Zero, err
	}

	balance, err := decimal.NewFromString(b)
	if err != nil {
		return decimal.Zero, err
	}

	return balance, nil
}

func MemberFindOneByUid(uid string) (ryrpc.TblMemberBase, error) {

	m := ryrpc.TblMemberBase{}

	ex := g.Ex{}
	if uid != "" {
		ex["uid"] = uid
	}

	t := dialect.From("tbl_member_base")
	query, _, _ := t.Select(colsMemberBase...).Where(ex).Limit(1).ToSQL()
	fmt.Println(query)
	err := taskDb.Get(&m, query)
	if err != nil && err != sql.ErrNoRows {
		return m, errors.New(helper.DBErr)
	}

	if err == sql.ErrNoRows {
		return m, errors.New(helper.UsernameErr)
	}

	return m, nil
}
