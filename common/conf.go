package common

import (
	"fmt"
	"github.com/BurntSushi/toml"
	"task/contrib/apollo"
)

type Conf struct {
	Prefix         string `toml:"prefix"`
	Dev            bool   `toml:"dev"`
	Sock5          string `toml:"sock5"`
	RPC            string `toml:"rpc"`
	Proxy          string `toml:"proxy"`
	Ipdb           string `toml:"ipdb"`
	Beanstalkd     string `toml:"beanstalkd"`
	Callback       string `toml:"callback"`
	CallbackDomain string `toml:"callback_domain"`
	WebURL         string `toml:"web_url"`
	Contate        string `toml:"contate"`
	WalletMode     string `toml:"wallet_mode"`
	Db             struct {
		Member      string `toml:"member"`
		Admin       string `toml:"admin"`
		Game        string `toml:"game"`
		Task        string `toml:"task"`
		MaxIdleConn int    `toml:"max_idle_conn"`
		MaxOpenConn int    `toml:"max_open_conn"`
	} `toml:"db"`
	Port struct {
		Member string `toml:"member"`
		Admin  string `toml:"admin"`
		Game   string `toml:"game"`
		RPC    string `toml:"rpc"`
	} `toml:"port"`
	Redis struct {
		Addr     []string `toml:"addr"`
		Password string   `toml:"password"`
		Sentinel string   `toml:"sentinel"`
		Db       int      `toml:"db"`
	} `toml:"redis"`
	Aws struct {
		Bucket          string `toml:"bucket"`
		AccessKeyID     string `toml:"accessKeyID"`
		SecretAccessKey string `toml:"secretAccessKey"`
	} `toml:"aws"`
	Email struct {
		URL      string `toml:"url"`
		Port     string `toml:"port"`
		Username string `toml:"username"`
		Password string `toml:"password"`
	} `toml:"email"`
	Meilisearch struct {
		Host string `toml:"host"`
		Key  string `toml:"key"`
	} `toml:"meilisearch"`
	Zinc struct {
		URL      string `toml:"url"`
		Username string `toml:"username"`
		Password string `toml:"password"`
	} `toml:"zinc"`
}

type PlatCfg_t struct {
	EVO struct {
		API        string `toml:"api"`
		APIPull    string `toml:"api_pull"`
		MerchantID string `toml:"merchant_id"`
		Country    string `toml:"country"`
		Language   string `toml:"language"`
		Currency   string `toml:"currency"`
		CasinoKey  string `toml:"casino_key"`
		APIToken   string `toml:"api_token"`
	} `toml:"evo"`
	VIVO struct {
		API        string `toml:"api"`
		OperatorID string `toml:"operator_id"`
		ServerID   string `toml:"server_id"`
		PassKey    string `toml:"pass_key"`
		Lang       string `toml:"lang"`
		Currency   string `toml:"currency"`
		Country    string `toml:"country"`
	} `toml:"vivo"`
	IBC struct {
		API         string `toml:"api"`
		Prefix      string `toml:"prefix"`
		VendorID    string `toml:"vendor_id"`
		Currency    string `toml:"currency"`
		Lang        string `toml:"lang"`
		OperatorId  string `toml:"operator_id"`
		MaxTransfer string `toml:"max_transfer"`
		MinTransfer string `toml:"min_transfer"`
		OddsType    string `toml:"odds_type"`
	} `toml:"ibc"`
	BTI struct {
		API        string `toml:"api"`
		AgentID    string `toml:"agent_id"`
		CustomerID string `toml:"customer_id"`
		AuthKey    string `toml:"auth_key"`
		Currency   string `toml:"currency"`
		Lang       string `toml:"lang"`
		OddsType   string `toml:"odds_type"`
	} `toml:"bti"`
	PG struct {
		API           string `toml:"api"`
		OperatorToken string `toml:"operator_token"`
		SecretKey     string `toml:"secret_key"`
		Salt          string `toml:"salt"`
		Currency      string `toml:"currency"`
		Language      string `toml:"language"`
		LaunchURL     string `toml:"launch_url"`
		DataURL       string `toml:"data_url"`
	} `toml:"pg"`
	JDB struct {
		API   string `toml:"api"`
		Agent string `toml:"agent"`
		Key   string `toml:"key"`
		Iv    string `toml:"iv"`
		Dc    string `toml:"dc"`
		Lang  string `toml:"lang"`
	} `toml:"jdb"`
	Jl struct {
		API      string `toml:"api"`
		AgentID  string `toml:"agent_id"`
		AgentKey string `toml:"agent_key"`
		Lang     string `toml:"lang"`
		Currency string `toml:"currency"`
	} `toml:"jl"`
	PP struct {
		API         string `toml:"api"`
		SecureLogin string `toml:"secure_login"`
		ProviderID  string `toml:"provider_id"`
		SecretKey   string `toml:"secret_key"`
		Lang        string `toml:"lang"`
		Currency    string `toml:"currency"`
		Country     string `toml:"country"`
	} `toml:"pp"`
	One struct {
		API       string `toml:"api"`
		APIKey    string `toml:"api_key"`
		APISecret string `toml:"api_secret"`
		Lang      string `toml:"lang"`
		Currency  string `toml:"currency"`
	} `toml:"one"`
}

func ConfParse(endpoints []string, etcdName, etcdPass, path string) Conf {

	cfg := Conf{}

	apollo.New(endpoints, etcdName, etcdPass)
	apollo.ParseTomlStruct(path, &cfg)
	apollo.Close()

	return cfg
}

func ConfPlatParse(endpoints []string, etcdName, etcdPass, path string) PlatCfg_t {

	cfg := PlatCfg_t{}

	apollo.New(endpoints, etcdName, etcdPass)
	//apollo.ParseTomlStruct(path, &cfg)
	_, err := toml.DecodeFile(path, &cfg)
	if err != nil {
		fmt.Printf("ParseTomlStruct error: %s", err.Error())
		return cfg
	}
	apollo.Close()

	return cfg
}
