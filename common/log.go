package common

import (
	"fmt"
	"runtime"
	"strings"
	"task/contrib/helper"
	"time"
)

type zinc_t struct {
	ID       string `json:"id"`
	Content  string `json:"content"`
	Flags    string `json:"flags"`
	Filename string `json:"filename"`
	Index    string `json:"_index"`
}

func Log(flag, format string, v ...interface{}) {

	_, file, line, _ := runtime.Caller(1)
	paths := strings.Split(file, "/")
	l := len(paths)
	if l > 2 {
		file = paths[l-2] + "/" + paths[l-1]
	}
	path := fmt.Sprintf("%s:%d", file, line)
	msg := fmt.Sprintf(format, v...)

	fmt.Println(msg)

	ts := time.Now()
	id := helper.GenId()

	data := zinc_t{
		ID:       id,
		Content:  msg,
		Flags:    "LOG",
		Filename: path,
		Index:    fmt.Sprintf("task_%s_%04d%02d", flag, ts.Year(), ts.Month()),
	}
	b, err := helper.JsonMarshal(data)
	if err != nil {
		fmt.Println("task Log MarshalBinary err =  ", err.Error())
		return
	}

	_, err = taskBean.Put("zinc_fluent_log", b, 0, 0, 0)
	if err != nil {
		fmt.Println("pushLog MerchantBean Put err =  ", err.Error())
		return
	}
}
