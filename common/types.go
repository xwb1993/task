package common

type MemberVip struct {
	Vip           int    `json:"vip" db:"vip" cbor:"vip"`                                  //会员等级
	Name          string `json:"name" db:"name" cbor:"name"`                               //会员等级名称
	DepositAmount int64  `json:"deposit_amount" db:"deposit_amount" cbor:"deposit_amount"` //对应的积分
	Flow          int64  `json:"flow" db:"flow" cbor:"flow"`                               //升级对应的流水
	KeepFlow      int64  `json:"keep_flow" db:"keep_flow" cbor:"keep_flow"`                //保级对应的流水
	RebateRate    string `json:"rebate_rate" db:"rebate_rate" cbor:"rebate_rate"`          //返水比例
	Amount        string `json:"amount" db:"amount" cbor:"amount"`                         //升级奖励金额
}

type MemberTransaction struct {
	AfterAmount  string `db:"after_amount" json:"after_amount" cbor:"after_amount"`    //账变后的金额
	Amount       string `db:"amount" json:"amount" cbor:"amount"`                      //用户填写的转换金额
	BeforeAmount string `db:"before_amount" json:"before_amount" cbor:"before_amount"` //账变前的金额
	BillNo       string `db:"bill_no" json:"bill_no" cbor:"bill_no"`                   //转账|充值|提现ID
	CashType     int    `db:"cash_type" json:"cash_type" cbor:"cash_type"`             //0:转入1:转出2:转入失败补回3:转出失败扣除4:存款5:提现
	CreatedAt    int64  `db:"created_at" json:"created_at" cbor:"created_at"`          //
	ID           string `db:"id" json:"id" cbor:"id"`                                  //
	UID          string `db:"uid" json:"uid" cbor:"uid"`                               //用户ID
	Username     string `db:"username" json:"username" cbor:"username"`                //用户名
	Remark       string `db:"remark" json:"remark" cbor:"remark"`                      //备注
	OperationNo  string `db:"operation_no" json:"operation_no" cbor:"operation_no"`    //操作码
	PlatformID   string `db:"platform_id" json:"platform_id" cbor:"platform_id"`       //场馆id
	Tester       int    `db:"tester" json:"tester" cbor:"tester"`                      //1正式 2测试 3代理
}

type GameRecord struct {
	RowId          string  `db:"row_id" json:"row_id" cbor:"row_id"`
	BillNo         string  `db:"bill_no" json:"bill_no" cbor:"bill_no"`
	ApiType        string  `db:"api_type" json:"api_type" cbor:"api_type"`
	PlayerName     string  `db:"player_name" json:"player_name" cbor:"player_name"`
	Name           string  `db:"name" json:"name" cbor:"name"`
	Uid            string  `db:"uid" json:"-" cbor:"uid"`
	TopUid         string  `db:"top_uid" json:"-"`                                                 //总代uid
	TopName        string  `db:"top_name" json:"top_name"`                                         //总代代理
	ParentUid      string  `db:"parent_uid" json:"parent_uid" cbor:"parent_uid"`                   //上级uid
	ParentName     string  `db:"parent_name" json:"parent_name" cbor:"parent_name"`                //上级代理
	GrandID        string  `db:"grand_id" json:"grand_id"  cbor:"grand_id"`                        //祖父级代理id
	GrandName      string  `db:"grand_name" json:"grand_name" cbor:"grand_name"`                   //祖父级代理
	GreatGrandID   string  `db:"great_grand_id" json:"great_grand_id"  cbor:"great_grand_id"`      //曾祖父级代理id
	GreatGrandName string  `db:"great_grand_name" json:"great_grand_name" cbor:"great_grand_name"` //曾祖父级代理
	NetAmount      float64 `db:"net_amount" json:"net_amount" cbor:"net_amount"`
	BetTime        int64   `db:"bet_time" json:"bet_time" cbor:"bet_time"`
	SettleTime     int64   `db:"settle_time" json:"settle_time" cbor:"settle_time"`
	ApiBetTime     int64   `db:"api_bet_time" json:"api_bet_time" cbor:"api_bet_time"`
	ApiSettleTime  int64   `db:"api_settle_time" json:"api_settle_time" cbor:"api_settle_time"`
	StartTime      int64   `db:"start_time" json:"start_time" cbor:"start_time"`
	Resettle       uint8   `db:"resettle" json:"resettle" cbor:"resettle"`
	Presettle      uint8   `db:"presettle" json:"presettle" cbor:"presettle"`
	GameType       string  `db:"game_type" json:"game_type" cbor:"game_type"`
	GameCode       string  `db:"game_code" json:"game_code" cbor:"game_code"`
	BetAmount      float64 `db:"bet_amount" json:"bet_amount" cbor:"bet_amount"`
	ValidBetAmount float64 `db:"valid_bet_amount" json:"valid_bet_amount" cbor:"valid_bet_amount"`
	RebateAmount   float64 `db:"-" json:"rebate_amount" cbor:"rebate_amount"`
	Flag           int     `db:"flag" json:"flag" cbor:"flag"`
	PlayType       string  `db:"play_type" json:"play_type" cbor:"play_type"`
	Prefix         string  `db:"prefix" json:"prefix" cbor:"prefix"`
	Result         string  `db:"result" json:"result" cbor:"result"`
	CreatedAt      int64   `db:"created_at" json:"-" cbor:"created_at"`
	UpdatedAt      int64   `db:"updated_at" json:"-" cbor:"updated_at"`
	ApiName        string  `db:"api_name" json:"api_name" cbor:"api_name"`
	ApiBillNo      string  `db:"api_bill_no" json:"api_bill_no" cbor:"api_bill_no"`
	MainBillNo     string  `db:"main_bill_no" json:"main_bill_no" cbor:"main_bill_no"`
	GameName       string  `db:"game_name" json:"game_name" cbor:"game_name"`
	HandicapType   string  `db:"handicap_type" json:"handicap_type" cbor:"handicap_type"`
	Handicap       string  `db:"handicap" json:"handicap" cbor:"handicap"`
	Odds           float64 `db:"odds" json:"odds" cbor:"odds"`
	Tester         int     `db:"tester" json:"tester" cbor:"tester"`
}

type RebateRecord struct {
	ID           string `json:"id" db:"id" cbor:"id"`
	RowId        string `json:"row_id" db:"row_id" cbor:"row_id"`                      //注单表row_id
	Uid          string `json:"uid" db:"uid" cbor:"uid"`                               //用户ID(返水会员uid)
	Username     string `json:"username" db:"username" cbor:"username"`                //用户名(返水会员名)
	SubUid       string `json:"sub_uid" db:"sub_uid" cbor:"sub_uid"`                   //下级用户ID(投注会员uid)
	SubName      string `json:"sub_name" db:"sub_name" cbor:"sub_name"`                //下级用户名(投注会员名)
	PlatformId   string `json:"platform_id" db:"platform_id" cbor:"platform_id"`       //场馆id
	CashType     int    `json:"cash_type" db:"cash_type" cbor:"cash_type"`             //203 下级返水（1级）204下级返水（2级）205下级返水（3级）
	Amount       string `json:"amount" db:"amount" cbor:"amount"`                      //用户填写的转换金额
	RebateAmount string `json:"rebate_amount" db:"rebate_amount" cbor:"rebate_amount"` //用户填写的转换金额
	Rate         string `json:"rate" db:"rate" cbor:"rate"`                            //返水比例
	SettleTime   int64  `json:"settle_time" db:"settle_time" cbor:"settle_time"`       //注单结算时间
	CreatedAt    int64  `json:"created_at" db:"created_at" cbor:"created_at"`          //创建时间
}

type BonusConfig struct {
	ID             string `db:"id" cbor:"id"`
	Name           string `db:"name" cbor:"name"`
	Code           string `db:"code" cbor:"code"`
	RateAmount     string `db:"rate_amount" cbor:"rate_amount"`
	LvlOneRebate   string `db:"lvl_one_rebate" cbor:"lvl_one_rebate"`
	LvlTwoRebate   string `db:"lvl_two_rebate" cbor:"lvl_two_rebate"`
	LvlThreeRebate string `db:"lvl_three_rebate" cbor:"lvl_three_rebate"`
}

type RebateTransaction struct {
	UID          string `db:"uid"`           //uid
	RebateAmount string `db:"rebate_amount"` //返水金额
}

type TblPromoInviteRecord struct {
	Id             string  `db:"id" cbor:"id"`
	Uid            string  `db:"uid" cbor:"uid"`
	Username       string  `db:"username" cbor:"username"`
	Lvl            int     `db:"lvl" cbor:"lvl"`                           //123 一级二级三级
	ChildUid       string  `db:"child_uid" cbor:"child_uid"`               //下级的uid
	ChildUsername  string  `db:"child_username" cbor:"child_username"`     //下级的账号
	FirstDepositAt int     `db:"first_deposit_at" cbor:"first_deposit_at"` //首存时间
	DepositAmount  float64 `db:"deposit_amount" cbor:"deposit_amount"`     //存款金额
	BonusAmount    float64 `db:"bonus_amount" cbor:"bonus_amount"`         //奖金
	CreatedAt      uint32  `db:"created_at" cbor:"created_at"`             //注册时间
	State          int     `db:"state" json:"state"`                       //状态1注册未充值2充值未结算3已结算4过期作废
}
