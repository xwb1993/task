package pull

import (
	"fmt"
	"github.com/shopspring/decimal"
	"github.com/valyala/fasthttp"
	"github.com/valyala/fastjson"
	"strconv"
	"strings"
	"task/common"
	"task/contrib/helper"
	"time"
	"unicode/utf8"
)

var (
	evoCigT = EvoConfig{
		Path: "/api/gamehistory/v1/casino/games",
	}
	param = EvoParam{}
)

// 配置
type EvoConfig struct {
	Api      string `json:"api"`
	CasinoId string `json:"casino_id"`
	Ua2Token string `json:"ua2Token"`
	Path     string `json:"path"`
}

type EvoParam struct {
	StartDate string `json:"startDate"`
	EndDate   string `json:"endDate"`
}

var (
	evoCig = EvoConfig{
		Path: "/api/gamehistory/v1/casino/games",
	}

	evop = EvoParam{}
)

type evoGameBet struct {
	Stake       decimal.Decimal `json:"stake"`
	Payout      decimal.Decimal `json:"payout"`
	PlacedOn    string          `json:"placedOn"`
	Code        string          `json:"code"`
	Description string          `json:"description"`
}

// 转账拉单
func EvoReadT(date, flag string) bool {
	now := time.Now().Unix()
	// 全量补单
	if date != "0" && flag == "full" {
		if len(date) == 8 {
			year, _ := strconv.Atoi(date[:4])
			month, _ := strconv.Atoi(date[4:6])
			day, _ := strconv.Atoi(date[6:])
			t := time.Date(year, time.Month(month), day, 0, 0, 0, 0, time.Local)
			startTime := t.Unix()
			for i := 0; i < 24; i++ {
				endTime := startTime + 3600
				if endTime > now+3600 {
					break
				}
				if endTime > now-60 {
					endTime = now
				}
				stNow := time.Unix(startTime, 0).In(locUTC)
				etNow := time.Unix(endTime, 0).In(locUTC)
				param.StartDate = stNow.Format(TIME8)
				param.EndDate = etNow.Format(TIME8)
				_ = evoProcessT(&param, flag)
				startTime += 3600
				time.Sleep(time.Duration(12) * time.Second)
			}
		} else if len(date) == 10 {
			year, _ := strconv.Atoi(date[:4])
			month, _ := strconv.Atoi(date[4:6])
			day, _ := strconv.Atoi(date[6:8])
			hour, _ := strconv.Atoi(date[8:])
			t := time.Date(year, time.Month(month), day, hour, 0, 0, 0, time.Local)
			startTime := t.Unix()
			endTime := startTime + 3600
			if endTime > now {
				endTime = now
			}
			stNow := time.Unix(startTime, 0).In(locUTC)
			etNow := time.Unix(endTime, 0).In(locUTC)
			param.StartDate = stNow.Format(TIME8)
			param.EndDate = etNow.Format(TIME8)
			_ = evoProcessT(&param, flag)
		}
		return true
	}
	//正常1小时前到现在
	param.StartDate = time.Now().Add(-time.Minute * 60).In(locUTC).Format(TIME8)
	param.EndDate = time.Now().Add(-time.Minute * 1).In(locUTC).Format(TIME8)
	_ = evoProcessT(&param, flag)

	return true
}

func evoProcessT(param *EvoParam, flag string) error {

	evoCigT.Api = GameConfig.EVO.APIPull
	evoCigT.CasinoId = GameConfig.EVO.CasinoKey
	evoCigT.Ua2Token = GameConfig.EVO.APIToken
	evoPageT(evoCigT, flag, param)

	return nil
}

func evoPageT(evoCigT EvoConfig, flag string, param *EvoParam) bool {
	var p fastjson.Parser
	header := map[string]string{
		"Authorization": fmt.Sprintf(`Basic %s`, helper.Base64Encode([]byte(fmt.Sprintf(`%s:%s`, evoCigT.CasinoId, evoCigT.Ua2Token)))),
	}
	requestUri := fmt.Sprintf(`%s%s?startDate=%s&endDate=%s`, evoCigT.Api, evoCigT.Path, param.StartDate, param.EndDate)
	fmt.Println("header=", header)
	fmt.Println("requestUri=", requestUri)
	statusCode, body, err := common.HttpGetHeader(requestUri, header)
	if err != nil {
		fmt.Println("evoPageT err=", err)
		return false
	}
	fmt.Println("statusCode=", statusCode)
	fmt.Println("body={}")
	fmt.Println(string(body))

	if statusCode != fasthttp.StatusOK {
		fmt.Printf("evoT unexpected status code: %d. Expecting %d\r\n", statusCode, fasthttp.StatusOK)
		return false
	}

	v, err := p.ParseBytes(body)
	if err != nil {
		fmt.Println("evoT Page content error : ", err, string(body))
		return false
	}
	datalist := v.GetArray("data")
	for _, r := range datalist {
		games := r.GetArray("games")
		for _, gameData := range games {
			table := gameData.Get("table")
			gameId := string(gameData.GetStringBytes("id"))
			participants := gameData.GetArray("participants")
			settleTime := string(gameData.GetStringBytes("settledAt"))
			status := string(gameData.GetStringBytes("status"))
			object := gameData.GetObject("result")
			gameResult := fmt.Sprintf("%v", object)
			gameTypeZr := string(gameData.GetStringBytes("gameType"))
			for _, participant := range participants {
				playerId := string(participant.GetStringBytes("playerId"))
				casinoId := string(participant.GetStringBytes("casinoId"))
				bets := participant.GetArray("bets")
				//betMap := map[string]evoGameBet{}
				for idx, bet := range bets {
					billNo := string(bet.GetStringBytes("transactionId"))
					gameBill := evoGameBet{
						Stake:       decimal.NewFromFloat(bet.GetFloat64("stake")),
						Payout:      decimal.NewFromFloat(bet.GetFloat64("payout")),
						PlacedOn:    string(bet.GetStringBytes("placedOn")),
						Code:        string(bet.GetStringBytes("code")),
						Description: string(bet.GetStringBytes("description")),
					}
					err := evoParseT(table, playerId, casinoId, gameId, billNo, gameBill, flag, idx, settleTime, status, gameResult, gameTypeZr)
					if err != nil {
						continue
					}
				}
			}
		}
	}
	return true
}
func evoParseT(table *fastjson.Value, playerId, casinoId, gameId string, billNo string, gb evoGameBet, flag string, idx int, settleTime, status, gameResult, gameTypeZr string) error {

	username := playerId
	gameCode := string(table.GetStringBytes("id"))
	gameName := string(table.GetStringBytes("name"))
	betId := billNo
	betTime := gb.PlacedOn
	settime := settleTime
	betAmount, _ := gb.Stake.Float64()
	winLoss, _ := gb.Payout.Float64() //派奖金额
	netAmount, _ := decimal.NewFromFloat(winLoss).Sub(decimal.NewFromFloat(betAmount)).Float64()
	validbet, _ := decimal.NewFromFloat(betAmount).Abs().Float64()

	info, _ := MemberFindOne(username[3:])
	if info.Uid == "" {
		info.Uid = "0"
		return nil
	}
	if info.ParentID == "" {
		info.ParentID = "0"
	}
	if info.TopID == "" {
		info.TopID = "0"
	}
	if info.ParentName == "" {
		info.ParentName = ""
	}
	if info.TopName == "" {
		info.TopName = ""
	}
	betContent := evoPlayResult[gb.Code]
	if betContent == "" {
		split := strings.Split(gb.Code, "_")
		if len(split) > 2 {
			betContent = evoPlayResult[split[1]]
			if betContent == "" {
				betContent = gb.Code
			}
		}
	}
	if betContent == "" {
		betContent = gb.Description
	}
	result := "HandNumber:" + string(table.GetStringBytes("id")) + "|casinoId:" + casinoId + "|gameId:" + gameId
	orderFlag := 1
	if status == "Cancelled" {
		orderFlag = 2
	}
	// zr
	netAbs, _ := decimal.NewFromFloat(netAmount).Abs().Float64()
	betAbs, _ := decimal.NewFromFloat(betAmount).Abs().Float64()
	if netAbs >= betAbs {
		validbet = betAbs
	} else {
		validbet = netAbs
	}
	if utf8.RuneCountInString(gameResult) > 200 {
		gameResultText := []rune(gameResult)
		gameResult = string(gameResultText[:100])
	}
	if utf8.RuneCountInString(betContent) > 200 {
		playContentText := []rune(betContent)
		betContent = string(playContentText[:100])
	}
	record := GameRecord{
		Uid:            info.Uid,
		Result:         result,
		GameType:       "1",
		GameName:       gameName,
		GameCode:       gameCode,
		ApiType:        16595015200101,
		ApiName:        "EVO",
		Flag:           orderFlag,
		Handicap:       gameCode,
		PlayType:       gameName,
		CreatedAt:      time.Now().UnixMilli(),
		Prefix:         username[:3],
		PlayerName:     info.Username,
		Name:           info.Username,
		BetAmount:      betAmount,
		ValidBetAmount: validbet,
		ApiBillNo:      betId,
		BillNo:         betId,
		NetAmount:      netAmount,
		Resettle:       0,
		Presettle:      0,
		RowId:          "evo" + gameId + gb.Code + betId,
		MainBillNo:     gameId,
		TableId:        gameCode,
		PlayContent:    betContent,
		GameResult:     gameResult,
		GameTypeZr:     gameTypeZr,
	}
	record.ParentUid = info.ParentID
	record.ParentName = info.ParentName
	record.GrandId = info.GrandID
	record.GrandName = info.GrandName
	record.GrantGrantId = info.GreatGrandID
	record.GrantGrantName = info.GreatGrandName
	record.TopUid = info.TopID
	record.TopName = info.TopName
	if betTime != "" {
		apibetTime, _ := time.ParseInLocation(TIME8, betTime, locUTC)
		record.ApiBetTime = apibetTime.UnixMilli()
		record.BetTime = apibetTime.UnixMilli()
	}

	if settime != "" {
		apisettleTime, _ := time.ParseInLocation(TIME8, settime, locUTC)
		record.ApiSettleTime = apisettleTime.UnixMilli()
		record.SettleTime = apisettleTime.UnixMilli()
	}

	//入库处理
	if cb, ok := modes[flag]; ok {
		cb("evo", record)
	}
	return nil
}

var (
	evoPlayResult = map[string]string{
		"Player":                      "闲家",
		"Banker":                      "庄家",
		"Tie":                         "平局",
		"None":                        "没结局",
		"01":                          "下注在01",
		"02":                          "下注在02",
		"05":                          "下注在05",
		"10":                          "下注在10",
		"20":                          "下注在20",
		"X2":                          "两倍奖金，再转盘",
		"X7":                          "七倍奖金，再转盘",
		"Dragon":                      "龙",
		"Tiger":                       "虎",
		"Suited Tie":                  "同花和局",
		"A":                           "主場",
		"B":                           "客場",
		"X":                           "平",
		"SevenCardStraightFlush":      "7张牌同花顺",
		"SixCardStraightFlush":        "6张牌同花顺",
		"MiniRoyal":                   "迷你皇家",
		"RoyalFlush":                  "皇家同花顺",
		"StraightFlush":               "同花顺",
		"FourOfAKind":                 "四条",
		"FullHouse":                   "葫芦",
		"Flush":                       "同花",
		"Straight":                    "順子",
		"ThreeOfAKind":                "最火3总和",
		"TwoPairs":                    "两对",
		"OnePair":                     "一对",
		"HighCard":                    "高牌",
		"Dealer":                      "荷官",
		"Push":                        "推牌",
		"No Winner":                   "无胜负",
		"No Decision":                 "无决定",
		"AutoStand":                   "自动停牌",
		"Stand":                       "停牌",
		"Hit":                         "要牌",
		"Split":                       "分拆",
		"DoubleDown":                  "加倍",
		"AutoHit":                     "自动要牌",
		"EarlyCashOut":                "提前兑现",
		"Surrender":                   "弃牌",
		"Win":                         "玩家胜",
		"Lost":                        "玩家输",
		"Bust":                        "玩家爆点",
		"1":                           "1",
		"2":                           "2",
		"3":                           "3",
		"4":                           "4",
		"5":                           "5",
		"6":                           "6",
		"Small":                       "小",
		"Big":                         "大",
		"Even":                        "双",
		"Odd":                         "单",
		"Combo1And2":                  "组合1和2",
		"Combo1And3":                  "组合1和3",
		"Combo1And4":                  "组合1和4",
		"Combo1And5":                  "组合1和5",
		"Combo1And6":                  "组合1和6",
		"Combo2And3":                  "组合2和3",
		"Combo2And4":                  "组合2和4",
		"Combo2And5":                  "组合2和5",
		"Combo2And6":                  "组合2和6",
		"Combo3And4":                  "组合3和4",
		"Combo3And5":                  "组合3和5",
		"Combo3And6":                  "组合3和6",
		"Combo4And5":                  "组合4和5",
		"Combo4And6":                  "组合4和6",
		"Combo5And6":                  "组合5和6",
		"Double1":                     "双1",
		"Double2":                     "双2",
		"Double3":                     "双3",
		"Double4":                     "双4",
		"Double5":                     "双5",
		"Double6":                     "双6",
		"Total4":                      "总共4",
		"Total5":                      "总共5",
		"Total6":                      "总共6",
		"Total7":                      "总共7",
		"Total8":                      "总共8",
		"Total9":                      "总共9",
		"Total10":                     "总共10",
		"Total11":                     "总共11",
		"Total12":                     "总共12",
		"Total13":                     "总共13",
		"Total14":                     "总共14",
		"Total15":                     "总共15",
		"Total16":                     "总共16",
		"Total17":                     "总共17",
		"Triple":                      "三",
		"Triple1":                     "三1",
		"Triple2":                     "三2",
		"Triple3":                     "三3",
		"Triple4":                     "三4",
		"Triple5":                     "三5",
		"Triple6":                     "三6",
		"Total3":                      "总共3",
		"Total18":                     "总共18",
		"Main":                        "主要赌注",
		"Hot3":                        "幸运3",
		"AnyPair":                     "任何对子",
		"BustIt":                      "爆",
		"21_plus_3":                   "21+3",
		"Insurance":                   "保险",
		"DoubleOnFirst":               "加倍的第一手",
		"DoubleOnSecond":              "加倍第二手牌",
		"TripleDown":                  "三倍赌注",
		"QuadrupleDown":               "四倍赌注",
		"TripleOnFirst":               "第一手三倍赌注",
		"TripleOnSecond":              "第二手三倍赌注",
		"QuadrupleOnFirst":            "第一手四倍赌注",
		"QuadrupleOnSecond":           "第二手四倍赌注",
		"BJ_FLUSH":                    "同花",
		"BJ_THREE_OF_A_KIND":          "最火3总和",
		"BJ_STRAIGHT":                 "順子",
		"BJ_STRAIGHT_FLUSH":           "同花顺",
		"BJ_SUITED_THREE_OF_A_KIND":   "同花三条",
		"BJ_SUITED_PAIR":              "同花对子",
		"BJ_ANY_PAIR":                 "一对",
		"SBJ_TRIPLE_7":                "7-7-7",
		"SBJ_SUITED_21":               "总共21同花",
		"SBJ_UNSUITED_21":             "总共21非同花",
		"SBJ_SUITED_20":               "总共20",
		"SBJ_SUITED_19":               "总共19",
		"SBJ_BUST_IT_PUSH":            "推牌",
		"SBJ_BUST_IT_3CARDS":          "3张牌爆掉",
		"SBJ_BUST_IT_4CARDS":          "4张牌爆",
		"SBJ_BUST_IT_5CARDS":          "5张牌爆掉",
		"SBJ_BUST_IT_6CARDS":          "6张牌爆",
		"SBJ_BUST_IT_7CARDS":          "7张牌爆掉",
		"SBJ_BUST_IT_8_OR_MORE_CARDS": "8+张牌爆",
	}
)
