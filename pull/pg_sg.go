package pull

import (
	"fmt"
	"github.com/google/uuid"
	"github.com/shopspring/decimal"
	"github.com/valyala/fasthttp"
	"github.com/valyala/fastjson"
	"strconv"
	"task/common"
	"time"
)

const pgSgId = "6861705028422769039"

var (
	pgsgp = pgSgReq{
		SecretKey:     "",
		OperatorToken: "",
		BetType:       "1",
		HandsStatus:   "0",
		RowVersion:    1,
	}
	pgsgm = map[string]string{
		"60":      "Leprechaun Riches",
		"65":      "Mahjong Ways",
		"74":      "Mahjong Ways 2",
		"87":      "Treasures of Aztec",
		"89":      "Lucky Neko",
		"48":      "Double Fortune",
		"53":      "The Great Icescape",
		"54":      "Captains Bounty",
		"71":      "Caishen Wins",
		"75":      "Ganesha Fortune",
		"79":      "Dreams of Macau",
		"84":      "Queen of Bounty ",
		"98":      "Fortune Ox",
		"104":     "Wild Bandito",
		"106":     "Ways of the Qilin",
		"57":      "Dragon Hatch",
		"73":      "Egypt's Book of Mystery",
		"82":      "Phoenix Rises",
		"83":      "Wild Fireworks",
		"92":      "Thai River Wonders",
		"94":      "Bali Vacation",
		"103":     "Crypto Gold",
		"1":       "Honey Trap of Diao Chan",
		"3":       "Fortune Gods",
		"0":       "Game Lobby",
		"24":      "Win Win Won",
		"6":       "Medusa II",
		"26":      "Tree of Fortune",
		"7":       "Medusa ",
		"25":      "Plushie Frenzy",
		"17":      "Wizdom Wonders",
		"2":       "Gem Saviour",
		"18":      "Hood vs Wolf",
		"28":      "Hotpot",
		"29":      "Dragon Legend",
		"35":      "Mr. Hallow-Win",
		"34":      "Legend of Hou Yi",
		"36":      "Prosperity Lion",
		"33":      "Hip Hop Panda",
		"37":      "Santas Gift Rush",
		"31":      "Baccarat Deluxe",
		"38":      "Gem Saviour Sword",
		"39":      "Piggy Gold",
		"41":      "Symbols of Egypt",
		"44":      "Emperors Favour",
		"42":      "Ganesha Gold",
		"43":      "Three Monkeys ",
		"40":      "Jungle Delight",
		"50":      "Journey to the Wealth",
		"61":      "Flirting Scholar",
		"59":      "Ninja vs Samurai",
		"64":      "Muay Thai Champion",
		"63":      "Dragon Tiger Luck",
		"68":      "Fortune Mouse",
		"20":      "Reel Love",
		"62":      "Gem Saviour Conquest",
		"67":      "Shaolin Soccer",
		"70":      "Candy Burst ",
		"69":      "Bikini Paradise ",
		"85":      "Genie's 3 Wishes",
		"80":      "Circus Delight",
		"90":      "Secrets of Cleopatra",
		"58":      "Vampires Charm",
		"88":      "Jewels of Prosperity",
		"97":      "Jack Frosts Winter",
		"86":      "Galactic Gems",
		"91":      "Guardians of Ice and Fire",
		"93":      "Opera Dynasty",
		"95":      "Majestic Treasures",
		"100":     "Candy Bonanza",
		"105":     "Heist  Stakes",
		"101":     "Rise of Apollo",
		"109":     "Sushi Oishi",
		"110":     "Jurassic Kingdom",
		"102":     "Mermaid Riches",
		"111":     "Groundhog Harvest",
		"113":     "Raider Jane's Crypt of Fortune",
		"115":     "Supermarket Spree",
		"108":     "Buffalo Win",
		"107":     "Legendary Monkey King",
		"119":     "Spirited Wonders",
		"116":     "Farm Invaders",
		"114":     "Emoji Riches",
		"117":     "Cocktail Nights",
		"118":     "Mask Carnival",
		"112":     "Oriental Prosperity",
		"126":     "Fortune Tiger",
		"122":     "Garuda Gems",
		"121":     "Destiny of Sun & Moon",
		"125":     "Butterfly Blossom",
		"123":     "Rooster Rumble",
		"124":     "Battleground Royale",
		"127":     "Speed Winner",
		"128":     "Legend of Perseus",
		"129":     "Win Win Fish Prawn Crab",
		"130":     "Lucky Piggy",
		"132":     "Wild Coaster",
		"135":     "Wild Bounty Showdown",
		"1312883": "Prosperity Fortune Tree",
		"1338274": "Totem Wonders",
		"1340277": "Asgardian Rising",
		"1368367": "Alchemy Gold",
		"1372643": "Diner Delights",
		"1381200": "Hawaiian Tiki",
		"1402846": "Midas Fortune",
		"1418544": "Bakery Bonanza",
		"1420892": "Rave Party Fever",
		"1448762": "Songkran Splash",
		"1543462": "Fortune Rabbit",
	}

	pasgKey = fmt.Sprintf("pull:%s", pgSgId)
)

type pgSgReq struct {
	OperatorToken string `json:"operator_token"`
	SecretKey     string `json:"secret_key"`
	Count         string `json:"count"`
	BetType       string `json:"bet_type"`
	RowVersion    int64  `json:"row_version"`
	HandsStatus   string `json:"hands_status"`
}

func PGSgReadT(date, flag string) bool {

	// 全量补单
	if flag == "full" {

		if len(date) != 8 {
			return false
		}
		year, _ := strconv.Atoi(date[:4])
		month, _ := strconv.Atoi(date[4:6])
		day, _ := strconv.Atoi(date[6:])
		//fmt.Println(year, month, day)
		t := time.Date(year, time.Month(month), day, 0, 0, 0, 0, time.Local)
		pgsgp.RowVersion = t.UnixMilli()
		_ = pgSgProcessT(pgsgp, flag)
		return true
	} else if flag == "min" {

		min, _ := strconv.ParseInt(date, 10, 64)
		pgsgp.RowVersion = time.Now().UnixMilli() - min*60000
		_ = pgSgProcessT(pgsgp, "full")
		return true
	} else {

		pgsgp.RowVersion = time.Now().UnixMilli() - 1800000
		_ = pgSgProcessT(pgsgp, "full")
		return true
	}
}

func pgSgProcessT(req pgSgReq, flag string) error {

	pgUri := GameConfig.PG.DataURL + "/Bet/v4/GetHistory"
	req.SecretKey = GameConfig.PG.SecretKey
	req.OperatorToken = GameConfig.PG.OperatorToken
	req.Count = "5000"
	fmt.Println(req)
	i := 0
	b, ts := pgSgPage(req, flag, pgUri)
	for b && ts > 0 && ts < time.Now().UnixMilli()-1800000 && i < 600 {
		time.Sleep(4 * time.Second)
		i += 1
		req.RowVersion = ts
		b, ts = pgSgPage(req, flag, pgUri)
	}

	return nil
}

func pgSgPage(req pgSgReq, flag, pgUri string) (bool, int64) {

	var p fastjson.Parser

	//fmt.Println(req.StartTime, req.EndTime)

	header := map[string]string{
		"Content-Type": "application/x-www-form-urlencoded",
	}
	args := map[string]string{
		"operator_token": req.OperatorToken,
		"secret_key":     req.SecretKey,
		"count":          req.Count,
		"bet_type":       req.BetType,
		"row_version":    fmt.Sprintf(`%d`, req.RowVersion),
		"hands_status":   req.HandsStatus,
	}
	params := paramEncode(args)
	fmt.Println(params)
	reqUri := fmt.Sprintf(`%s?trace_id=%s`, pgUri, uuid.New().String())
	fmt.Println("requri=", reqUri)
	//payload := strings.NewReader(params)
	//client := &http.Client{}
	//request, err := http.NewRequest("POST", reqUri, payload)
	//if err != nil {
	//	return false, 0
	//}
	//request.Header.Add("Content-Type", "application/x-www-form-urlencoded")
	//res, err := client.Do(request)
	//if err != nil {
	//	return false, 0
	//}
	//defer res.Body.Close()
	//
	//body, err := ioutil.ReadAll(res.Body)
	//if err != nil {
	//	return false, 0
	//}
	//fmt.Println(string(body))

	body, statusCode, err := common.HttpDoTimeout([]byte(params), "POST", reqUri, header, time.Duration(200)*time.Second)
	if err != nil {
		if flag == "full" {
			fmt.Println("err1 = ", err.Error(), err)
			fmt.Println("timeout=", err.Error() == "timeout")
		}
		if err.Error() == "timeout" {
			body, statusCode, err = common.HttpDoTimeout([]byte(params), "POST", reqUri, header, time.Duration(200)*time.Second)
		}
		if err != nil {
			if flag == "full" {
				fmt.Println("err10 = ", err.Error(), err)
				fmt.Println("timeout=", err.Error() == "timeout")
			}
			return false, 0
		}
	}

	if statusCode != fasthttp.StatusOK {
		fmt.Printf("unexpected status code: %d. Expecting %d\r\n", statusCode, fasthttp.StatusOK)
		return false, 0
	}

	v, err := p.ParseBytes(body)
	if err != nil {
		if flag == "full" {
			fmt.Println("err2 = ", err.Error(), err)
		}
		return false, 0
	}

	betlogs := v.GetArray("data")
	fmt.Println("betlogs===", betlogs)
	fmt.Println("code===", v.GetInt("code"))
	for _, betlog := range betlogs {
		err, rowVersion := pgSgParse(betlog, flag)
		if err != nil {
			if flag == "full" {
				fmt.Println("err3 = ", err.Error(), err)
			}
			continue
		}
		req.RowVersion = rowVersion
	}

	if len(betlogs) >= 999 {
		return true, req.RowVersion
	} else {
		return true, 0
	}
}

func pgSgParse(v *fastjson.Value, flag string) (error, int64) {

	username := string(v.GetStringBytes("playerName"))
	gameCode := strconv.FormatInt(v.GetInt64("gameId"), 10)
	traceId := strconv.FormatInt(v.GetInt64("parentBetId"), 10)
	orderNum := strconv.FormatInt(v.GetInt64("betId"), 10)
	createdAt := v.GetInt64("betTime")
	updatedAt := v.GetInt64("rowVersion")
	betEndTime := v.GetInt64("betEndTime")
	betAmount := v.GetFloat64("betAmount")
	winAmount := v.GetFloat64("winAmount")

	gameName := pgsgm[gameCode]
	info, _ := MemberFindOne(username[3:])
	fmt.Println("info:", info)
	if info.Uid == "" {
		info.Uid = "0"
		return nil, updatedAt
	}

	//info, _ := ryrpc.MemberByName(username[3:])
	//if info.Uid == "" {
	//	info.Uid = "0"
	//	err := ryrpc.MemberFlushByName(username[3:])
	//	fmt.Println(err)
	//	info, _ = ryrpc.MemberByName(username[3:])
	//	if info.Uid == "" {
	//		fmt.Println("uid is null=", username)
	//		return nil, updatedAt
	//	}
	//}
	if info.Uid == "" {
		info.Uid = "0"
	}
	if info.ParentID == "" {
		info.ParentID = "0"
	}
	if info.TopID == "" {
		info.TopName = "0"
	}
	if info.ParentName == "" {
		info.ParentName = ""
	}
	if info.TopName == "" {
		info.TopName = ""
	}
	record := GameRecord{
		Uid:            info.Uid,
		Result:         "Game Name:" + gameName + "|Hand number:" + traceId,
		GameType:       "3",
		GameName:       gameName,
		GameCode:       gameCode,
		ApiType:        16595015200303,
		ApiName:        "PG",
		Flag:           1,
		Handicap:       gameName,
		PlayType:       gameName,
		CreatedAt:      time.Now().UnixMilli(),
		Prefix:         username[:3],
		PlayerName:     username[3:],
		Name:           username[3:],
		BetAmount:      betAmount,
		BetTime:        createdAt,
		ApiBetTime:     createdAt,
		SettleTime:     betEndTime,
		ApiSettleTime:  betEndTime,
		ValidBetAmount: betAmount,
		ApiBillNo:      orderNum,
		BillNo:         orderNum,
		Resettle:       0,
		Presettle:      0,
		RowId:          "pgdy" + orderNum,
		MainBillNo:     orderNum,
	}
	record.ParentUid = info.ParentID
	record.ParentName = info.ParentName
	record.GrandId = info.GrandID
	record.GrandName = info.GrandName
	record.GrantGrantId = info.GreatGrandID
	record.GrantGrantName = info.GreatGrandName
	record.TopUid = info.TopID
	record.TopName = info.TopName
	record.NetAmount, _ = decimal.NewFromFloat(winAmount).Sub(decimal.NewFromFloat(betAmount)).Truncate(4).Float64()

	//入库处理
	if cb, ok := modes[flag]; ok {
		cb("pgdy", record)
	}

	return nil, updatedAt
}
