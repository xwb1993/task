package pull

import (
	"context"
	"database/sql"
	"errors"
	"fmt"
	g "github.com/doug-martin/goqu/v9"
	_ "github.com/doug-martin/goqu/v9/dialect/mysql"
	"github.com/go-redis/redis/v8"
	"github.com/jmoiron/sqlx"
	"net/url"
	"strconv"
	"strings"
	"task/common"
	"task/contrib/conn"
	"task/contrib/helper"
	ryrpc "task/rpc"
	"time"
)

var (
	locUTC, _     = time.LoadLocation("UTC") //(UTC+00:00) 协调世界时
	dialect       = g.Dialect("mysql")
	ctx           = context.Background()
	BetDb         *sqlx.DB
	MerchantRedis *redis.Client
	GameConfig    common.PlatCfg_t
	TIME8         = "2006-01-02T15:04:05.999Z"
	colsMember    = helper.EnumFields(ryrpc.TblMemberBase{})
	loc, _        = time.LoadLocation("America/Sao_Paulo")
)

type callFunc func(path, flag string) bool

var routes = map[string]callFunc{
	"pg":    PGSgReadT, //pg拉注单
	"evo":   EvoReadT,  //evo转账
	"pgone": PGSReadT,  //pg拉注单
}

func Parse(conf common.Conf, platCfg common.PlatCfg_t, flag string) {

	if !strings.Contains(flag, "_") {
		fmt.Println("flag is error , ex : pg_20220222_full")
		return
	}

	GameConfig = platCfg

	// 初始化db
	BetDb = conn.InitDB(conf.Db.Task, conf.Db.MaxIdleConn, conf.Db.MaxIdleConn)
	MerchantRedis = conn.InitRedis(conf.Redis.Addr[0], conf.Redis.Password, 0)
	//MerchantRedis = conn.InitRedisSentinel(conf.Redis.Addr, conf.Redis.Password, conf.Redis.Sentinel, conf.Redis.Db)
	bean := conn.BeanNew(conf.Beanstalkd)
	common.InitBean(bean)
	ryrpc.Constructor(conf.RPC)
	common.New(conf.Proxy)
	// 初始化日志
	common.InitRedis(MerchantRedis)
	ps := strings.Split(flag, "_")
	if cb, ok := routes[ps[0]]; ok {
		cb(ps[1], ps[2])
	}

	defer func() {
		Close()
	}()
}

func Close() {
	BetDb.Close()
	MerchantRedis.Close()
	userMap = nil
}

func paramEncode(args map[string]string) string {

	if len(args) < 1 {
		return ""
	}

	data := url.Values{}
	for k, v := range args {
		data.Set(k, v)
	}
	return data.Encode()
}

type GameRecord struct {
	RowId           string  `json:"row_id" db:"row_id"`
	BillNo          string  `json:"bill_no" db:"bill_no"`
	ApiType         int64   `json:"api_type" db:"api_type"`
	PlayerName      string  `json:"player_name" db:"player_name"`
	Name            string  `json:"name" db:"name"`
	Uid             string  `json:"uid" db:"uid"`
	NetAmount       float64 `json:"net_amount" db:"net_amount"`
	BetTime         int64   `json:"bet_time" db:"bet_time"`
	GameType        string  `json:"game_type" db:"game_type"`
	BetAmount       float64 `json:"bet_amount" db:"bet_amount"`
	ValidBetAmount  float64 `json:"valid_bet_amount" db:"valid_bet_amount"`
	Flag            int     `json:"flag" db:"flag"`
	PlayType        string  `json:"play_type" db:"play_type"`
	LeagueName      string  `json:"league_name" db:"league_name"`
	CompetitionName string  `json:"competition_name" db:"competition_name"`
	PlayContent     string  `json:"play_content" db:"play_content"`
	Prefix          string  `json:"prefix" db:"prefix"`
	Result          string  `json:"result" db:"result"`
	CreatedAt       int64   `json:"created_at" db:"created_at"`
	UpdatedAt       int64   `json:"updated_at" db:"updated_at"`
	ApiName         string  `json:"api_name" db:"api_name"`
	ApiBillNo       string  `json:"api_bill_no" db:"api_bill_no"`
	MainBillNo      string  `json:"main_bill_no" db:"main_bill_no"`
	GameName        string  `json:"game_name" db:"game_name"`
	GameCode        string  `json:"game_code" db:"game_code"`
	HandicapType    string  `json:"handicap_type" db:"handicap_type"`
	Handicap        string  `json:"handicap" db:"handicap"`
	Odds            float64 `json:"odds" db:"odds"`
	SettleTime      int64   `json:"settle_time" db:"settle_time"`
	StartTime       int64   `json:"start_time" db:"start_time"`
	Resettle        int     `json:"resettle" db:"resettle"`
	Presettle       int     `json:"presettle" db:"presettle"`
	ApiBetTime      int64   `json:"api_bet_time" db:"api_bet_time"`
	ApiSettleTime   int64   `json:"api_settle_time" db:"api_settle_time"`
	ParentUid       string  `json:"parent_uid" db:"parent_uid"`
	ParentName      string  `json:"parent_name" db:"parent_name"`
	GrandId         string  `json:"grand_id" db:"grand_id"`
	GrandName       string  `json:"grand_name" db:"grand_name"`
	GrantGrantId    string  `json:"grand_grand_id" db:"grand_grand_id"`
	GrantGrantName  string  `json:"grand_grand_name" db:"grand_grand_name"`
	TopUid          string  `json:"top_uid" db:"top_uid"`
	TopName         string  `json:"top_name" db:"top_name"`
	SettleAmount    float64 `json:"settle_amount" db:"settle_amount"`
	RebateAmount    float64 `json:"rebate_amount" db:"rebate_amount"`
	TableId         string  `json:"table_id" db:"table_id"`       // 桌号
	SetId           string  `json:"set_id" db:"set_id"`           // 靴号
	RoundId         string  `json:"round_id" db:"round_id"`       // 局号
	GameResult      string  `json:"game_result" db:"game_result"` // 游戏结果
	GameTypeZr      string  `json:"game_type_zr"`                 //真人游戏类型
}

type modFunc func(plat string, record GameRecord)

var modes = map[string]modFunc{
	"full":   full,
	"update": update,
}

// 增量拉取入库
func update(plat string, record GameRecord) {

	key := fmt.Sprintf("%s_records", plat)
	timeStr := helper.DayTST(0, loc).Format("2006-01-02 15:04:05")
	success, err := MerchantRedis.Do(ctx, "BF.ADD", key+timeStr, record.RowId+strconv.FormatInt(record.SettleTime, 10)).Bool()
	if err != nil {
		fmt.Printf("%s order pull update  redis error : %v\n", plat, err)
		return
	}

	if !success {
		return
	}
	full(plat, record)
}

// 全量拉取入库
func full(plat string, record GameRecord) {

	// add 非结算注单 输赢和有效投注为0
	if record.Flag != 1 {
		record.NetAmount = 0
		record.ValidBetAmount = 0
	}
	record.Result = strings.ReplaceAll(record.Result, "'", "")
	record.GameName = strings.ReplaceAll(record.GameName, "'", "")
	record.PlayType = strings.ReplaceAll(record.PlayType, "'", "")
	record.Handicap = strings.ReplaceAll(record.Handicap, "'", "")
	record.GameName = strings.ReplaceAll(record.GameName, "'", "")

	query := fmt.Sprintf(`INSERT INTO tbl_game_record (row_id,bill_no,api_type,player_name,name,uid,net_amount,bet_time,game_type,
bet_amount,valid_bet_amount,flag,play_type,prefix,result,created_at,updated_at,api_name,api_bill_no,main_bill_no,
game_name,game_code,handicap_type,handicap,odds,settle_time,start_time,resettle,presettle,api_bet_time,api_settle_time,parent_uid,parent_name,top_uid,top_name,settle_amount,rebate_amount)
VALUES('%s','%s','%d','%s','%s','%s','%f','%d','%s','%f','%f','%d','%s','%s','%s','%d','%d','%s','%s','%s','%s','%s','%s','%s','%f','%d','%d','%d','%d','%d','%d','%s','%s','%s','%s','%f','%f') 
on duplicate key update row_id = '%s',bill_no = '%s',api_type = '%d',player_name = '%s',name = '%s',uid = '%s',net_amount = '%f',bet_time = '%d',game_type = '%s',
bet_amount = '%f',valid_bet_amount = '%f',flag = '%d',play_type = '%s',prefix = '%s',result = '%s',updated_at = '%d',api_name = '%s',api_bill_no = '%s',main_bill_no = '%s',
game_name = '%s',game_code = '%s',handicap_type = '%s',handicap = '%s',odds = '%f',settle_time = '%d',start_time = '%d',resettle = '%d',presettle = '%d',api_bet_time = '%d',api_settle_time = '%d',parent_uid = '%s',parent_name = '%s',top_uid = '%s',top_name = '%s',settle_amount = '%f',rebate_amount = '%f'`,
		record.RowId, record.BillNo, record.ApiType, record.PlayerName, record.Name, record.Uid, record.NetAmount, record.BetTime, record.GameType,
		record.BetAmount, record.ValidBetAmount, record.Flag, record.PlayType, record.Prefix, record.Result, 0, 0, record.ApiName, record.ApiBillNo, record.MainBillNo,
		record.GameName, record.GameCode, record.HandicapType, record.Handicap, record.Odds, record.SettleTime, record.StartTime, record.Resettle, record.Presettle, record.ApiBetTime, record.ApiSettleTime, record.ParentUid, record.ParentName, record.TopUid, record.TopName, record.SettleAmount, record.RebateAmount,
		record.RowId, record.BillNo, record.ApiType, record.PlayerName, record.Name, record.Uid, record.NetAmount, record.BetTime, record.GameType,
		record.BetAmount, record.ValidBetAmount, record.Flag, record.PlayType, record.Prefix, record.Result, 0, record.ApiName, record.ApiBillNo, record.MainBillNo,
		record.GameName, record.GameCode, record.HandicapType, record.Handicap, record.Odds, record.SettleTime, record.StartTime, record.Resettle, record.Presettle, record.ApiBetTime, record.ApiSettleTime, record.ParentUid, record.ParentName, record.TopUid, record.TopName, record.SettleAmount, record.RebateAmount)
	_, err := BetDb.Exec(query)
	fmt.Println(query)
	if err != nil {
		fmt.Printf("%s order pull update  error : %v, sql : %s\n", plat, err, query)
	}

}

var (
	userMap = map[string]ryrpc.TblMemberBase{}
)

func MemberFindOne(username string) (ryrpc.TblMemberBase, error) {

	m := ryrpc.TblMemberBase{}
	if _, ok := userMap[username]; ok {
		m = userMap[username]
	} else {
		ex := g.Ex{}
		if username != "" {
			ex["uid"] = username
		}

		t := dialect.From("tbl_member_base")
		query, _, _ := t.Select(colsMember...).Where(ex).Limit(1).ToSQL()
		fmt.Println(query)
		err := BetDb.Get(&m, query)
		if err != nil && err != sql.ErrNoRows {
			return m, err
		}

		if err == sql.ErrNoRows {
			return m, errors.New(helper.UsernameErr)
		}
		userMap[username] = m
	}

	return m, nil
}
