package upgrade

import (
	"context"
	"fmt"
	g "github.com/doug-martin/goqu/v9"
	_ "github.com/doug-martin/goqu/v9/dialect/mysql"
	"github.com/go-redis/redis/v8"
	"github.com/jmoiron/sqlx"
	"github.com/shopspring/decimal"
	"task/common"
	"task/contrib/conn"
	"task/contrib/helper"
	ryrpc "task/rpc"
	"time"
)

type MetaTable struct {
	WalletMode string
}

var (
	cli        *redis.Client
	db         *sqlx.DB
	ctx        = context.Background()
	dialect    = g.Dialect("mysql")
	upgradeCfg = make(map[int]common.MemberVip)
	meta       *MetaTable
)

func Parse(conf common.Conf, args []string) {
	cli = conn.InitRedis(conf.Redis.Addr[0], conf.Redis.Password, 0)
	//cli = conn.InitRedisSentinel(conf.Redis.Addr, conf.Redis.Password, conf.Redis.Sentinel, conf.Redis.Db)
	db = conn.InitDB(conf.Db.Task, conf.Db.MaxIdleConn, conf.Db.MaxIdleConn)
	bean := conn.BeanNew(conf.Beanstalkd)
	common.InitBean(bean)
	common.InitRedis(cli)
	common.InitDb(db)
	upgradeWork()
}

func upgradeWork() {

	var err error
	upgradeCfg, err = common.MemberVipList()
	if err != nil {
		return
	}

	l := len(upgradeCfg)
	for i := 2; i <= l; i++ {
		mbs, err := common.MemberBaseList(g.Ex{"vip": i - 1, "can_bonus": 1})
		if err != nil {
			return
		}

		for _, mb := range mbs {
			for j := i; j <= l; j++ {
				if !upgradeDo(j, upgradeCfg[j], mb) {
					break
				}
			}
		}
	}
}

func upgradeDo(vip int, cfg common.MemberVip, mb ryrpc.TblMemberBase) bool {

	mDepositAmount := decimal.NewFromFloat(mb.DepositAmount)
	vDepositAmount := decimal.NewFromInt(cfg.DepositAmount)
	// 存款不满足条件
	if vDepositAmount.GreaterThan(mDepositAmount) {
		return false
	}

	mScore := decimal.NewFromFloat(mb.Score)
	vScore := decimal.NewFromInt(cfg.Flow)
	// 流水不满足条件
	if vScore.GreaterThan(mScore) {
		return false
	}

	mKeepScore := decimal.NewFromFloat(mb.KeepScore)
	vKeepFlow := decimal.NewFromInt(cfg.KeepFlow)
	// 达到降级条件
	if vKeepFlow.GreaterThan(mKeepScore) {
		return false
	}

	balance, _ := common.MemberBalance(mb.Uid)
	amount, _ := decimal.NewFromString(cfg.Amount)
	balanceAfter := balance.Add(amount)

	tx, err := db.Begin()
	if err != nil {
		common.Log("upgrade", "db.Begin() error,[%s]", err.Error())
		return false
	}

	ex := g.Ex{
		"uid": mb.Uid,
	}
	record := g.Record{
		"vip": g.L("vip+1"),
	}
	query, _, _ := dialect.Update("tbl_member_base").Set(record).Where(ex).ToSQL()
	fmt.Println(query)
	_, err = tx.Exec(query)
	if err != nil {
		_ = tx.Rollback()
		common.Log("upgrade", "%s,[%s]", err.Error(), query)
		return false
	}

	id := helper.GenId()
	trans := common.MemberTransaction{
		AfterAmount:  balanceAfter.String(),
		Amount:       amount.String(),
		BeforeAmount: balance.String(),
		BillNo:       id,
		CreatedAt:    time.Now().UnixMilli(),
		ID:           id,
		CashType:     helper.TransactionVIPUpgrade,
		UID:          mb.Uid,
		Username:     mb.Username,
		PlatformID:   "0",
		Remark:       fmt.Sprintf("vip%d upgrade bonus", vip),
	}
	query, _, _ = dialect.Insert("tbl_balance_transaction").Rows(&trans).ToSQL()
	fmt.Println(query)
	_, err = tx.Exec(query)
	if err != nil {
		_ = tx.Rollback()
		common.Log("upgrade", "%s,[%s]", err.Error(), query)
		return false
	}

	record = g.Record{
		"brl": g.L(fmt.Sprintf("brl+%s", amount.String())),
	}
	if meta.WalletMode == "1" {
		record = g.Record{
			"brl": g.L(fmt.Sprintf("brl+%s", amount.String())),
		}
	} else if meta.WalletMode == "2" {
		record = g.Record{
			"brl":           g.L(fmt.Sprintf("brl+%s", amount.String())),
			"unlock_amount": g.L(fmt.Sprintf("unlock_amount+%s", amount.String())),
		}
	}
	query, _, _ = dialect.Update("tbl_member_balance").Set(record).Where(ex).ToSQL()
	fmt.Println(query)
	_, err = tx.Exec(query)
	if err != nil {
		_ = tx.Rollback()
		common.Log("upgrade", "%s,[%s]", err.Error(), query)
		return false
	}

	err = tx.Commit()
	if err != nil {
		common.Log("upgrade", "tx.Commit() error,[%s]", err.Error())
		return false
	}

	return true
}
