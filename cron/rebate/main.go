package rebate

import (
	"context"
	"fmt"
	g "github.com/doug-martin/goqu/v9"
	_ "github.com/doug-martin/goqu/v9/dialect/mysql"
	"github.com/doug-martin/goqu/v9/exp"
	"github.com/go-redis/redis/v8"
	"github.com/jmoiron/sqlx"
	"github.com/shopspring/decimal"
	"task/common"
	"task/contrib/conn"
	"task/contrib/helper"
	"time"
)

var (
	loc, _          = time.LoadLocation("America/Sao_Paulo")
	cli             *redis.Client
	db              *sqlx.DB
	ctx             = context.Background()
	dialect         = g.Dialect("mysql")
	thousand        = decimal.NewFromFloat(1000)
	colsGameRecord  = helper.EnumFields(common.GameRecord{})
	colsBonusConfig = helper.EnumFields(common.BonusConfig{})
	rebateTopic     string
	rebateCashTypes = map[int]bool{
		helper.TransactionRebate:       true,
		helper.TransactionSubRebateLv1: true,
		helper.TransactionSubRebateLv2: true,
		helper.TransactionSubRebateLv3: true,
	}
)

func Parse(conf common.Conf, args []string) {

	//cli = conn.InitRedisSentinel(conf.Redis.Addr, conf.Redis.Password, conf.Redis.Sentinel, conf.Redis.Db)
	cli = conn.InitRedis(conf.Redis.Addr[0], conf.Redis.Password, 0)
	db = conn.InitDB(conf.Db.Task, conf.Db.MaxIdleConn, conf.Db.MaxIdleConn)
	bean := conn.BeanNew(conf.Beanstalkd)
	common.InitBean(bean)
	common.InitRedis(cli)
	common.InitDb(db)
	rebateTopic = conf.Prefix + "_venue_rebate"

	switch args[4] {
	case "gameRecord":
		rebateGameRecord()
	case "rebateRecord":
		rebateGenRecord()
	case "rebateTransaction":
		rebateTransaction()
	}
}

// 将注单从数据库取出，写入redis list
func rebateGameRecord() {

	var (
		data  []string
		count uint
	)
	ts := time.Now()
	ts = ts.Add(-24 * time.Hour)
	st := time.Date(ts.Year(), ts.Month(), ts.Day(), 0, 0, 0, 0, loc).UnixMilli()
	et := time.Date(ts.Year(), ts.Month(), ts.Day(), 23, 59, 59, 999, loc).UnixMilli()
	ex := g.Ex{
		"flag":     1,
		"bet_time": g.Op{"between": exp.NewRangeVal(st, et)},
	}
	query, _, _ := dialect.From("tbl_game_record").Select(g.COUNT("row_id")).Where(ex).ToSQL()
	fmt.Println(query)
	err := db.Get(&count, query)
	if err != nil {
		return
	}

	if count == 0 {
		return
	}

	p := count / 1000
	l := count % 1000
	if l > 0 {
		p += 1
	}

	for j := uint(1); j <= p; j++ {
		offset := (p - 1) * 1000
		query, _, _ = dialect.From("tbl_game_record").
			Select("row_id").Where(ex).Offset(offset).Limit(1000).Order(g.C("row_id").Asc()).ToSQL()
		fmt.Println(query)
		err = db.Select(&data, query)
		if err != nil {
			common.Log("rebate", "%s,[%s]", err.Error(), query)
			return
		}

		for _, v := range data {
			_ = cli.LPush(ctx, rebateTopic, v).Err()
		}
	}
}

// 生成返水记录
func rebateGenRecord() {
	for {
		res, err := cli.BRPop(ctx, time.Duration(15)*time.Second, rebateTopic).Result()
		if err != nil {
			if err != redis.Nil {
				common.Log("rebate", "%s", err.Error())
			}
			continue
		}

		venue(res[1])
	}
}

func venue(rowID string) {

	data := common.GameRecord{}
	query, _, _ := dialect.From("tbl_game_record").Select(colsGameRecord...).Where(g.Ex{"row_id": rowID}).ToSQL()
	fmt.Println(query)
	err := db.Get(&data, query)
	if err != nil {
		common.Log("rebate", "%s,[%s]", err.Error(), query)
		return
	}

	cfg := common.BonusConfig{}
	query, _, _ = dialect.From("tbl_bonus_config").Select(colsBonusConfig...).Where(g.Ex{"code": data.ApiType}).ToSQL()
	fmt.Println(query)
	err = db.Get(&data, query)
	if err != nil {
		common.Log("rebate", "%s,[%s]", err.Error(), query)
		return
	}

	m, err := common.MemberFindOneByUid(data.Uid)
	if err != nil {
		common.Log("rebate", "%s", err.Error())
		return
	}

	if m.CanBonus != 1 {
		return
	}

	vip, err := common.MemberVipFindOne(m.Vip)
	if err != nil {
		common.Log("rebate", "%s", err.Error())
		return
	}

	tx, err := db.Begin()
	if err != nil {
		common.Log("rebate", "%s", err.Error())
		return
	}

	//会员自身返水
	rebateRate, _ := decimal.NewFromString(vip.RebateRate)
	// 取有效投注
	validBetAmount := decimal.NewFromFloat(data.ValidBetAmount)
	if rebateRate.GreaterThan(decimal.Zero) {
		rebateAmount := validBetAmount.Mul(rebateRate).Div(thousand)
		rr := common.RebateRecord{
			ID:           helper.GenId(),
			RowId:        data.RowId,               //注单表row_id
			Uid:          m.ParentID,               //用户ID(返水会员uid)
			Username:     m.ParentName,             //用户名(返水会员名)
			SubUid:       m.Uid,                    //下级用户ID(投注会员uid)
			SubName:      m.Username,               //下级用户名(投注会员名)
			PlatformId:   data.ApiType,             //场馆id
			CashType:     helper.TransactionRebate, //201 投注返水
			Amount:       validBetAmount.String(),  //投注金额
			Rate:         vip.RebateRate,           //返水比例
			RebateAmount: rebateAmount.String(),    //返水金额
			SettleTime:   data.SettleTime,          //注单结算时间
			CreatedAt:    time.Now().UnixMilli(),   //返水比例
		}
		query, _, _ = dialect.Insert("tbl_rebate_record").Rows(&rr).ToSQL()
		fmt.Println(query)
		_, err = tx.Exec(query)
		if err != nil {
			_ = tx.Rollback()
			common.Log("rebate", "%s,[%s]", err.Error(), query)
			return
		}
	}

	// 1级代理返点
	if m.ParentID != "0" {
		rateLv1, _ := decimal.NewFromString(cfg.LvlOneRebate)
		rebateAmount := validBetAmount.Mul(rateLv1).Div(thousand)
		rr := common.RebateRecord{
			ID:           helper.GenId(),
			RowId:        data.RowId,                     //注单表row_id
			Uid:          m.ParentID,                     //用户ID(返水会员uid)
			Username:     m.ParentName,                   //用户名(返水会员名)
			SubUid:       m.Uid,                          //下级用户ID(投注会员uid)
			SubName:      m.Username,                     //下级用户名(投注会员名)
			PlatformId:   data.ApiType,                   //场馆id
			CashType:     helper.TransactionSubRebateLv1, //203 下级返水（1级）
			Amount:       validBetAmount.String(),        //投注金额
			Rate:         rateLv1.String(),               //返水比例
			RebateAmount: rebateAmount.String(),          //返水金额
			SettleTime:   data.SettleTime,                //注单结算时间
			CreatedAt:    time.Now().UnixMilli(),         //返水比例
		}
		query, _, _ = dialect.Insert("tbl_rebate_record").Rows(&rr).ToSQL()
		fmt.Println(query)
		_, err = tx.Exec(query)
		if err != nil {
			_ = tx.Rollback()
			common.Log("rebate", "%s,[%s]", err.Error(), query)
			return
		}

		// 2级代理返点
		if m.GrandID != "0" {
			rateLv2, _ := decimal.NewFromString(cfg.LvlTwoRebate)
			rebateAmount = validBetAmount.Mul(rateLv2).Div(thousand)
			rr.ID = helper.GenId()
			rr.Uid = m.GrandID                           //用户ID(返水会员uid)
			rr.Username = m.GrandName                    //用户名(返水会员名)
			rr.CashType = helper.TransactionSubRebateLv2 //204 下级返水（2级）
			rr.Rate = rateLv2.String()                   //返水比例
			rr.RebateAmount = rebateAmount.String()      //返水金额
			query, _, _ = dialect.Insert("tbl_rebate_record").Rows(&rr).ToSQL()
			fmt.Println(query)
			_, err = tx.Exec(query)
			if err != nil {
				_ = tx.Rollback()
				common.Log("rebate", "%s,[%s]", err.Error(), query)
				return
			}

			//3级代理返点
			if m.GreatGrandID != "0" {
				rateLv3, _ := decimal.NewFromString(cfg.LvlThreeRebate)
				rebateAmount = validBetAmount.Mul(rateLv3).Div(thousand)
				rr.ID = helper.GenId()
				rr.Uid = m.GreatGrandID                      //用户ID(返水会员uid)
				rr.Username = m.GreatGrandName               //用户名(返水会员名)
				rr.CashType = helper.TransactionSubRebateLv3 //205 下级返水（3级）
				rr.Rate = rateLv3.String()                   //返水比例
				rr.RebateAmount = rebateAmount.String()      //返水金额
				query, _, _ = dialect.Insert("tbl_rebate_record").Rows(&rr).ToSQL()
				fmt.Println(query)
				_, err = tx.Exec(query)
				if err != nil {
					_ = tx.Rollback()
					common.Log("rebate", "%s,[%s]", err.Error(), query)
					return
				}
			}
		}
	}

	err = tx.Commit()
	if err != nil {
		common.Log("rebate", "%s", err.Error())
		return
	}
}

// 写入返水帐变
func rebateTransaction() {

	ts := time.Now()
	sts := ts.Add(24 * 7 * time.Hour)
	ymd := ts.Format("2006-01-02")
	st := time.Date(sts.Year(), sts.Month(), sts.Day(), 0, 0, 0, 0, loc)
	et := time.Date(ts.Year(), ts.Month(), ts.Day(), 23, 59, 59, 999, loc)
	ex := g.Ex{
		"created_at": g.Op{"between": g.Range(st.In(loc).UnixMilli(), et.In(loc).UnixMilli())},
	}
	for k := range helper.BRPlatforms {
		ex["platform_id"] = k
		for kk := range rebateCashTypes {
			ex["cash_type"] = kk
			var data []common.RebateTransaction
			query, _, _ := dialect.From("tbl_rebate_record").Select(g.SUM("rebate_amount").As("rebate_amount"), g.C("uid")).Where(ex).GroupBy(g.C("uid")).ToSQL()
			fmt.Println(query)
			err := db.Select(&data, query)
			if err != nil {
				common.Log("rebate", "%s,[%s]", err.Error(), query)
				continue
			}

			if len(data) == 0 {
				return
			}

			for kkk, vvv := range data {
				m, err := common.MemberFindOneByUid(vvv.UID)
				if err != nil {
					common.Log("rebate", "%s", err.Error())
					continue
				}

				if m.CanBonus != 1 {
					continue
				}

				balance, _ := common.MemberBalance(vvv.UID)
				bonus, _ := decimal.NewFromString(vvv.RebateAmount)
				afterAmount := balance.Add(bonus).String()
				tx, err := db.Begin()
				if err != nil {
					common.Log("rebate", "%s", err.Error())
					continue
				}

				record := common.MemberTransaction{
					AfterAmount:  afterAmount,
					Amount:       bonus.String(),
					BeforeAmount: balance.String(),
					BillNo:       ymd,
					CreatedAt:    time.Now().In(loc).UnixMilli() + int64(kkk),
					ID:           helper.GenId(),
					CashType:     kk,
					UID:          m.Uid,
					PlatformID:   k,
					Username:     m.Username,
					Tester:       m.Tester,
				}
				query, _, _ := dialect.Insert("tbl_balance_transaction").Rows(record).ToSQL()
				fmt.Println(query)
				_, err = tx.Exec(query)
				if err != nil {
					common.Log("rebate", "%s,[%s]", err.Error(), query)
					_ = tx.Rollback()
					continue
				}

				recs := g.Record{
					"brl": g.L(fmt.Sprintf("brl+%s", bonus.String())),
				}
				query, _, _ = dialect.Update("tbl_member_balance").Set(recs).Where(g.Ex{"uid": m.Uid}).ToSQL()
				fmt.Println(query)
				_, err = tx.Exec(query)
				if err != nil {
					common.Log("rebate", "%s,[%s]", err.Error(), query)
					_ = tx.Rollback()
					continue
				}

				err = tx.Commit()
				if err != nil {
					common.Log("rebate", "%s", err.Error())
				}
			}
		}
	}
}
