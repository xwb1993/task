package report

import (
	"database/sql"
	"errors"
	"fmt"
	g "github.com/doug-martin/goqu/v9"
	"github.com/shopspring/decimal"
	"task/common"
	"task/contrib/helper"
	ryrpc "task/rpc"
	"time"
)

var (
	colsPromoInvite = helper.EnumFields(common.TblPromoInviteRecord{})
)

func updateAgencyRebate(date, flag string) {

	common.InitDb(BetDb)
	var list []common.TblPromoInviteRecord
	//发放待结算的邀请奖励
	ts := time.Now().Unix()
	query, _, _ := dialect.From("tbl_promo_invite_record").Select(colsPromoInvite...).Where(g.Ex{"state": 2, "first_deposit_at": g.Op{"lte": ts - 1800}}).ToSQL()
	fmt.Println(query)
	err := BetDb.Select(&list, query)
	if err != nil {
		fmt.Println(err)
		return
	}

	for _, v := range list {

		mb, err := MemberFindOne(v.ChildUid)
		if err != nil {
			continue
		}

		tx, err := BetDb.Begin()
		if err != nil {
			fmt.Println(err)
			return
		}
		ex := g.Ex{
			"child_uid": mb.Uid,
		}

		record := g.Record{
			"state":      3,
			"settled_at": time.Now().Unix(),
		}
		query, _, _ = dialect.Update("tbl_promo_invite_record").Set(record).Where(ex).ToSQL()
		fmt.Println(query)
		_, err = tx.Exec(query)
		if err != nil {
			_ = tx.Rollback()
			fmt.Println(err)
			return
		}

		if mb.ParentID != "0" {
			exparent := g.Ex{
				"uid": mb.ParentID,
			}

			brparent := g.Record{
				"brl":                g.L(fmt.Sprintf("brl+%f", v.BonusAmount)),
				"agency_amount":      g.L(fmt.Sprintf("agency_amount-%f", v.BonusAmount)),
				"agency_lock_amount": g.L(fmt.Sprintf("agency_lock_amount-%f", v.BonusAmount)),
			}
			query, _, _ = dialect.Update("tbl_member_balance").Set(brparent).Where(exparent).ToSQL()
			fmt.Println(query)
			_, err = tx.Exec(query)
			if err != nil {
				_ = tx.Rollback()
				fmt.Println(err)
				return
			}

			bl, _ := common.MemberBalance(mb.ParentID)
			// 4、新增账变记录
			parentTrans := MemberTransaction{
				AfterAmount:  bl.Add(decimal.NewFromFloat(v.BonusAmount)).StringFixed(2),
				Amount:       v.BonusAmount,
				BeforeAmount: bl.StringFixed(2),
				BillNo:       helper.GenId(),
				CreatedAt:    time.Now().UnixMilli(),
				ID:           helper.GenId(),
				CashType:     helper.TransactionInviteDividend,
				UID:          mb.ParentID,
				Username:     mb.ParentName,
				Remark:       "deposit for first parent by " + mb.Username,
			}

			query, _, _ = dialect.Insert("tbl_balance_transaction").Rows(parentTrans).ToSQL()
			fmt.Println(query)
			_, err = tx.Exec(query)
			if err != nil {
				_ = tx.Rollback()
				fmt.Println(err)
				return
			}
		}
		//if mb.GrandID != "0" {
		//	exparent := g.Ex{
		//		"uid": mb.GrandID,
		//	}
		//
		//	brparent := g.Record{
		//		"brl":                g.L(fmt.Sprintf("brl+%f", v.BonusAmount)),
		//		"agency_amount":      g.L(fmt.Sprintf("agency_amount-%f", v.BonusAmount)),
		//		"agency_lock_amount": g.L(fmt.Sprintf("agency_lock_amount-%f", v.BonusAmount)),
		//	}
		//	query, _, _ = dialect.Update("tbl_member_balance").Set(brparent).Where(exparent).ToSQL()
		//	fmt.Println(query)
		//	_, err = tx.Exec(query)
		//	if err != nil {
		//		_ = tx.Rollback()
		//		fmt.Println(err)
		//		return
		//	}
		//
		//	bl, _ := common.MemberBalance(mb.GrandID)
		//	// 4、新增账变记录
		//	parentTrans := MemberTransaction{
		//		AfterAmount:  bl.Add(decimal.NewFromFloat(v.BonusAmount)).StringFixed(2),
		//		Amount:       v.BonusAmount,
		//		BeforeAmount: bl.StringFixed(2),
		//		BillNo:       helper.GenId(),
		//		CreatedAt:    time.Now().UnixMilli(),
		//		ID:           helper.GenId(),
		//		CashType:     helper.TransactionInviteDividend,
		//		UID:          mb.GrandID,
		//		Username:     mb.GrandName,
		//		Remark:       "deposit for first grant by " + mb.Username,
		//	}
		//
		//	query, _, _ = dialect.Insert("tbl_balance_transaction").Rows(parentTrans).ToSQL()
		//	fmt.Println(query)
		//	_, err = tx.Exec(query)
		//	if err != nil {
		//		_ = tx.Rollback()
		//		fmt.Println(err)
		//		return
		//	}
		//}
		//if mb.GreatGrandID != "0" {
		//	exparent := g.Ex{
		//		"uid": mb.GreatGrandID,
		//	}
		//
		//	brparent := g.Record{
		//		"brl":                g.L(fmt.Sprintf("brl+%f", v.BonusAmount)),
		//		"agency_amount":      g.L(fmt.Sprintf("agency_amount-%f", v.BonusAmount)),
		//		"agency_lock_amount": g.L(fmt.Sprintf("agency_lock_amount-%f", v.BonusAmount)),
		//	}
		//	query, _, _ = dialect.Update("tbl_member_balance").Set(brparent).Where(exparent).ToSQL()
		//	fmt.Println(query)
		//	_, err = tx.Exec(query)
		//	if err != nil {
		//		_ = tx.Rollback()
		//		fmt.Println(err)
		//		return
		//	}
		//
		//	bl, _ := common.MemberBalance(mb.GreatGrandID)
		//	// 4、新增账变记录
		//	parentTrans := MemberTransaction{
		//		AfterAmount:  bl.Add(decimal.NewFromFloat(v.BonusAmount)).StringFixed(2),
		//		Amount:       v.BonusAmount,
		//		BeforeAmount: bl.StringFixed(2),
		//		BillNo:       helper.GenId(),
		//		CreatedAt:    time.Now().UnixMilli(),
		//		ID:           helper.GenId(),
		//		CashType:     helper.TransactionInviteDividend,
		//		UID:          mb.GreatGrandID,
		//		Username:     mb.GreatGrandName,
		//		Remark:       "deposit for first grantgrant by " + mb.Username,
		//	}
		//
		//	query, _, _ = dialect.Insert("tbl_balance_transaction").Rows(parentTrans).ToSQL()
		//	fmt.Println(query)
		//	_, err = tx.Exec(query)
		//	if err != nil {
		//		_ = tx.Rollback()
		//		fmt.Println(err)
		//		return
		//	}
		//}

		err = tx.Commit()
		if err != nil {
			fmt.Println(err)
			return
		}
	}

	//统计返水更新到代理余额

}

var (
	userMap = map[string]ryrpc.TblMemberBase{}
)

func MemberFindOne(uid string) (ryrpc.TblMemberBase, error) {

	m := ryrpc.TblMemberBase{}
	if _, ok := userMap[uid]; ok {
		m = userMap[uid]
	} else {
		ex := g.Ex{}
		if uid != "" {
			ex["uid"] = uid
		}

		t := dialect.From("tbl_member_base")
		query, _, _ := t.Select(colsMember...).Where(ex).Limit(1).ToSQL()
		fmt.Println(query)
		err := BetDb.Get(&m, query)
		if err != nil && err != sql.ErrNoRows {
			return m, err
		}

		if err == sql.ErrNoRows {
			return m, errors.New(helper.UsernameErr)
		}
		userMap[uid] = m
	}

	return m, nil
}
