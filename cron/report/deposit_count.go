package report

import (
	"fmt"
	g "github.com/doug-martin/goqu/v9"
	"task/common"
	"time"
)

type DepositOrder struct {
	Uid       string  `json:"uid" db:"uid"`
	Amount    float64 `json:"amount" db:"amount"`
	ConfirmAt int     `json:"confirm_at" db:"confirm_at"`
	Id        string  `json:"id" db:"id"`
}

type MemberDepositInfo struct {
	Uid           string  `json:"uid" db:"uid"`
	DepositAmount float64 `json:"deposit_amount" db:"deposit_amount"`
	DepositAt     int     `json:"deposit_at" db:"deposit_at"`
	Flags         int     `json:"flags" db:"flags"`
}

func depositcount(date, flag string) {

	// 处理中的存款订单1天之后变为失败
	var data []DepositOrder
	var temp []MemberDepositInfo
	var uids []string
	tempMap := map[string]MemberDepositInfo{}
	now := time.Now().Unix()
	ex := g.Ex{
		"state":  362,
		"tester": []int{1, 3},
	}
	if flag == "update" {
		ex["confirm_at"] = g.Op{"gt": now - 1*60*60}
	}
	if flag == "again" {
		ex["confirm_at"] = g.Op{"gt": now - 6*60*60}
	}
	query, _, _ := dialect.From("tbl_deposit").Select(g.C("id"), g.C("uid"), g.C("amount"), g.C("confirm_at")).Where(ex).Order(g.C("confirm_at").Asc()).ToSQL()
	fmt.Println(query)
	err := BetDb.Select(&data, query)
	if err != nil {
		common.Log("deposit", "finance execute query [%s] error: [%v]", query, err)
	}

	ex1 := g.Ex{
		"created_at": g.Op{"lt": now - 86400},
		"state":      361,
	}
	record := g.Record{"state": 363}
	query, _, _ = dialect.Update("tbl_deposit").Set(record).Where(ex1).ToSQL()
	_, err = BetDb.Exec(query)
	if err != nil {
		common.Log("deposit", "finance execute query [%s] error: [%v]", query, err)
	}

	if len(data) == 0 {
		return
	}
	for _, v := range data {
		uids = append(uids, v.Uid)
	}
	query, _, _ = dialect.From("tbl_member_deposit_info").Select(g.C("uid"), g.C("deposit_amount"), g.C("deposit_at"), g.C("flags")).Where(g.Ex{"uid": uids}).Order(g.C("flags").Asc()).ToSQL()
	err = BetDb.Select(&temp, query)
	fmt.Println(query)
	if err != nil {
		common.Log("deposit", "finance execute query [%s] error: [%v]", query, err)
	}

	for _, v := range temp {

		i, ok := tempMap[v.Uid]
		if !ok || v.Flags > i.Flags {
			tempMap[v.Uid] = v
		}
	}

	for _, v := range data {
		records := g.Record{
			"uid":            v.Uid,
			"deposit_amount": v.Amount,
			"deposit_at":     v.ConfirmAt,
			"flags":          0,
		}
		i, ok := tempMap[v.Uid]
		//fmt.Println("tempMap:", tempMap)
		if !ok {
			//从没有存款次数的数据
			query, _, _ = dialect.Insert("tbl_member_deposit_info").Rows(records).ToSQL()
			fmt.Println(query)
			_, err = BetDb.Exec(query)
			if err != nil {
				common.Log("deposit", "finance execute query [%s] error: [%v]", query, err)
			}

			records["flags"] = 1
			query, _, _ = dialect.Insert("tbl_member_deposit_info").Rows(records).ToSQL()
			fmt.Println(query)
			_, err = BetDb.Exec(query)
			if err != nil {
				common.Log("deposit", "finance execute query [%s] error: [%v]", query, err)
			}

			query, _, _ = dialect.Update("tbl_deposit").Set(g.Record{"success_time": 1}).Where(g.Ex{"id": v.Id}).ToSQL()
			fmt.Println(query)
			_, err = BetDb.Exec(query)
			if err != nil {
				common.Log("deposit", "finance execute query [%s] error: [%v]", query, err)
			}
			tempMap[v.Uid] = MemberDepositInfo{
				Uid:           v.Uid,
				DepositAmount: v.Amount,
				DepositAt:     v.ConfirmAt,
				Flags:         1,
			}

		} else {
			query, _, _ = dialect.Update("tbl_member_deposit_info").Set(g.Record{"deposit_amount": v.Amount, "flags": 0}).ToSQL()
			fmt.Println(query)
			_, err = BetDb.Exec(query)
			if err != nil {
				common.Log("deposit", "finance execute query [%s] error: [%v]", query, err)
			}

			if v.ConfirmAt > i.DepositAt {
				records["flags"] = i.Flags + 1
				query, _, _ = dialect.Insert("tbl_member_deposit_info").Rows(records).ToSQL()
				fmt.Println(query)
				_, err = BetDb.Exec(query)
				if err != nil {
					common.Log("deposit", "finance execute query [%s] error: [%v]", query, err)
				}

				query, _, _ = dialect.Update("tbl_deposit").Set(g.Record{"success_time": i.Flags + 1}).Where(g.Ex{"id": v.Id}).ToSQL()
				fmt.Println(query)
				_, err = BetDb.Exec(query)
				if err != nil {
					common.Log("deposit", "finance execute query [%s] error: [%v]", query, err)
				}

				tempMap[v.Uid] = MemberDepositInfo{
					Uid:           v.Uid,
					DepositAmount: v.Amount,
					DepositAt:     v.ConfirmAt,
					Flags:         i.Flags + 1,
				}
			}
		}
	}

}
