package report

import (
	"database/sql"
	"errors"
	"fmt"
	g "github.com/doug-martin/goqu/v9"
	"github.com/doug-martin/goqu/v9/exp"
	"github.com/shopspring/decimal"
	"sort"
	"strconv"
	"task/contrib/helper"
	ryrpc "task/rpc"
	"time"
)

var (
	colsFlowConfig      = helper.EnumFields(TblFlowConfig{})
	colsDeposit         = helper.EnumFields(tblDeposit{})
	colsTransaction     = helper.EnumFields(MemberTransaction{})
	colsPromoInspection = helper.EnumFields(TblPromoInspection{})
)

type TblPromoInspection struct {
	Id               string          `json:"id" db:"id" `
	Uid              string          `json:"uid" db:"uid"`
	Username         string          `json:"username" db:"username"`
	ParentId         string          `json:"parent_id" db:"parent_id"`
	ParentName       string          `json:"parent_name" db:"parent_name"`
	Level            int             `json:"level" db:"level"`
	Ty               int             `json:"ty" db:"ty"`
	State            int             `json:"state" db:"state"`
	CapitalAmount    float64         `json:"capital_amount" db:"capital_amount"`
	FlowMultiple     float64         `json:"flow_multiple" db:"flow_multiple"`
	FlowAmount       float64         `json:"flow_amount" db:"flow_amount"`
	FinishedAmount   float64         `json:"finished_amount" db:"finished_amount"`
	UnfinishedAmount float64         `json:"unfinished_amount" db:"unfinished_amount"`
	CreatedAt        int64           `json:"created_at" db:"created_at"`
	StartAt          int64           `json:"start_at" db:"start_at"`
	FinishAt         int64           `json:"finish_at" db:"finish_at"`
	BillNo           string          `json:"bill_no" db:"bill_no"`
	Remark           string          `json:"remark" db:"remark"`
	UFA              decimal.Decimal `json:"ufa" db:"-"`
}

// 账变表
type MemberTransaction struct {
	AfterAmount  string  `db:"after_amount" json:"after_amount" cbor:"after_amount"`    //账变后的金额
	Amount       float64 `db:"amount" json:"amount" cbor:"amount"`                      //用户填写的转换金额
	BeforeAmount string  `db:"before_amount" json:"before_amount" cbor:"before_amount"` //账变前的金额
	BillNo       string  `db:"bill_no" json:"bill_no" cbor:"bill_no"`                   //转账|充值|提现ID
	CashType     int     `db:"cash_type" json:"cash_type" cbor:"cash_type"`             //
	CreatedAt    int64   `db:"created_at" json:"created_at" cbor:"created_at"`          //
	ID           string  `db:"id" json:"id" cbor:"id"`                                  //
	UID          string  `db:"uid" json:"uid" cbor:"uid"`                               //用户ID
	Username     string  `db:"username" json:"username" cbor:"username"`                //用户名
	Remark       string  `db:"remark" json:"remark" cbor:"remark"`                      //备注
}

type tblDeposit struct {
	Id           string  `json:"id" db:"id" cbor:"id"`
	Oid          string  `json:"oid" db:"oid" cbor:"oid"`
	Uid          string  `json:"uid" db:"uid" cbor:"uid"`
	ParentId     string  `json:"parent_id" db:"parent_id" cbor:"parent_id"`
	ParentName   string  `json:"parent_name" db:"parent_name" cbor:"parent_name"`
	Username     string  `json:"username" db:"username" cbor:"username"`
	Fid          string  `json:"fid" db:"fid" cbor:"fid"`
	Fname        string  `json:"fname" db:"fname" cbor:"fname"`
	Amount       float64 `json:"amount" db:"amount" cbor:"amount"`
	State        int     `json:"state" db:"state" cbor:"state"`
	CreatedAt    int64   `json:"created_at" db:"created_at" cbor:"created_at"`
	CreatedUid   string  `json:"created_uid" db:"created_uid" cbor:"created_uid"`
	CreatedName  string  `json:"created_name" db:"created_name" cbor:"created_name"`
	ConfirmAt    int64   `json:"confirm_at" db:"confirm_at" cbor:"confirm_at"`
	ConfirmUid   string  `json:"confirm_uid" db:"confirm_uid" cbor:"confirm_uid"`
	ConfirmName  string  `json:"confirm_name" db:"confirm_name" cbor:"confirm_name"`
	ReviewRemark string  `json:"review_remark" db:"review_remark" cbor:"review_remark"`
	TopId        string  `json:"top_id" db:"top_id" cbor:"top_id"`
	TopName      string  `json:"top_name" db:"top_name" cbor:"top_name"`
	Level        int     `json:"level" db:"level" cbor:"level"`
	Discount     float64 `json:"discount" db:"discount" cbor:"discount"`
	Tester       int     `json:"tester" db:"tester" cbor:"tester"`
	SuccessTime  int     `json:"success_time" db:"success_time" cbor:"success_time"`
}

type TblFlowConfig struct {
	Id           string  `json:"id"  cbor:"id" db:"id"`
	Name         string  `json:"name" cbor:"name" db:"name"`
	Ty           int     `json:"ty" cbor:"ty" db:"ty"`
	FlowMultiple float64 `json:"flow_multiple" cbor:"flow_multiple" db:"flow_multiple"`
}

func updateFlow(date, flag string) {

	var memberList []ryrpc.TblMemberBase
	var flowConfig []TblFlowConfig
	flowCfgMap := map[int]TblFlowConfig{}

	ex := g.Ex{
		"tester": []int{1, 3},
	}
	if date != "0" {
		ex["uid"] = date
	}
	query, _, _ := dialect.From("tbl_member_base").Select(colsMember...).Where(ex).ToSQL()
	fmt.Println(query)
	err := BetDb.Select(&memberList, query)
	if err != nil {
		fmt.Println(err)
		return
	}

	query, _, _ = dialect.From("tbl_flow_config").Select(colsFlowConfig...).ToSQL()
	fmt.Println(query)
	err = BetDb.Select(&flowConfig, query)
	if err != nil && err != sql.ErrNoRows {
		return
	}
	//首充	0
	//充值	1
	//首充赠金	2
	//充值赠金	3
	//签到	4
	//VIP升级	5
	//邀请盒子	6
	//周投注活动	7
	//邀请奖金	8
	//流水奖金	9
	//手动彩金	10
	for _, v := range flowConfig {
		flowCfgMap[v.Ty] = v
	}

	for _, v := range memberList {
		var lastWithdrawAt sql.NullInt64
		var depositList []tblDeposit
		var promoList []MemberTransaction
		var reocrds []TblPromoInspection
		var result []TblPromoInspection
		tempMap := map[string]TblPromoInspection{}

		query, _, _ = dialect.From("tbl_withdraw").Select(g.MAX("withdraw_at")).Where(g.Ex{"state": 374, "uid": v.Uid}).ToSQL()
		fmt.Println(query)
		err = BetDb.Get(&lastWithdrawAt, query)
		if err != nil {
			fmt.Println(err)
			return
		}

		query, _, _ = dialect.From("tbl_deposit").Select(colsDeposit...).Where(g.Ex{"state": []int{362, 365}, "uid": v.Uid, "confirm_at": g.Op{"gte": lastWithdrawAt.Int64}}).ToSQL()
		fmt.Println(query)
		err = BetDb.Select(&depositList, query)
		if err != nil {
			fmt.Println(err)
			return
		}

		query, _, _ = dialect.From("tbl_balance_transaction").Select(colsTransaction...).Where(g.Ex{"cash_type": []int{301, 306, 307, 308, 309, 310, 311}, "uid": v.Uid, "created_at": g.Op{"gte": lastWithdrawAt.Int64 * 1000}}).ToSQL()
		fmt.Println(query)
		err = BetDb.Select(&promoList, query)
		if err != nil {
			fmt.Println(err)
			return
		}

		query, _, _ = dialect.From("tbl_promo_inspection").Select(colsPromoInspection...).Where(g.Ex{"uid": v.Uid, "start_at": g.Op{"gte": lastWithdrawAt.Int64}}).ToSQL()
		fmt.Println(query)
		err = BetDb.Select(&reocrds, query)
		if err != nil {
			fmt.Println(err)
			return
		}

		for _, r := range reocrds {
			if r.Ty <= 3 {
				fmt.Println(fmt.Sprintf(`%s%d`, r.BillNo, r.Ty))
				tempMap[fmt.Sprintf(`%s%d`, r.BillNo, r.Ty)] = r
			} else {
				tempMap[r.BillNo] = r
			}
			if r.Uid == "42" {
				fmt.Println("r.Id1===:", r.Id, ",reocrds1:", reocrds)
			}
		}
		for _, r := range depositList {
			matched := false
			if depositPIR, ok := tempMap[r.Id+"0"]; ok {
				depositPIR.FinishedAmount = 0
				result = append(result, depositPIR)
				fmt.Println("r.Id+0===:", r.Uid, ",result:", result)
				matched = true
			}
			if depositPIR2, ok2 := tempMap[r.Id+"2"]; ok2 {
				result = append(result, depositPIR2)
				fmt.Println("r.Id+2===:", r.Uid, ",result:", result)
				matched = true
			}
			if depositPIR1, ok1 := tempMap[r.Id+"1"]; ok1 {
				depositPIR1.FinishedAmount = 0
				result = append(result, depositPIR1)
				fmt.Println("r.Id+1===:", r.Uid, ",result:", result)
				matched = true
			}
			if depositPIR4, ok4 := tempMap[r.Id+"4"]; ok4 {
				result = append(result, depositPIR4)
				fmt.Println("r.Id+4===:", r.Uid, ",result:", result)
				matched = true
			}

			if !matched {
				dty := 1
				bty := 3
				if r.SuccessTime == 1 {
					dty = 0
					bty = 2
				}

				flow, _ := decimal.NewFromFloat(r.Amount).Mul(decimal.NewFromFloat(flowCfgMap[dty].FlowMultiple)).Float64()
				result = append(result, TblPromoInspection{
					Uid:              r.Uid,
					Username:         r.Username,
					ParentId:         r.ParentId,
					ParentName:       r.ParentName,
					Level:            v.Vip,
					Ty:               dty,
					State:            1,
					CapitalAmount:    r.Amount,
					FlowMultiple:     flowCfgMap[dty].FlowMultiple,
					FlowAmount:       flow,
					FinishedAmount:   0,
					UnfinishedAmount: flow,
					CreatedAt:        r.CreatedAt,
					StartAt:          r.ConfirmAt,
					FinishAt:         0,
					BillNo:           r.Id,
					Remark:           "充值的打码记录",
				})

				bflow, _ := decimal.NewFromFloat(r.Discount).Mul(decimal.NewFromFloat(flowCfgMap[bty].FlowMultiple)).Float64()
				result = append(result, TblPromoInspection{
					Uid:              r.Uid,
					Username:         r.Username,
					ParentId:         r.ParentId,
					ParentName:       r.ParentName,
					Level:            v.Vip,
					Ty:               bty,
					State:            1,
					CapitalAmount:    r.Discount,
					FlowMultiple:     flowCfgMap[bty].FlowMultiple,
					FlowAmount:       bflow,
					FinishedAmount:   0,
					UnfinishedAmount: bflow,
					CreatedAt:        r.CreatedAt,
					StartAt:          r.ConfirmAt,
					FinishAt:         0,
					BillNo:           r.Id,
					Remark:           "充值彩金的打码记录",
				})
				fmt.Println("r.Uid===:", r.Uid, ",result:", result)
			}
		}
		for _, rl := range result {
			if rl.Uid == "42" {
				fmt.Println("rl.Id depositList===", rl.Id)
				fmt.Println("uid depositList===42", rl)
			}
		}
		for _, r := range promoList {
			if depositPIR, ok := tempMap[r.BillNo]; ok {
				result = append(result, depositPIR)
				fmt.Println("r.BillNo===:", r.UID, r.BillNo, ",result:", result)
			} else {

				dty := 5
				name := "VIP升级"
				//301	vip晋级礼金
				//306	天天签到活动红利
				//307	宝箱活动红利
				//308	周投注活动
				switch r.CashType {
				case 306:
					dty = 4
					name = "签到"
				case 307:
					dty = 6
					name = "宝箱"
				case 308:
					dty = 7
					name = "周投注"
				case 309:
					dty = 8
					name = "邀请奖金"
				case 310:
					dty = 9
					name = "流水奖金"
				case 311:
					dty = 10
					name = "手动彩金"
				}

				flow, _ := decimal.NewFromFloat(r.Amount).Mul(decimal.NewFromFloat(flowCfgMap[dty].FlowMultiple)).Float64()
				result = append(result, TblPromoInspection{
					Uid:              r.UID,
					Username:         r.Username,
					ParentId:         v.ParentID,
					ParentName:       v.ParentName,
					Level:            v.Vip,
					Ty:               dty,
					State:            1,
					CapitalAmount:    r.Amount,
					FlowMultiple:     flowCfgMap[dty].FlowMultiple,
					FlowAmount:       flow,
					FinishedAmount:   0,
					UnfinishedAmount: flow,
					CreatedAt:        r.CreatedAt / 1000,
					StartAt:          r.CreatedAt / 1000,
					FinishAt:         0,
					BillNo:           r.BillNo,
					Remark:           name + "的打码记录",
				})
				fmt.Println("r.cash_type:", r.CashType, ",result:", result)

			}
		}
		for _, rl := range result {
			if rl.Uid == "42" {
				fmt.Println("rl.Id membalan===", rl.Id)
				fmt.Println("uid membalan===42", rl)
			}
		}
		sort.Slice(result, func(i, j int) bool {
			if result[i].Id > result[j].Id {
				return true
			}
			return false
		})

		t := int64(len(result))
		validMap := map[int64]decimal.Decimal{}
		for j := t - 1; j >= 0; j-- {
			fmt.Println("欠了钱的组合：", validMap)

			if j < t-1 {
				var validBetAmount decimal.Decimal //总流水

				now := result[j]
				finishAt := int64(0)
				if j-1 >= 0 {
					fmt.Println("@111111")
					fmt.Println(now.Remark)
					fmt.Println(now.FlowAmount)
					fmt.Println(now.FinishedAmount)
					next := result[j-1]
					finishAt = next.CreatedAt
					validBetAmount, _ = uidValidBet(v.Uid, now.CreatedAt, next.CreatedAt)
				} else {
					fmt.Println("@222222")
					fmt.Println(now.Remark)
					fmt.Println(now.FlowAmount)
					fmt.Println(now.FinishedAmount)
					validBetAmount, _ = uidValidBet(v.Uid, now.CreatedAt, time.Now().Unix())
				}

				//之前的稽查是否欠了流水
				for k := t - 1; k >= j; k-- {
					fmt.Println("之前欠了钱的：", validMap[k])
					fmt.Println("当前有钱：", validBetAmount)

					if validMap[k].Cmp(decimal.Zero) < 0 && validBetAmount.Cmp(decimal.Zero) > 0 {
						//之前的欠了钱 且当前稽查的完成流水大于0 先还之前的钱
						fmt.Println("之前欠了钱的：", result[k])
						//够钱还的话
						if validBetAmount.Add(validMap[k]).Cmp(decimal.Zero) >= 0 {
							result[k].UFA = decimal.Zero
							result[k].UnfinishedAmount = 0
							result[k].FinishedAmount = result[k].FlowAmount
							result[k].FinishAt = finishAt
							validBetAmount = validBetAmount.Add(validMap[k])

							delete(validMap, k)
						} else {
							//不够钱还的情况
							r := validBetAmount.Add(validMap[k])
							result[k].UFA = result[k].UFA.Add(validBetAmount)
							result[k].UnfinishedAmount, _ = result[k].UFA.Abs().Float64()
							result[k].FinishedAmount, _ = decimal.NewFromFloat(result[k].FlowAmount).Add(result[k].UFA).Float64()
							validMap[k] = r
							validBetAmount = decimal.Zero
						}
					}
				}
				fmt.Println(result[j])
				//还完钱还剩的才去算完成流水2
				fmt.Println("还剩流水：", validBetAmount)
				if validBetAmount.Cmp(decimal.NewFromFloat(now.FlowAmount)) >= 0 {
					fmt.Println("还剩流水够还：", validBetAmount)
					result[j].UFA = decimal.Zero
					result[j].UnfinishedAmount = 0
					result[j].FinishedAmount, _ = validBetAmount.Float64()
					result[j].FinishAt = finishAt

				} else if validBetAmount.Cmp(decimal.Zero) > 0 {
					fmt.Println("还剩流水不够还：", validBetAmount)
					r := validBetAmount.Sub(decimal.NewFromFloat(now.FlowAmount).Add(decimal.NewFromFloat(result[j].FinishedAmount)))
					result[j].UFA = r
					result[j].UnfinishedAmount, _ = r.Abs().Float64()
					result[j].FinishedAmount, _ = decimal.NewFromFloat(now.FlowAmount).Add(result[j].UFA).Float64()
					validMap[j] = r

				} else {
					fmt.Println("还剩流水为零：", validBetAmount)
					result[j].UFA = decimal.Zero.Sub(decimal.NewFromFloat(result[j].FlowAmount))
					result[j].UnfinishedAmount, _ = result[j].UFA.Abs().Float64()
					result[j].FinishedAmount = 0
					validMap[j] = result[j].UFA

				}
				fmt.Println(result[j])
			} else if j == t-1 {
				var validBetAmount decimal.Decimal
				now := result[j]
				finishAt := int64(0)
				fmt.Println("@333333")

				if j-1 >= 0 {
					next := result[j-1]

					validBetAmount, _ = uidValidBet(v.Uid, now.CreatedAt, next.CreatedAt)
					finishAt = next.CreatedAt
				} else {

					validBetAmount, _ = uidValidBet(v.Uid, now.CreatedAt, time.Now().Unix())
					finishAt = time.Now().Unix()
				}

				if validBetAmount.Cmp(decimal.NewFromFloat(now.FlowAmount)) >= 0 {
					result[j].UFA = decimal.Zero
					result[j].UnfinishedAmount = 0
					result[j].FinishedAmount, _ = validBetAmount.Float64()
					result[j].FinishAt = finishAt
					validMap[j] = result[j].UFA

				} else {
					r := validBetAmount.Sub(decimal.NewFromFloat(now.FlowAmount))
					result[j].UFA = r
					result[j].UnfinishedAmount, _ = r.Abs().Float64()
					result[j].FinishedAmount, _ = validBetAmount.Float64()
					validMap[j] = r
				}
			}
		}
		tempMap = nil
		tx, err := BetDb.Begin()
		if err != nil {
			fmt.Println(err)
		}
		for _, rl := range result {
			if rl.FinishAt > time.Now().Unix()*10 {
				rl.FinishAt = rl.FinishAt / 1000
			}
			if rl.Uid == "42" {
				fmt.Println("rl.Id后===", rl.Id)
				fmt.Println("uid后===42", rl)
			}
			if rl.State != 3 && rl.FlowMultiple > 0 {
				//if rl.FlowMultiple > 0 {
				if rl.Id == "" {
					if rl.UFA.Equal(decimal.Zero) {
						rl.State = 2
						rl.FinishAt = time.Now().Unix()
					} else {
						rl.State = 1
						rl.FinishAt = 0
						//depostLockAmount.Add(decimal.NewFromFloat(rl.CapitalAmount))
					}
					rl.Id = helper.GenId()
					query, _, _ = dialect.Insert("tbl_promo_inspection").Rows(rl).ToSQL()
					fmt.Println(query)
					_, err = tx.Exec(query)
					if err != nil {
						_ = tx.Rollback()
						fmt.Println(err)
					}
				} else {
					state := 1
					if rl.UFA.Equal(decimal.Zero) {
						state = 2
						rl.FinishAt = time.Now().Unix()
					} else {
						rl.State = 1
						rl.FinishAt = 0
						//depostLockAmount.Add(decimal.NewFromFloat(rl.CapitalAmount))
					}

					query, _, _ = dialect.Update("tbl_promo_inspection").Set(g.Record{"state": state,
						"finished_amount": rl.FinishedAmount, "unfinished_amount": rl.UnfinishedAmount, "flow_amount": rl.FlowAmount,
						"finish_at": rl.FinishAt}).Where(g.Ex{"id": rl.Id}).ToSQL()
					fmt.Println(query)
					_, err = tx.Exec(query)
					if err != nil {
						_ = tx.Rollback()
						fmt.Println(err)
					}
				}
			}
		}
		if meta.WalletMode == "1" {
			lockAmount, _ := uidValidAmount(v.Uid, "1")
			config := ryrpc.TblGameConfig{}
			query, _, _ = dialect.From("tbl_game_config").Select(colsGameConfig...).Where(g.Ex{"cfg_type": helper.WithdrawFEE}).ToSQL()
			fmt.Println(query)
			err = BetDb.Get(&config, query)
			if err != nil {
				fmt.Println(fmt.Errorf("query : %s, error : %s", query, err))
				return
			}
			value, _ := strconv.ParseFloat(config.CfgValue, 64)
			discount := decimal.NewFromFloat(value).Div(decimal.NewFromInt(100))
			amount := decimal.NewFromFloat(lockAmount).Mul(discount)
			var b string
			query, _, _ = dialect.From("tbl_member_balance").Select("brl").Where(g.Ex{"uid": v.Uid}).ToSQL()
			fmt.Println(query)
			err = BetDb.Get(&b, query)
			if err != nil {
				fmt.Println(fmt.Errorf("query : %s, error : %s", query, err))
				return
			}
			balance, err := decimal.NewFromString(b)
			if err != nil {
				fmt.Println(fmt.Errorf("query : %s, error : %s", query, err))
				return
			}
			//balance, _ := common.MemberBalance(v.Uid)
			if balance.Cmp(amount) <= 0 {
				query, _, _ = dialect.Delete("tbl_promo_inspection").Where(g.Ex{"uid": v.Uid}).ToSQL()
				fmt.Println(query)
				_, err = tx.Exec(query)
				if err != nil {
					_ = tx.Rollback()
					fmt.Println(err)
				}
				// 3、更新余额
				br := g.Record{
					"deposit_lock_amount": 0,
					"unlock_amount":       balance,
				}
				query, _, _ = dialect.Update("tbl_member_balance").Set(br).Where(g.Ex{"uid": v.Uid}).ToSQL()
				fmt.Println(query)
				_, err = tx.Exec(query)
				if err != nil {
					_ = tx.Rollback()
					return
				}
			} else {
				validAmount, _ := uidValidAmount(v.Uid, "2")
				if balance.Cmp(decimal.NewFromFloat(validAmount)) <= 0 {
					validAmount, _ = balance.Float64()
				}
				// 3、更新余额
				br := g.Record{
					"deposit_lock_amount": g.L(fmt.Sprintf("deposit_lock_amount-%f", validAmount)),
					"unlock_amount":       validAmount,
				}
				query, _, _ = dialect.Update("tbl_member_balance").Set(br).Where(g.Ex{"uid": v.Uid}).ToSQL()
				fmt.Println(query)
				_, err = tx.Exec(query)
				if err != nil {
					_ = tx.Rollback()
					return
				}
			}
		} else if meta.WalletMode == "2" {
			records, err := uidPromoInspection(v.Uid)
			if err != nil {
				return
			}
			var agencyAmount, depositAmount, agencyMoney, agencyLockAmount decimal.Decimal
			var isOk = true
			if len(reocrds) > 0 {
				for _, r := range records {
					if r.State == 1 {
						isOk = false
						break
					}
				}
				if isOk {
					depositAmount, _ = uidValidAgencyAmount(v.Uid, "2", "2")
					agencyAmount, _ = uidValidAgencyAmount(v.Uid, "2", "10")
					if depositAmount.Cmp(decimal.Zero) > 0 || agencyAmount.Cmp(decimal.Zero) > 0 {
						memBalance, err := MemberBalanceFindOne(v.Uid)
						if err != nil {
							return
						}
						agencyMoney = decimal.NewFromFloat(memBalance.DepositLockAmount + memBalance.AgencyAmount)
						var hasDepositLockTask, hasAgencyTask bool
						if depositAmount.Cmp(decimal.Zero) > 0 {
							hasDepositLockTask = true
						}
						if agencyAmount.Cmp(decimal.Zero) > 0 {
							hasAgencyTask = true
						}
						agencyMoney, agencyLockAmount = FinishTasks(hasDepositLockTask, hasAgencyTask, depositAmount, agencyAmount, agencyMoney)
						// 3、更新余额
						br := g.Record{
							"brl": g.L(fmt.Sprintf("%s+%f", agencyMoney.String(), memBalance.UnlockAmount)),
							//"brl":                 g.L(fmt.Sprintf("brl+%s-%s", agencyMoney.String(), agencyLockAmount.String())),
							"unlock_amount":       g.L(fmt.Sprintf("unlock_amount+%s", agencyMoney.String())),
							"agency_amount":       "0",
							"deposit_lock_amount": "0",
							"agency_lock_amount":  g.L(fmt.Sprintf("agency_lock_amount+%s", agencyLockAmount.String())),
						}
						query, _, _ = dialect.Update("tbl_member_balance").Set(br).Where(g.Ex{"uid": v.Uid}).ToSQL()
						fmt.Println(query)
						_, err = tx.Exec(query)
						if err != nil {
							_ = tx.Rollback()
							return
						}
						//转为真金后把打码任务状态置为已完成并完成结算
						//query, _, _ = dialect.Delete("tbl_promo_inspection").Where(g.Ex{"uid": v.Uid, "state": 2}).ToSQL()
						query, _, _ = dialect.Update("tbl_promo_inspection").Set(g.Record{"state": 3}).Where(g.Ex{"uid": v.Uid}).ToSQL()
						fmt.Println(query)
						_, err = tx.Exec(query)
						if err != nil {
							_ = tx.Rollback()
							return
						}
						//转为真金后把游戏记录状态都置为无效,避免再次统计到已完成流水里 flag=3
						currentTime := time.Now()
						milliseconds := currentTime.UnixNano() / int64(time.Millisecond)
						query, _, _ = dialect.Update("tbl_game_record").Set(g.Record{"flag": 3}).Where(g.Ex{"uid": v.Uid, "bet_time": g.Op{"lte": milliseconds}}).ToSQL()
						fmt.Println(query)
						_, err = tx.Exec(query)
						if err != nil {
							_ = tx.Rollback()
							return
						}
					}
				}
			}
		}
		err = tx.Commit()
		if err != nil {
			return
		}
	}

}

// UidValidBet 获取指定会员时间段的有效投注
func uidValidBet(uid string, startAt, endAt int64) (decimal.Decimal, error) {

	waterFlow := decimal.NewFromFloat(0.0000)
	if startAt == 0 && endAt == 0 {
		return waterFlow, errors.New(helper.QueryTimeRangeErr)
	}

	ex := g.Ex{}
	ex["flag"] = 1
	ex["uid"] = uid
	var validAmount sql.NullFloat64
	ex["bet_time"] = g.Op{"between": exp.NewRangeVal(startAt*1000, endAt*1000)}

	query, _, _ := dialect.From("tbl_game_record").Select(g.SUM("valid_bet_amount").As("valid_bet_amount")).Where(ex).Limit(1).ToSQL()
	fmt.Println(query)
	err := BetDb.Get(&validAmount, query)
	if err != nil {
		return decimal.Zero, errors.New(helper.DBErr)
	}

	return decimal.NewFromFloat(validAmount.Float64), nil
}

// uidValidAmount 获取指定会员解锁的金额
func uidValidAmount(uid, state string) (float64, error) {

	ex := g.Ex{}
	ex["state"] = state
	ex["uid"] = uid
	var validAmount sql.NullFloat64

	query, _, _ := dialect.From("tbl_promo_inspection").Select(g.SUM("capital_amount").As("capital_amount")).Where(ex).ToSQL()
	fmt.Println(query)
	err := BetDb.Get(&validAmount, query)
	if err != nil {
		return 0, errors.New(helper.DBErr)
	}

	return validAmount.Float64, nil
}

// uidPromoInspection 查询会员所有的彩金打码任务
func uidPromoInspection(uid string) ([]TblPromoInspection, error) {

	var reocrds []TblPromoInspection
	query, _, _ := dialect.From("tbl_promo_inspection").Select(colsPromoInspection...).Where(g.Ex{"ty": []int{2, 10}, "uid": uid}).ToSQL()
	fmt.Println(query)
	err := BetDb.Select(&reocrds, query)
	if err != nil {
		return reocrds, errors.New(helper.DBErr)
	}

	return reocrds, nil
}

// uidValidAgencyAmount 获取指定会员解锁的彩金金额
func uidValidAgencyAmount(uid, state, ty string) (decimal.Decimal, error) {

	ex := g.Ex{}
	ex["state"] = state
	ex["uid"] = uid
	ex["ty"] = ty
	var validAmount sql.NullFloat64

	query, _, _ := dialect.From("tbl_promo_inspection").Select(g.SUM("capital_amount").As("capital_amount")).Where(ex).ToSQL()
	fmt.Println(query)
	err := BetDb.Get(&validAmount, query)
	if err != nil {
		return decimal.Zero, errors.New(helper.DBErr)
	}
	return decimal.NewFromFloat(validAmount.Float64), nil
}

func MemberBalanceFindOne(uid string) (ryrpc.MemberBalance, error) {

	balance := ryrpc.MemberBalance{}
	query, _, _ := dialect.From("tbl_member_balance").Select(colsMemberBalance...).Where(g.Ex{"uid": uid}).Limit(1).ToSQL()
	fmt.Println(query)
	err := BetDb.Get(&balance, query)
	if err != nil {
		return balance, errors.New(helper.DBErr)
	}

	return balance, err
}

func FinishTasks(hasDepositLockTask, hasAgencyTask bool, depositAmount, agencyAmount, agencyMoney decimal.Decimal) (balance, agencyLockAmount decimal.Decimal) {
	if hasDepositLockTask {
		agencyMoney = agencyMoney.Sub(depositAmount)
		if agencyMoney.LessThan(decimal.Zero) {
			balance = agencyMoney.Add(depositAmount)
			agencyMoney = decimal.Zero
		} else {
			balance = depositAmount
		}
	}
	if hasAgencyTask {
		agencyMoney = agencyMoney.Sub(agencyAmount)
		if agencyMoney.LessThan(decimal.Zero) {
			balance = agencyMoney.Add(agencyAmount)
			agencyMoney = decimal.Zero
		} else {
			balance = agencyAmount
		}
	}
	hundred := decimal.NewFromInt(100)
	if agencyMoney.GreaterThanOrEqual(hundred) {
		agencyLockAmount = hundred
	} else {
		agencyLockAmount = agencyMoney
	}
	return balance, agencyLockAmount
}
