package report

import (
	"fmt"
	g "github.com/doug-martin/goqu/v9"
	"strings"
	"task/common"
	"task/contrib/helper"
	ryrpc "task/rpc"
)

// 统计代理、会员报表
func agencyReport(date, flag string) {

	agencyReportData()
}

func agencyReportData() {

	var (
		memberList                                                                                []ryrpc.TblMemberBase
		teamNumList                                                                               []TeamNum
		depositList, withdrawList, validAmountList, dividendList, flowBonusList, firstDepositList []AmountData
		reportMap                                                                                 = map[string]tblReportUser{}
		memberMap                                                                                 = map[string]ryrpc.TblMemberBase{}
		agReports                                                                                 []tblReportUser
	)
	//timeNow := time.Unix(startTime, 0)                    //2017-08-30 16:19:19 +0800 CST
	//startTimeStr := timeNow.Format("2006-01-02 15:04:05") //2015-06-15 08:52:32
	//ts := time.Now()

	query, _, _ := dialect.From("tbl_member_base").Select(colsMember...).Where(g.Ex{"tester": []int{1, 3}}).ToSQL()
	err := BetDb.Select(&memberList, query)
	if err != nil {
		fmt.Println(err)
		return
	}
	for _, v := range memberList {
		reportMap[v.Uid] = tblReportUser{
			Uid:        v.Uid,
			Ty:         1,
			ReportTime: 0,
			Username:   v.Username,
			CreatedAt:  int64(v.CreatedAt),
			CreatedIp:  v.CreatedIp,
			Id:         fmt.Sprintf(`%s|%d|1`, v.Uid, 0),
		}
		memberMap[v.Uid] = v
	}

	//团队总人数
	query = fmt.Sprintf(`select ancestor,count(descendant) as total_team_num,lvl from tbl_members_tree group by ancestor,lvl`)
	err = BetDb.Select(&teamNumList, query)
	if err != nil {
		fmt.Println(err)
		return
	}

	for _, v := range teamNumList {
		if value, exists := reportMap[v.Ancestor]; exists {
			value.TotalTeamNum = value.TotalTeamNum + v.TotalTeamNum
			reportMap[v.Ancestor] = value
		}
	}
	for _, v := range memberList {
		if v.ParentID != "0" {
			if value, exists := reportMap[v.ParentID]; exists {
				value.DirectPushNum = value.DirectPushNum + 1
				reportMap[v.ParentID] = value
			}
		}
		if v.GrandID != "0" {
			if value, exists := reportMap[v.GrandID]; exists {
				value.LvlSecondNum = value.LvlSecondNum + 1
				reportMap[v.GrandID] = value
			}
		}
		if v.GreatGrandID != "0" {
			if value, exists := reportMap[v.GreatGrandID]; exists {
				value.LvlThreeNum = value.LvlThreeNum + 1
				reportMap[v.GreatGrandID] = value
			}
		}

	}

	//充值
	query = fmt.Sprintf(`select uid,sum(amount) as amount from tbl_deposit where state = 362  group by uid`)
	fmt.Println(query)
	err = BetDb.Select(&depositList, query)
	if err != nil {
		fmt.Println(err)
		return
	}
	for _, v := range depositList {
		if user, exists := memberMap[v.Uid]; exists {
			if user.ParentID != "" {
				if value, ok := reportMap[user.ParentID]; ok {
					value.DirectPushDepositNum = value.DirectPushDepositNum + 1
					value.DirectPushDeposit = value.DirectPushDeposit + v.Amount
					value.DepositWithdrawDiff = value.DepositWithdrawDiff + v.Amount
					value.DirectPushDwDiff = value.DirectPushDwDiff + v.Amount

					reportMap[user.ParentID] = value
				}
			}
			if user.GrandID != "" {
				if value, ok := reportMap[user.GrandID]; ok {
					value.LvlSecondDepositNum = value.LvlSecondDepositNum + 1
					value.LvlSecondDepositAmount = value.LvlSecondDepositAmount + v.Amount
					value.DepositWithdrawDiff = value.DepositWithdrawDiff + v.Amount
					value.LvlSecondDwDiff = value.LvlSecondDwDiff + v.Amount
					reportMap[user.GrandID] = value
				}
			}
			if user.GreatGrandID != "" {
				if value, ok := reportMap[user.GreatGrandID]; ok {
					value.LvlThreeDepositNum = value.LvlThreeDepositNum + 1
					value.LvlThreeDepositAmount = value.LvlThreeDepositAmount + v.Amount
					value.DepositWithdrawDiff = value.DepositWithdrawDiff + v.Amount
					value.LvlThreeDwDiff = value.LvlThreeDwDiff + v.Amount
					reportMap[user.GreatGrandID] = value
				}
			}
		}
	}

	//提现
	query = fmt.Sprintf(`select uid,sum(amount) as amount from tbl_withdraw where state = 374 group by uid`)
	err = BetDb.Select(&withdrawList, query)
	if err != nil {
		fmt.Println(err)
		return
	}
	for _, v := range withdrawList {
		if user, exists := memberMap[v.Uid]; exists {
			if value, ok := reportMap[user.Uid]; ok {
				value.WithdrawAmount = value.WithdrawAmount + v.Amount
				value.DepositWithdrawDiff = value.DepositWithdrawDiff - v.Amount
				reportMap[user.Uid] = value
			}
			if user.ParentID != "" {
				if value, ok := reportMap[user.ParentID]; ok {
					value.WithdrawAmount = value.WithdrawAmount + v.Amount
					value.DepositWithdrawDiff = value.DepositWithdrawDiff - v.Amount
					value.DirectPushDwDiff = value.DirectPushDwDiff - v.Amount
					reportMap[user.ParentID] = value
				}
			}
			if user.GrandID != "" {
				if value, ok := reportMap[user.GrandID]; ok {
					value.WithdrawAmount = value.WithdrawAmount + v.Amount
					value.DepositWithdrawDiff = value.DepositWithdrawDiff - v.Amount
					value.LvlSecondDwDiff = value.LvlSecondDwDiff - v.Amount
					reportMap[user.GrandID] = value
				}
			}
			if user.GreatGrandID != "" {
				if value, ok := reportMap[user.GreatGrandID]; ok {
					value.WithdrawAmount = value.WithdrawAmount + v.Amount
					value.DepositWithdrawDiff = value.DepositWithdrawDiff - v.Amount
					value.LvlThreeDwDiff = value.LvlThreeDwDiff - v.Amount
					reportMap[user.GreatGrandID] = value
				}
			}
		}
	}

	//流水
	query = fmt.Sprintf(`select uid,sum(valid_bet_amount) as amount from tbl_game_record where flag = 1 group by uid`)
	err = BetDb.Select(&validAmountList, query)
	if err != nil {
		fmt.Println(err)
		return
	}

	for _, v := range validAmountList {
		if user, exists := memberMap[v.Uid]; exists {
			if user.ParentID != "" {
				if value, ok := reportMap[user.ParentID]; ok {
					value.DirectPushFlow = value.DirectPushFlow + v.Amount
					reportMap[user.ParentID] = value
				}
			}
			if user.GrandID != "" {
				if value, ok := reportMap[user.GrandID]; ok {
					value.DirectPushFlow = value.DirectPushFlow + v.Amount
					reportMap[user.GrandID] = value
				}
			}
			if user.GreatGrandID != "" {
				if value, ok := reportMap[user.GreatGrandID]; ok {
					value.DirectPushFlow = value.DirectPushFlow + v.Amount
					reportMap[user.GreatGrandID] = value
				}
			}
		}
	}

	query, _, _ = dialect.From("tbl_balance_transaction").Select(g.SUM("amount").As("amount"), g.C("uid")).Where(
		g.Ex{"cash_type": []int{helper.TransactionTreasureDividend}}).GroupBy(g.C("uid")).ToSQL()
	fmt.Println(query)
	err = BetDb.Select(&dividendList, query)
	if err != nil {
		fmt.Println(err)
		return
	}

	for _, v := range dividendList {
		if user, exists := memberMap[v.Uid]; exists {
			if value, ok := reportMap[user.Uid]; ok {
				value.TotalCommissions = value.TotalCommissions + v.Amount
				reportMap[user.Uid] = value
			}
		}
	}

	query, _, _ = dialect.From("tbl_balance_transaction").Select(g.SUM("amount").As("amount"), g.C("uid")).Where(
		g.Ex{"cash_type": []int{helper.TransactionInviteDividend}}).GroupBy(g.C("uid")).ToSQL()
	fmt.Println(query)
	err = BetDb.Select(&firstDepositList, query)
	if err != nil {
		fmt.Println(err)
		return
	}

	for _, v := range firstDepositList {
		if user, exists := memberMap[v.Uid]; exists {
			if value, ok := reportMap[user.Uid]; ok {
				value.TotalCommissions = value.TotalCommissions + v.Amount
				value.DirectPushFdBonus = value.DirectPushFdBonus + v.Amount
				reportMap[user.Uid] = value
			}
		}
	}

	query, _, _ = dialect.From("tbl_balance_transaction").Select(g.SUM("amount").As("amount"), g.C("uid")).Where(
		g.Ex{"cash_type": []int{helper.TransactionSubRebateLv1}}).GroupBy(g.C("uid")).ToSQL()
	fmt.Println(query)
	err = BetDb.Select(&flowBonusList, query)
	if err != nil {
		fmt.Println(err)
		return
	}

	for _, v := range flowBonusList {
		if user, exists := memberMap[v.Uid]; exists {
			if value, ok := reportMap[user.Uid]; ok {
				value.TotalCommissions = value.TotalCommissions + v.Amount
				value.DirectPushFlowBonus = value.DirectPushFlowBonus + v.Amount
				reportMap[user.Uid] = value
			}
		}
	}

	for _, v := range reportMap {
		agReports = append(agReports, v)
	}

	//待结算流水和流水奖励
	ls := len(agReports)
	ps := 300
	if ls > 0 {
		p := ls / ps
		l := ls % ps
		for x := 0; x < p; x++ {
			offset := x * ps
			updateAgencyReports(agReports[offset : offset+ps])
		}

		if l > 0 {
			updateAgencyReports(agReports[p*ps:])
		}
	}

}

func updateAgencyReports(data []tblReportUser) {

	ids := ""
	for _, v := range data {
		ids += ","
		ids += "'" + v.Id + "'"
	}

	tx, err := BetDb.Begin()
	if err != nil {
		common.Log("tbl_report_user", "error : %v", err)
		return
	}

	query := fmt.Sprintf("delete from tbl_report_user where id in (%s)", strings.TrimPrefix(ids, ","))
	//fmt.Println(query)
	_, err = tx.Exec(query)
	if err != nil {
		_ = tx.Rollback()
		common.Log("tbl_report_user", "error : %v", err)
		return
	}

	query, _, _ = dialect.From("tbl_report_user").Insert().Rows(data).ToSQL()
	//fmt.Println(query)
	_, err = tx.Exec(query)
	if err != nil {
		_ = tx.Rollback()
		common.Log("tbl_report_user", "error : %v", err)
		return
	}

	_ = tx.Commit()
}
