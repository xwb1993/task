package report

import (
	"fmt"
	g "github.com/doug-martin/goqu/v9"
	"github.com/doug-martin/goqu/v9/exp"
	"strings"
	"task/common"
	"task/contrib/helper"
	ryrpc "task/rpc"
	"time"
)

type tblReportUser struct {
	Id                     string  `json:"id" db:"id"`
	Ty                     int     `json:"ty" db:"ty"`
	ReportTime             int64   `json:"report_time" db:"report_time"`
	Uid                    string  `json:"uid" db:"uid"`
	TotalCommissions       float64 `json:"total_commissions" db:"total_commissions"`                 //历史总佣金
	WaitCommissions        float64 `json:"wait_commissions" db:"wait_commissions"`                   //待结算佣金
	TotalTeamNum           int     `json:"total_team_num" db:"total_team_num"`                       //团队总人数
	DepositWithdrawDiff    float64 `json:"deposit_withdraw_diff" db:"deposit_withdraw_diff"`         //团队存取查
	DirectPushNum          int     `json:"direct_push_num" db:"direct_push_num"`                     //直推人数
	DirectPushDepositNum   int     `json:"direct_push_deposit_num" db:"direct_push_deposit_num"`     //直推充值人数
	DirectPushFdBonus      float64 `json:"direct_push_fd_bonus" db:"direct_push_fd_bonus"`           //直推首充奖励
	DirectPushDeposit      float64 `json:"direct_push_deposit" db:"direct_push_deposit"`             //直推充值
	DirectPushFlow         float64 `json:"direct_push_flow" db:"direct_push_flow"`                   //直推流水
	DirectPushFlowBonus    float64 `json:"direct_push_flow_bonus" db:"direct_push_flow_bonus"`       //直推流水奖励
	DirectPushDwDiff       float64 `json:"direct_push_dw_diff" db:"direct_push_dw_diff"`             //直推充提差
	DirectPushWaitFlow     float64 `json:"direct_push_wait_flow" db:"direct_push_wait_flow"`         //待结算直推流水
	DirectPushWaitBonus    float64 `json:"direct_push_wait_bonus" db:"direct_push_wait_bonus"`       //待结算直推流水奖励
	LvlSecondNum           int     `json:"lvl_second_num" db:"lvl_second_num"`                       //二级人数
	LvlSecondDepositNum    int     `json:"lvl_second_deposit_num" db:"lvl_second_deposit_num"`       //二级充值人数
	LvlSecondDepositAmount float64 `json:"lvl_second_deposit_amount" db:"lvl_second_deposit_amount"` ///二级充值金额
	LvlSecondFlow          float64 `json:"lvl_second_flow" db:"lvl_second_flow"`                     //二级流水
	LvlSecondFlowBonus     float64 `json:"lvl_second_flow_bonus" db:"lvl_second_flow_bonus"`         //二级流水奖励
	LvlSecondWaitFlow      float64 `json:"lvl_second_wait_flow" db:"lvl_second_wait_flow"`           //二级待结算流水
	LvlSecondWaitBonus     float64 `json:"lvl_second_wait_bonus" db:"lvl_second_wait_bonus"`         //二级待结算流水奖励
	LvlSecondDwDiff        float64 `json:"lvl_second_dw_diff" db:"lvl_second_dw_diff"`               //二级充提差
	LvlThreeNum            int     `json:"lvl_three_num" db:"lvl_three_num"`                         //三级人数
	LvlThreeDepositNum     int     `json:"lvl_three_deposit_num" db:"lvl_three_deposit_num"`         //三级充值人数
	LvlThreeDepositAmount  float64 `json:"lvl_three_deposit_amount" db:"lvl_three_deposit_amount"`   //三级充值金额
	LvlThreeFlow           float64 `json:"lvl_three_flow" db:"lvl_three_flow"`                       //三级流水
	LvlThreeFlowBonus      float64 `json:"lvl_three_flow_bonus" db:"lvl_three_flow_bonus"`           //三级流水奖励
	LvlThreeWaitFlow       float64 `json:"lvl_three_wait_flow" db:"lvl_three_wait_flow"`             //三级待结算流水
	LvlThreeWaitBonus      float64 `json:"lvl_three_wait_bonus" db:"lvl_three_wait_bonus"`           //三级待结算流水奖励
	LvlThreeDwDiff         float64 `json:"lvl_three_dw_diff" db:"lvl_three_dw_diff"`                 //三级充提差
	WithdrawAmount         float64 `json:"withdraw_amount" db:"withdraw_amount"`                     //提现金额
	Username               string  `json:"username" db:"username"`                                   //用户名
	CreatedAt              int64   `json:"created_at" db:"created_at"`                               //注册时间
	CreatedIp              string  `json:"created_ip" db:"created_ip"`                               //注册ip
}

// 统计代理、会员报表
func userReport(date, flag string) {

	now := helper.DayTST(0, loc)
	endTime := now.Unix()
	if flag == "update" {
		userReportData(now.Unix(), helper.DayTET(0, loc).Unix(), flag)
	} else if flag == "monthDay" {
		startMonDay := helper.MonthTST(time.Now().Unix(), loc).Unix()
		for startMonDay < endTime {
			userReportData(startMonDay, startMonDay+86400, "update")
			startMonDay += 86400
		}
	}
}

type TeamNum struct {
	Ancestor     string `db:"ancestor"`
	TotalTeamNum int    `db:"total_team_num"`
	Lvl          int    `db:"lvl"`
}

type AmountData struct {
	Uid    string  `db:"uid"`
	Amount float64 `db:"amount"`
}

type FlowAmountData struct {
	Uid      string  `db:"uid"`
	Amount   float64 `db:"amount"`
	CashType int     `db:"cash_type"`
}

type LvlData struct {
	Uid          string `db:"uid"`
	ParentId     string `db:"parent_id"`
	GrandId      string `db:"grand_id"`
	GreatGrandId string `db:"great_grand_id"`
}

type BalanceData struct {
	Uid      string  `db:"uid"`
	CashType string  `db:"cash_type"`
	Amount   float64 `db:"amount"`
}

func userReportData(startTime, endTime int64, flag string) {

	var (
		memberList                                                                 []ryrpc.TblMemberBase
		teamNumList                                                                []TeamNum
		depositList, withdrawList, validAmountList, dividendList, firstDepositList []AmountData
		flowBonusList                                                              []FlowAmountData
		reportMap                                                                  = map[string]tblReportUser{}
		memberMap                                                                  = map[string]ryrpc.TblMemberBase{}
		agReports                                                                  []tblReportUser
		//lvlList                                                                    []LvlData
		//balanceData                                                                []BalanceData
	)
	//timeNow := time.Unix(startTime, 0)                    //2017-08-30 16:19:19 +0800 CST
	//startTimeStr := timeNow.Format("2006-01-02 15:04:05") //2015-06-15 08:52:32
	//ts := time.Now()

	query, _, _ := dialect.From("tbl_member_base").Select(colsMember...).Where(g.Ex{"tester": []int{1, 3}}).ToSQL()
	err := BetDb.Select(&memberList, query)
	if err != nil {
		fmt.Println(err)
		return
	}
	for _, v := range memberList {
		reportMap[v.Uid] = tblReportUser{
			Uid:        v.Uid,
			Ty:         2,
			ReportTime: startTime,
			Username:   v.Username,
			CreatedAt:  int64(v.CreatedAt),
			CreatedIp:  v.CreatedIp,
			Id:         fmt.Sprintf(`%s|%d|2`, v.Uid, startTime),
		}
		memberMap[v.Uid] = v
	}

	//团队总人数
	query = fmt.Sprintf(`select ancestor,count(descendant) as total_team_num,lvl from tbl_members_tree group by ancestor,lvl`)
	err = BetDb.Select(&teamNumList, query)
	if err != nil {
		fmt.Println(err)
		return
	}

	for _, v := range teamNumList {
		if value, exists := reportMap[v.Ancestor]; exists {
			value.TotalTeamNum = value.TotalTeamNum + v.TotalTeamNum
			reportMap[v.Ancestor] = value
		}
	}
	for _, v := range memberList {
		if int64(v.CreatedAt) > startTime && int64(v.CreatedAt) < endTime {
			if v.ParentID != "0" {
				if value, exists := reportMap[v.ParentID]; exists {
					value.DirectPushNum = value.DirectPushNum + 1
					reportMap[v.ParentID] = value
				}
			}
			if v.GrandID != "0" {
				if value, exists := reportMap[v.GrandID]; exists {
					value.LvlSecondNum = value.LvlSecondNum + 1
					reportMap[v.GrandID] = value
				}
			}
			if v.GreatGrandID != "0" {
				if value, exists := reportMap[v.GreatGrandID]; exists {
					value.LvlThreeNum = value.LvlThreeNum + 1
					reportMap[v.GreatGrandID] = value
				}
			}
		}
	}

	//充值
	query = fmt.Sprintf(`select uid,sum(amount) as amount from tbl_deposit where state in(362,365) and confirm_at >= %d and confirm_at < %d group by uid`, startTime, endTime)
	fmt.Println(query)
	err = BetDb.Select(&depositList, query)
	if err != nil {
		fmt.Println(err)
		return
	}
	for _, v := range depositList {
		if user, exists := memberMap[v.Uid]; exists {

			if user.ParentID != "" {
				if value, ok := reportMap[user.ParentID]; ok {
					value.DirectPushDepositNum = value.DirectPushDepositNum + 1
					value.DirectPushDeposit = value.DirectPushDeposit + v.Amount
					value.DepositWithdrawDiff = value.DepositWithdrawDiff + v.Amount
					value.DirectPushDwDiff = value.DirectPushDwDiff + v.Amount

					reportMap[user.ParentID] = value
				}
			}
			if user.GrandID != "" {
				if value, ok := reportMap[user.GrandID]; ok {
					value.LvlSecondDepositNum = value.LvlSecondDepositNum + 1
					value.LvlSecondDepositAmount = value.LvlSecondDepositAmount + v.Amount
					value.DepositWithdrawDiff = value.DepositWithdrawDiff + v.Amount
					value.LvlSecondDwDiff = value.LvlSecondDwDiff + v.Amount
					reportMap[user.GrandID] = value
				}
			}
			if user.GreatGrandID != "" {
				if value, ok := reportMap[user.GreatGrandID]; ok {
					value.LvlThreeDepositNum = value.LvlThreeDepositNum + 1
					value.LvlThreeDepositAmount = value.LvlThreeDepositAmount + v.Amount
					value.DepositWithdrawDiff = value.DepositWithdrawDiff + v.Amount
					value.LvlThreeDwDiff = value.LvlThreeDwDiff + v.Amount
					reportMap[user.GreatGrandID] = value
				}
			}
		}
	}

	//提现
	query = fmt.Sprintf(`select uid,sum(amount) as amount from tbl_withdraw where state = 374 and withdraw_at >= %d and withdraw_at < %d group by uid`, startTime, endTime)
	fmt.Println(query)
	err = BetDb.Select(&withdrawList, query)
	if err != nil {
		fmt.Println(err)
		return
	}
	for _, v := range withdrawList {
		if user, exists := memberMap[v.Uid]; exists {
			if value, ok := reportMap[user.Uid]; ok {
				value.WithdrawAmount = value.WithdrawAmount + v.Amount
				value.DepositWithdrawDiff = value.DepositWithdrawDiff - v.Amount
				reportMap[user.Uid] = value
			}
			if user.ParentID != "" {
				if value, ok := reportMap[user.ParentID]; ok {
					value.WithdrawAmount = value.WithdrawAmount + v.Amount
					value.DepositWithdrawDiff = value.DepositWithdrawDiff - v.Amount
					value.DirectPushDwDiff = value.DirectPushDwDiff - v.Amount
					reportMap[user.ParentID] = value
				}
			}
			if user.GrandID != "" {
				if value, ok := reportMap[user.GrandID]; ok {
					value.WithdrawAmount = value.WithdrawAmount + v.Amount
					value.DepositWithdrawDiff = value.DepositWithdrawDiff - v.Amount
					value.LvlSecondDwDiff = value.LvlSecondDwDiff - v.Amount
					reportMap[user.GrandID] = value
				}
			}
			if user.GreatGrandID != "" {
				if value, ok := reportMap[user.GreatGrandID]; ok {
					value.WithdrawAmount = value.WithdrawAmount + v.Amount
					value.DepositWithdrawDiff = value.DepositWithdrawDiff - v.Amount
					value.LvlThreeDwDiff = value.LvlThreeDwDiff - v.Amount
					reportMap[user.GreatGrandID] = value
				}
			}
		}
	}

	//流水
	query = fmt.Sprintf(`select uid,sum(valid_bet_amount) as amount from tbl_game_record where flag = 1 and bet_time >= %d and bet_time < %d group by uid`, startTime*1000, endTime*1000+999)
	err = BetDb.Select(&validAmountList, query)
	if err != nil {
		fmt.Println(err)
		return
	}

	for _, v := range validAmountList {
		if user, exists := memberMap[v.Uid]; exists {
			if user.ParentID != "" {
				if value, ok := reportMap[user.ParentID]; ok {
					value.DirectPushFlow = value.DirectPushFlow + v.Amount
					value.DirectPushWaitFlow = value.DirectPushWaitFlow + v.Amount
					reportMap[user.ParentID] = value
				}
			}
			if user.GrandID != "" {
				if value, ok := reportMap[user.GrandID]; ok {
					value.DirectPushFlow = value.DirectPushFlow + v.Amount
					value.DirectPushWaitFlow = value.DirectPushWaitFlow + v.Amount
					reportMap[user.GrandID] = value
				}
			}
			if user.GreatGrandID != "" {
				if value, ok := reportMap[user.GreatGrandID]; ok {
					value.DirectPushFlow = value.DirectPushFlow + v.Amount
					value.DirectPushWaitFlow = value.DirectPushWaitFlow + v.Amount
					reportMap[user.GreatGrandID] = value
				}
			}
		}
	}

	query, _, _ = dialect.From("tbl_balance_transaction").Select(g.SUM("amount").As("amount"), g.C("uid")).Where(
		g.Ex{"cash_type": []int{helper.TransactionTreasureDividend},
			"created_at": g.Op{"between": exp.NewRangeVal(startTime*1000, endTime*1000+999)}}).GroupBy(g.C("uid")).ToSQL()
	fmt.Println(query)
	err = BetDb.Select(&dividendList, query)
	if err != nil {
		fmt.Println(err)
		return
	}
	fmt.Println("dividendList:", len(dividendList))
	for _, v := range dividendList {
		if user, exists := memberMap[v.Uid]; exists {
			if value, ok := reportMap[user.Uid]; ok {
				value.TotalCommissions = value.TotalCommissions + v.Amount
				reportMap[user.Uid] = value
			}
		}
	}

	query, _, _ = dialect.From("tbl_balance_transaction").Select(g.SUM("amount").As("amount"), g.C("uid")).Where(
		g.Ex{"cash_type": []int{helper.TransactionInviteDividend}, "created_at": g.Op{"between": exp.NewRangeVal(startTime*1000, endTime*1000+999)}}).GroupBy(g.C("uid")).ToSQL()
	fmt.Println(query)
	err = BetDb.Select(&firstDepositList, query)
	if err != nil {
		fmt.Println(err)
		return
	}
	fmt.Println("firstDepositList:", len(firstDepositList))

	for _, v := range firstDepositList {
		if user, exists := memberMap[v.Uid]; exists {
			if value, ok := reportMap[user.Uid]; ok {
				value.TotalCommissions = value.TotalCommissions + v.Amount
				value.DirectPushFdBonus = value.DirectPushFdBonus + v.Amount
				reportMap[user.Uid] = value
			}
		}
	}

	query, _, _ = dialect.From("tbl_balance_transaction").Select(g.SUM("amount").As("amount"), g.C("uid"), g.C("cash_type")).Where(
		g.Ex{"cash_type": []int{helper.TransactionSubRebateLv1, helper.TransactionSubRebateLv2, helper.TransactionSubRebateLv3}, "created_at": g.Op{"between": exp.NewRangeVal(startTime*1000, endTime*1000+999)}}).GroupBy(g.C("uid"), g.C("cash_type")).ToSQL()
	fmt.Println(query)
	err = BetDb.Select(&flowBonusList, query)
	if err != nil {
		fmt.Println(err)
		return
	}
	fmt.Println("flowBonusList:", len(flowBonusList))

	for _, v := range flowBonusList {
		if user, exists := memberMap[v.Uid]; exists {
			if value, ok := reportMap[user.Uid]; ok {
				value.TotalCommissions = value.TotalCommissions + v.Amount
				if v.CashType == helper.TransactionSubRebateLv1 {
					value.DirectPushFlowBonus = value.DirectPushFlowBonus + v.Amount
				}
				if v.CashType == helper.TransactionSubRebateLv2 {
					value.LvlSecondFlowBonus = value.LvlSecondFlowBonus + v.Amount
				}
				if v.CashType == helper.TransactionSubRebateLv3 {
					value.LvlThreeFlowBonus = value.LvlThreeFlowBonus + v.Amount
				}
				reportMap[user.Uid] = value
			}
		}
	}

	for _, v := range reportMap {
		agReports = append(agReports, v)
	}

	////推荐详情
	//for _, v := range agReports {
	//	//直推
	//	query = fmt.Sprintf(`select uid,parent_id,grand_id,great_grand_id from tbl_member_base where parent_id = %d`, v.Uid)
	//	fmt.Println(query)
	//	err = BetDb.Select(&lvlList, query)
	//	if err != nil {
	//		fmt.Println(err)
	//		return
	//	}
	//	v.DirectPushNum = len(lvlList) //直推人数
	//
	//	ids := ""
	//	for _, v := range lvlList {
	//		ids += ","
	//		ids += "'" + v.Uid + "'"
	//	}
	//	query = fmt.Sprintf(`select uid,cash_type,sum(amount) as amount from tbl_balance_transaction where cash_type in (1,2) and uid in (%s) group BY cash_type,uid`, strings.TrimPrefix(ids, ","))
	//	fmt.Println(query)
	//	err = BetDb.Select(&balanceData, query)
	//	if err != nil {
	//		fmt.Println(err)
	//		return
	//	}
	//	v.DirectPushDepositNum = 0
	//	v.DirectPushDeposit = 0
	//	dwAmount := float64(0)
	//	for _, e := range balanceData {
	//		if e.CashType == "1" {
	//			v.DirectPushDepositNum++                             //直推充值人数
	//			v.DirectPushDeposit = v.DirectPushDeposit + e.Amount //直推充值
	//		}
	//		if e.CashType == "2" {
	//			dwAmount = dwAmount + e.Amount
	//		}
	//	}
	//	v.DirectPushDwDiff = dwAmount //直推充提差
	//
	//	//二级
	//	query = fmt.Sprintf(`select uid,parent_id,grand_id,great_grand_id from tbl_member_base where grand_id = %d`, v.Uid)
	//	fmt.Println(query)
	//	err = BetDb.Select(&lvlList, query)
	//	if err != nil {
	//		fmt.Println(err)
	//		return
	//	}
	//	v.LvlSecondNum = len(lvlList) //二级人数
	//
	//	ids = ""
	//	for _, v := range lvlList {
	//		ids += ","
	//		ids += "'" + v.Uid + "'"
	//	}
	//	query = fmt.Sprintf(`select uid,cash_type,sum(amount) as amount from tbl_balance_transaction where cash_type in (1,2) and uid in (%s) group BY cash_type,uid`, strings.TrimPrefix(ids, ","))
	//	fmt.Println(query)
	//	err = BetDb.Select(&balanceData, query)
	//	if err != nil {
	//		fmt.Println(err)
	//		return
	//	}
	//	v.LvlSecondDepositNum = 0
	//	v.LvlSecondDepositAmount = 0
	//	dwAmount = float64(0)
	//	for _, e := range balanceData {
	//		if e.CashType == "1" {
	//			v.LvlSecondDepositNum++                                        //二级充值人数
	//			v.LvlSecondDepositAmount = v.LvlSecondDepositAmount + e.Amount //二级充值
	//		}
	//		if e.CashType == "2" {
	//			dwAmount = dwAmount + e.Amount
	//		}
	//	}
	//	v.LvlSecondDwDiff = dwAmount //二级充提差
	//
	//	//三级
	//	query = fmt.Sprintf(`select uid,parent_id,grand_id,great_grand_id from tbl_member_base where great_grand_id = %d`, v.Uid)
	//	fmt.Println(query)
	//	err = BetDb.Select(&lvlList, query)
	//	if err != nil {
	//		fmt.Println(err)
	//		return
	//	}
	//	v.LvlThreeNum = len(lvlList) //三级人数
	//
	//	ids = ""
	//	for _, v := range lvlList {
	//		ids += ","
	//		ids += "'" + v.Uid + "'"
	//	}
	//	query = fmt.Sprintf(`select uid,cash_type,sum(amount) as amount from tbl_balance_transaction where cash_type in (1,2) and uid in (%s) group BY cash_type,uid`, strings.TrimPrefix(ids, ","))
	//	fmt.Println(query)
	//	err = BetDb.Select(&balanceData, query)
	//	if err != nil {
	//		fmt.Println(err)
	//		return
	//	}
	//	v.LvlThreeDepositNum = 0
	//	v.LvlThreeDepositAmount = 0
	//	dwAmount = float64(0)
	//	for _, e := range balanceData {
	//		if e.CashType == "1" {
	//			v.LvlThreeDepositNum++                                       //三级充值人数
	//			v.LvlThreeDepositAmount = v.LvlThreeDepositAmount + e.Amount //三级充值
	//		}
	//		if e.CashType == "2" {
	//			dwAmount = dwAmount + e.Amount
	//		}
	//	}
	//	v.LvlThreeDwDiff = dwAmount //三级充提差
	//
	//}

	//待结算流水和流水奖励
	ls := len(agReports)
	ps := 300
	if ls > 0 {
		p := ls / ps
		l := ls % ps
		for x := 0; x < p; x++ {
			offset := x * ps
			updateUserReports(agReports[offset : offset+ps])
		}

		if l > 0 {
			updateUserReports(agReports[p*ps:])
		}
	}

}

func updateUserReports(data []tblReportUser) {

	ids := ""
	for _, v := range data {
		ids += ","
		ids += "'" + v.Id + "'"
	}

	tx, err := BetDb.Begin()
	if err != nil {
		common.Log("tbl_report_user", "error : %v", err)
		return
	}

	query := fmt.Sprintf("delete from tbl_report_user where id in (%s)", strings.TrimPrefix(ids, ","))
	//fmt.Println(query)
	_, err = tx.Exec(query)
	if err != nil {
		_ = tx.Rollback()
		common.Log("tbl_report_user", "error : %v", err)
		return
	}

	query, _, _ = dialect.From("tbl_report_user").Insert().Rows(data).ToSQL()
	//fmt.Println(query)
	_, err = tx.Exec(query)
	if err != nil {
		_ = tx.Rollback()
		common.Log("tbl_report_user", "error : %v", err)
		return
	}

	_ = tx.Commit()
}
