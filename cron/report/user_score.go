package report

import (
	"database/sql"
	"fmt"
	g "github.com/doug-martin/goqu/v9"
	ryrpc "task/rpc"
)

func updateScore(date, flag string) {

	var memberList []ryrpc.TblMemberBase

	query, _, _ := dialect.From("tbl_member_base").Select(colsMember...).Where(g.Ex{"deposit_amount": g.Op{"gte": 0}}).ToSQL()
	err := BetDb.Select(&memberList, query)
	if err != nil {
		fmt.Println(err)
		return
	}

	for _, v := range memberList {
		var score sql.NullFloat64
		query, _, _ = dialect.From("tbl_game_record").Select(g.SUM("valid_bet_amount").As("valid_bet_amount")).Where(g.Ex{"flag": 1, "uid": v.Uid}).ToSQL()
		err = BetDb.Get(&score, query)
		if err != nil {
			fmt.Println(err)
			return
		}
		if score.Valid {
			ex := g.Ex{
				"uid": v.Uid,
			}
			record := g.Record{"score": int64(score.Float64)}

			// 更新会员信息
			query, _, _ = dialect.Update("tbl_member_base").Set(record).Where(ex).ToSQL()
			fmt.Println("updateScore sql:", query)
			_, err = BetDb.Exec(query)
			if err != nil {
				fmt.Println("updateScore:", err)
			}
		}
	}

}
