package report

import (
	"context"
	"fmt"
	g "github.com/doug-martin/goqu/v9"
	_ "github.com/doug-martin/goqu/v9/dialect/mysql"
	"github.com/go-redis/redis/v8"
	"github.com/jmoiron/sqlx"
	"strings"
	"task/common"
	"task/contrib/conn"
	"task/contrib/helper"
	ryrpc "task/rpc"
	"time"
)

type MetaTable struct {
	WalletMode string
}

var (
	loc, _            = time.LoadLocation("America/Sao_Paulo")
	dialect           = g.Dialect("mysql")
	ctx               = context.Background()
	BetDb             *sqlx.DB
	MerchantRedis     *redis.Client
	colsMember        = helper.EnumFields(ryrpc.TblMemberBase{})
	colsGameConfig    = helper.EnumFields(ryrpc.TblGameConfig{})
	colsMemberBalance = helper.EnumFields(ryrpc.MemberBalance{})
	meta              *MetaTable
)

type callFunc func(path, flag string)

var routes = map[string]callFunc{
	"user":     userReport,         //代理报表
	"game":     gameReport,         //游戏报表
	"platform": platformReport,     //平台报表
	"score":    updateScore,        //更新流水积分
	"flow":     updateFlow,         //更新流水
	"agency":   agencyReport,       //邀请报表
	"deposit":  depositcount,       //更新存款次数中间表
	"invite":   updateAgencyRebate, //邀请奖励延迟发放
}

func Parse(conf common.Conf, args []string) {

	if !strings.Contains(args[4], "_") {
		fmt.Println("flag is error , ex : user_20220222_full")
		return
	}

	// 初始化db
	BetDb = conn.InitDB(conf.Db.Task, conf.Db.MaxIdleConn, conf.Db.MaxIdleConn)
	MerchantRedis = conn.InitRedis(conf.Redis.Addr[0], conf.Redis.Password, 0)
	//MerchantRedis = conn.InitRedisSentinel(conf.Redis.Addr, conf.Redis.Password, conf.Redis.Sentinel, conf.Redis.Db)
	ryrpc.Constructor(conf.RPC)
	// 初始化代理
	bean := conn.BeanNew(conf.Beanstalkd)
	common.InitBean(bean)
	common.New(conf.Proxy)
	common.InitRedis(MerchantRedis)
	ps := strings.Split(args[4], "_")
	if cb, ok := routes[ps[0]]; ok {
		cb(ps[1], ps[2])
	}
	defer func() {
		Close()
	}()
}

func Close() {
	BetDb.Close()
	MerchantRedis.Close()
}
func Constructor(mt *MetaTable) {
	meta = mt
}
