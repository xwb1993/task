package report

import (
	"fmt"
	"github.com/shopspring/decimal"
	"strconv"
	"strings"
	"task/common"
	"task/contrib/helper"
	"time"
)

type TBetReport struct {
	Id                  string  `json:"id" db:"id"`
	ReportTime          int64   `json:"report_time" db:"report_time"`
	ReportType          int64   `json:"report_type" db:"report_type"`
	ApiType             int64   `json:"api_type" db:"api_type"`
	Prefix              string  `json:"prefix" db:"prefix"`
	MemCount            int64   `json:"mem_count" db:"mem_count"`
	BetCount            int64   `json:"bet_count" db:"bet_count"`
	BetAmount           float64 `json:"bet_amount" db:"bet_amount"`
	ValidBetAmount      float64 `json:"valid_bet_amount" db:"valid_bet_amount"`
	CompanyNetAmount    float64 `json:"company_net_amount" db:"company_net_amount"`
	AvgBetAmount        float64 `json:"avg_bet_amount" db:"avg_bet_amount"`
	AvgValidBetAmount   float64 `json:"avg_valid_bet_amount" db:"avg_valid_bet_amount"`
	AvgCompanyNetAmount float64 `json:"avg_company_net_amount" db:"avg_company_net_amount"`
	Presettle           float64 `json:"presettle" db:"presettle"`
	ProfitRate          float64 `json:"profit_rate" db:"profit_rate"`
	RebateAmount        float64 `json:"rebate_amount" db:"rebate_amount"`
}

func gameReport(date, flag string) {

	if flag == "monthDay" {
		startMonDay := helper.MonthTST(time.Now().Unix(), loc).UnixMilli()
		nowDay := helper.DayTST(0, loc).UnixMilli()
		for startMonDay < nowDay {
			gameReportDay(startMonDay, startMonDay+86400*1000+999)
			startMonDay += 86400 * 1000
		}
	} else {
		endTime := helper.DayTET(0, loc).UnixMilli()
		startTime := helper.DayTST(0, loc).UnixMilli()
		gameReportDay(startTime, endTime+999)
	}
	//这个月的月报
	startMon := helper.MonthTST(time.Now().Unix(), loc).UnixMilli()
	endMon := helper.MonthTET(time.Now().Unix(), loc).UnixMilli()
	gameReportDay(startMon, endMon+1)

}

func gameReportDay(startTime int64, endTime int64) {

	var (
		settleList []TBetReport //游戏报表-结算时间-日报
		//apiSettleList []TBetReport //游戏报表-场馆结算时间-日报
	)

	st := startTime / 1000

	query := fmt.Sprintf(`select  api_type,COUNT( row_id ) as bet_count, count(distinct uid ) as mem_count, 
SUM(bet_amount) as bet_amount,SUM(rebate_amount) as rebate_amount,
SUM(valid_bet_amount) as valid_bet_amount, SUM( if ( presettle = 0, 0.0000 - net_amount, 0.0000 )) as company_net_amount, 
SUM( if ( presettle = 1, 0.0000 - net_amount, 0.0000 )) as presettle,
0.0 as profit_rate from tbl_game_record where ( flag = 1) 
and bet_time >= %d  and bet_time < %d and tester in(1,3) group by  api_type`, startTime, endTime)
	fmt.Println(query)
	err := BetDb.Select(&settleList, query)
	if err != nil {
		common.Log("gameReport", "error : %v", err)
		return
	}

	for i, v := range settleList {
		v.ReportTime = st
		v.ReportType = 2
		if v.MemCount > 0 {
			v.AvgBetAmount, _ = decimal.NewFromFloat(v.BetAmount).Div(decimal.NewFromInt(v.MemCount)).Float64()
			v.AvgValidBetAmount, _ = decimal.NewFromFloat(v.ValidBetAmount).Div(decimal.NewFromInt(v.MemCount)).Float64()
			v.AvgCompanyNetAmount, _ = decimal.Zero.Sub(decimal.NewFromFloat(v.CompanyNetAmount)).Div(decimal.NewFromInt(v.MemCount)).Float64()
		}
		if v.ValidBetAmount > 0 {
			v.ProfitRate, _ = (decimal.NewFromFloat(v.CompanyNetAmount)).Div(decimal.NewFromFloat(v.ValidBetAmount)).Float64()
		}
		v.Id = v.Prefix + strconv.FormatInt(v.ApiType, 10) + strconv.FormatInt(v.ReportTime, 10) + strconv.FormatInt(v.ReportType, 10)
		settleList[i] = v
	}

	ls := len(settleList)
	ps := 300
	if ls > 0 {
		p := ls / ps
		l := ls % ps
		for x := 0; x < p; x++ {
			offset := x * ps
			updateBet(settleList[offset : offset+ps])
		}

		if l > 0 {
			updateBet(settleList[p*ps:])
		}
	}

}

// updateBet
// @Description: 游戏报表 越南投注时间 更新
// @param list
func updateBet(data []TBetReport) {

	ids := ""
	for _, v := range data {
		ids += ","
		ids += "'" + v.Id + "'"
	}

	tx, err := BetDb.Begin()
	if err != nil {
		common.Log("tbl_report_game", "error : %v", err)
		return
	}

	query := fmt.Sprintf("delete from tbl_report_game where id in (%s)", strings.TrimPrefix(ids, ","))
	fmt.Println(query)
	_, err = tx.Exec(query)
	if err != nil {
		_ = tx.Rollback()
		common.Log("tbl_report_game", "error : %v", err)
		return
	}

	query, _, _ = dialect.From("tbl_report_game").Insert().Rows(data).ToSQL()
	fmt.Println(query)
	_, err = tx.Exec(query)
	if err != nil {
		_ = tx.Rollback()
		common.Log("tbl_report_game", "error : %v", err)
		return
	}

	_ = tx.Commit()
}
