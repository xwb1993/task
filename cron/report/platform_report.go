package report

import (
	"database/sql"
	"fmt"
	"github.com/shopspring/decimal"
	"strconv"
	"task/common"
	"task/contrib/helper"
	"time"
)

type TRegisterRecord struct {
	ReportTime  string `db:"report_time" json:"report_time"`
	RegistCount int64  `db:"regist_count" json:"regist_count"`
}

type TDepositsRecord struct {
	DepositNum    sql.NullInt64   `db:"deposit_num" json:"deposit_num"`
	DepositAmount sql.NullFloat64 `db:"deposit_amount" json:"deposit_amount"`
	DepositCount  sql.NullInt64   `db:"deposit_count" json:"deposit_count"`
}

type TWithdrawRecord struct {
	WithdrawNum    sql.NullInt64   `db:"withdraw_num" json:"withdraw_num"`
	WithdrawAmount sql.NullFloat64 `db:"withdraw_amount" json:"withdraw_amount"`
	WithdrawCount  sql.NullInt64   `db:"withdraw_count" json:"withdraw_count"`
}

type TGameRecord struct {
	BetMemCount    sql.NullInt64   `json:"bet_mem_count" db:"bet_mem_count"`
	BetNumCount    sql.NullInt64   `json:"bet_num_count" db:"bet_num_count"`
	BetAmount      sql.NullFloat64 `json:"bet_amount" db:"bet_amount"`
	ValidBetAmount sql.NullFloat64 `json:"valid_bet_amount" db:"valid_bet_amount"`
	NetAmount      sql.NullFloat64 `json:"net_amount" db:"net_amount"`
	Presettle      sql.NullFloat64 `json:"presettle" db:"presettle"`
}

type BalanceRecord struct {
	TotalBalance float64 `json:"total_balance" db:"total_balance"`
}

type TFirstDeposit struct {
	ReportTime         string  `json:"report_time" db:"report_time"`
	DepositCount       int64   `json:"deposit_count" db:"deposit_count"`
	FirstDepositAmount float64 `json:"first_deposit_amount" db:"first_deposit_amount"`
}
type TDepositInfo struct {
	ReportTime    string  `json:"report_time" db:"report_time"`
	DepositCount  int64   `json:"deposit_count" db:"deposit_count"`
	DepositAmount float64 `json:"deposit_amount" db:"deposit_amount"`
	Flags         int     `json:"flags" db:"flags"`
}

type TSecondDeposit struct {
	ReportTime          string  `json:"report_time" db:"report_time"`
	DepositCount        int64   `json:"deposit_count" db:"deposit_count"`
	SecondDepositAmount float64 `json:"second_deposit_amount" db:"second_deposit_amount"`
}

type TActiveData struct {
	ReportTime    string  `json:"report_time" db:"report_time"`
	Uid           string  `json:"uid" db:"uid"`
	DepositAmount float64 `json:"deposit_amount" db:"deposit_amount"`
	BetAmount     float64 `json:"bet_amount" db:"bet_amount"`
}

type TAgencyData struct {
	ReportTime   string  `json:"report_time" db:"report_time"`
	AgentAmount  float64 `json:"agent_amount" db:"agent_amount"`
	RebateAmount float64 `json:"rebate_amount" db:"rebate_amount"`
}

type PlatformKey struct {
	Id         string `json:"id" db:"id"`
	ReportTime int64  `json:"report_time" db:"report_time"`
}

type tblReportPlatform struct {
	Id                    string  `json:"id" db:"id"`
	ReportTime            int64   `json:"report_time" db:"report_time"`
	ReportType            int     `json:"report_type" db:"report_type"`
	ReportMonth           int64   `json:"report_month" db:"report_month"`
	RegistCount           int64   `json:"regist_count" db:"regist_count"`
	ActiveCount           int     `json:"active_count" db:"active_count"`
	EfficientActiveCount  int     `json:"efficient_active_count" db:"efficient_active_count"`
	FirstDepositCount     int64   `json:"first_deposit_count" db:"first_deposit_count"`
	SecondDepositCount    int64   `json:"second_deposit_count" db:"second_deposit_count"`
	ThirdDepositCount     int64   `json:"third_deposit_count" db:"third_deposit_count"`
	DepositCount          int64   `json:"deposit_count" db:"deposit_count"`
	WithdrawalCount       int64   `json:"withdrawal_count" db:"withdrawal_count"`
	ConversionRate        float64 `json:"conversion_rate" db:"conversion_rate"`
	FirstDepositAmount    float64 `json:"first_deposit_amount" db:"first_deposit_amount"`
	SecondDepositAmount   float64 `json:"second_deposit_amount" db:"second_deposit_amount"`
	ThirdDepositAmount    float64 `json:"third_deposit_amount" db:"third_deposit_amount"`
	AvgFirstDepositAmount float64 `json:"avg_first_deposit_amount" db:"avg_first_deposit_amount"`
	DepositMemCount       int64   `json:"deposit_mem_count" db:"deposit_mem_count"`
	WithdrawalMemCount    int64   `json:"withdrawal_mem_count" db:"withdrawal_mem_count"`
	DepositAmount         float64 `json:"deposit_amount" db:"deposit_amount"`
	WithdrawalAmount      float64 `json:"withdrawal_amount" db:"withdrawal_amount"`
	DepositWithdrawalSub  float64 `json:"deposit_withdrawal_sub" db:"deposit_withdrawal_sub"`
	DepositWithdrawalRate float64 `json:"deposit_withdrawal_rate" db:"deposit_withdrawal_rate"`
	BetMemCount           int64   `json:"bet_mem_count" db:"bet_mem_count"`
	BetNumCount           int64   `json:"bet_num_count" db:"bet_num_count"`
	BetAmount             float64 `json:"bet_amount" db:"bet_amount"`
	ValidBetAmount        float64 `json:"valid_bet_amount" db:"valid_bet_amount"`
	CompanyNetAmount      float64 `json:"company_net_amount" db:"company_net_amount"`
	AvgCompanyNetAmount   float64 `json:"avg_company_net_amount" db:"avg_company_net_amount"`
	ProfitAmount          float64 `json:"profit_amount" db:"profit_amount"`
	Presettle             float64 `json:"presettle" db:"presettle"`
	CompanyRevenue        float64 `json:"company_revenue" db:"company_revenue"`
	Uids                  string  `json:"uids" db:"uids"`
}

type AgencyRebateData struct {
	RebateAmount sql.NullFloat64 `json:"rebate_amount" db:"rebate_amount"`
}

// 统计平台报表
func platformReport(date, flag string) {

	now := helper.DayTST(0, loc)
	endTime := now.Unix()
	if flag == "update" {
		platformReportJob(endTime, time.Now().Unix(), flag)
	} else if flag == "monthDay" {
		startMonDay := helper.MonthTST(time.Now().Unix(), loc).Unix()
		for startMonDay < endTime {
			platformReportJob(startMonDay, startMonDay+86400, "update")
			startMonDay += 86400
		}
	}

}

func platformReportJob(startTime, endTime int64, flag string) {

	var (
		registerPList          sql.NullInt64
		depositPList           TDepositsRecord
		withdrawPList          TWithdrawRecord
		settlePList            TGameRecord
		depositCountPList      []TDepositInfo
		betMemlist             []TActiveData
		depositMemlist         []TActiveData
		activeNumPMap          = map[string]map[string]int{}
		efficientActiveNumPMap = map[string]map[string]int{}
		firstDPMap             = map[string]TDepositInfo{}
	)
	timeNow := time.Unix(startTime, 0)                    //2017-08-30 16:19:19 +0800 CST
	startTimeStr := timeNow.Format("2006-01-02 15:04:05") //2015-06-15 08:52:32
	//fmt.Println(startTimeStr)
	sqlTester := " and tester in(1,3)"
	//注册数
	query := fmt.Sprintf(`SELECT  count( uid ) regist_count FROM tbl_member_base  where 
created_at >= %d and created_at < %d %s `, startTime, endTime, sqlTester)
	err := BetDb.Get(&registerPList, query)
	if err != nil {
		common.Log("platformReport", "error : %v", err)
		return
	}

	//存人数 存金额
	query = fmt.Sprintf(`SELECT 
COUNT(DISTINCT uid) AS deposit_num,count(id) as deposit_count, sum( amount ) AS deposit_amount FROM tbl_deposit WHERE state = 362 
and confirm_at >= %d and confirm_at<%d %s `, startTime, endTime, sqlTester)
	err = BetDb.Get(&depositPList, query)
	if err != nil {
		common.Log("platformReport", "error : %v", err)
		return
	}

	//提现人数 提现金额
	query = fmt.Sprintf(`SELECT 
COUNT(DISTINCT uid) AS withdraw_num, sum( amount ) AS withdraw_amount,count(id) as withdraw_count FROM tbl_withdraw WHERE state = 374 
and withdraw_at >= %d and withdraw_at<%d %s`, startTime, endTime, sqlTester)
	err = BetDb.Get(&withdrawPList, query)
	if err != nil {
		common.Log("platformReport", "error : %v", err)
		return
	}

	query = fmt.Sprintf(`SELECT COUNT(DISTINCT uid) AS bet_mem_count,count(1) as bet_num_count, sum( bet_amount ) AS bet_amount, 
sum( valid_bet_amount ) AS valid_bet_amount, sum( IF ( presettle = 0, 0.0000- net_amount, 0.0000 )) net_amount, 
sum( IF ( presettle = 1, 0.0000- net_amount, 0.0000 )) presettle, count(distinct uid) as bet_mem_count FROM tbl_game_record WHERE flag < 2
and bet_time >= %d and bet_time <%d %s`, startTime*1000, endTime*1000, sqlTester)
	err = BetDb.Get(&settlePList, query)
	if err != nil {
		common.Log("platformReport", "error : %v", err)
		return
	}

	query = fmt.Sprintf(`select sum(deposit_amount) as deposit_amount,count(distinct uid) as deposit_count,flags from tbl_member_deposit_info 
	where deposit_at >= %d and deposit_at < %d and flags >0 and flags < 4 group by flags`, startTime, endTime)
	err = BetDb.Select(&depositCountPList, query)
	if err != nil {
		common.Log("platformReport", "error : %v", err)
		return
	}

	//活跃人数
	query = fmt.Sprintf(`SELECT uid, sum( amount ) AS deposit_amount, 0 AS bet_amount FROM tbl_deposit
WHERE state = 362 and amount>0 and confirm_at >= %d and confirm_at < %d %s GROUP BY  uid`, startTime, endTime, sqlTester)
	err = BetDb.Select(&depositMemlist, query)
	if err != nil {
		common.Log("platformReport", "error : %v", err)
		return
	}
	query = fmt.Sprintf(`SELECT uid, 0 deposit_amount, sum( bet_amount ) bet_amount FROM tbl_game_record 
WHERE flag < 2 and bet_amount>0 and bet_time >=%d and bet_time <%d %s GROUP BY uid`, startTime*1000, endTime*1000+999, sqlTester)
	err = BetDb.Select(&betMemlist, query)
	if err != nil {
		common.Log("platformReport", "error : %v", err)
		return
	}
	for _, v := range betMemlist {
		key := startTimeStr
		_, ok := activeNumPMap[key]
		if ok {
			_, ok2 := activeNumPMap[key][v.Uid]
			if !ok2 {
				activeNumPMap[key][v.Uid] = 1
			}
		} else {
			uidMap := map[string]int{}
			uidMap[v.Uid] = 1
			activeNumPMap[key] = uidMap
		}
		if v.BetAmount > 0 {
			_, ok := efficientActiveNumPMap[key]
			if ok {
				_, ok2 := efficientActiveNumPMap[key][v.Uid]
				if !ok2 {
					efficientActiveNumPMap[key][v.Uid] = 1
				}
			} else {
				uidMap := map[string]int{}
				uidMap[v.Uid] = 1
				efficientActiveNumPMap[key] = uidMap
			}
		}
	}
	for _, v := range depositMemlist {
		key := startTimeStr
		_, ok := activeNumPMap[key]
		if ok {
			_, ok2 := activeNumPMap[key][v.Uid]
			if !ok2 {
				activeNumPMap[key][v.Uid] = 1
			}
		} else {
			uidMap := map[string]int{}
			uidMap[v.Uid] = 1
			activeNumPMap[key] = uidMap
		}
		if v.DepositAmount > 0 {
			_, ok := efficientActiveNumPMap[key]
			if ok {
				_, ok2 := efficientActiveNumPMap[key][v.Uid]
				if !ok2 {
					efficientActiveNumPMap[key][v.Uid] = 1
				}
			} else {
				uidMap := map[string]int{}
				uidMap[v.Uid] = 1
				efficientActiveNumPMap[key] = uidMap
			}
		}
	}

	for _, v := range depositCountPList {
		v.ReportTime = startTimeStr
		key := fmt.Sprintf(`%s|%d`, startTimeStr, v.Flags)
		firstDPMap[key] = v
	}

	//更新平台报表数据

	k := startTimeStr
	obj := tblReportPlatform{
		Id:                   k,
		ReportTime:           startTime,
		ReportMonth:          helper.MonthTST(startTime, loc).Unix(),
		BetMemCount:          settlePList.BetMemCount.Int64,
		BetNumCount:          settlePList.BetNumCount.Int64,
		Presettle:            settlePList.Presettle.Float64,
		ValidBetAmount:       settlePList.ValidBetAmount.Float64,
		EfficientActiveCount: len(efficientActiveNumPMap[k]),
		BetAmount:            settlePList.BetAmount.Float64,
		ProfitAmount:         settlePList.BetAmount.Float64,
		CompanyNetAmount:     settlePList.NetAmount.Float64,
		RegistCount:          registerPList.Int64,
		ActiveCount:          len(activeNumPMap[k]),
		DepositAmount:        depositPList.DepositAmount.Float64,
		DepositCount:         depositPList.DepositCount.Int64,
		DepositMemCount:      depositPList.DepositNum.Int64,
		WithdrawalMemCount:   withdrawPList.WithdrawNum.Int64,
		WithdrawalAmount:     withdrawPList.WithdrawAmount.Float64,
		WithdrawalCount:      withdrawPList.WithdrawCount.Int64,
		FirstDepositCount:    firstDPMap[startTimeStr+"|1"].DepositCount,
		SecondDepositCount:   firstDPMap[startTimeStr+"|2"].DepositCount,
		ThirdDepositCount:    firstDPMap[startTimeStr+"|3"].DepositCount,
	}

	obj.DepositWithdrawalSub, _ = decimal.NewFromFloat(depositPList.DepositAmount.Float64).Sub(decimal.NewFromFloat(withdrawPList.WithdrawAmount.Float64)).Truncate(8).Float64()
	if depositPList.DepositAmount.Float64 != 0 {
		obj.DepositWithdrawalRate, _ = decimal.NewFromFloat(withdrawPList.WithdrawAmount.Float64).Div(decimal.NewFromFloat(depositPList.DepositAmount.Float64)).Truncate(8).Float64()
	}
	obj.CompanyRevenue, _ = decimal.NewFromFloat(settlePList.NetAmount.Float64).Add(decimal.NewFromFloat(settlePList.Presettle.Float64)).Truncate(8).Float64()
	if obj.ValidBetAmount != 0 {
		obj.ProfitAmount, _ = decimal.NewFromFloat(settlePList.NetAmount.Float64).Add(decimal.NewFromFloat(settlePList.Presettle.Float64)).Truncate(8).Div(
			decimal.NewFromFloat(settlePList.ValidBetAmount.Float64)).Mul(decimal.NewFromInt(100)).Truncate(8).Float64()
	}
	//转化率
	if obj.RegistCount != 0 {
		obj.ConversionRate, _ = decimal.NewFromInt(obj.FirstDepositCount).Div(decimal.NewFromInt(obj.RegistCount)).Truncate(8).Float64()
	} else {
		obj.ConversionRate = 0
	}
	obj.FirstDepositAmount = firstDPMap[startTimeStr+"|1"].DepositAmount
	obj.SecondDepositAmount = firstDPMap[startTimeStr+"|2"].DepositAmount
	obj.ThirdDepositAmount = firstDPMap[startTimeStr+"|3"].DepositAmount
	//平均首存比
	if obj.FirstDepositCount != 0 {
		obj.AvgFirstDepositAmount, _ = decimal.NewFromFloat(obj.FirstDepositAmount).Div(decimal.NewFromInt(obj.FirstDepositCount)).Truncate(8).Float64()
	} else {
		obj.AvgFirstDepositAmount = 0
	}
	//公司平均输赢
	if obj.BetMemCount > 0 {
		obj.AvgCompanyNetAmount, _ = decimal.NewFromFloat(obj.CompanyNetAmount).Div(decimal.NewFromInt(obj.BetMemCount)).Truncate(8).Float64()
	} else {
		obj.AvgCompanyNetAmount = 0
	}

	obj.ReportType = 2

	updatePlatform(obj)

}

func updatePlatform(v tblReportPlatform) {

	v.Id = v.Id + "|" + strconv.Itoa(v.ReportType)

	tx, err := BetDb.Begin()
	if err != nil {
		common.Log("tbl_report_platform", "error : %v", err)
		return
	}

	query := fmt.Sprintf("delete from tbl_report_platform where id = '%s'", v.Id)
	fmt.Println(query)
	_, err = tx.Exec(query)
	if err != nil {
		_ = tx.Rollback()
		common.Log("tbl_report_platform", "error : %v", err)
		return
	}

	query, _, _ = dialect.From("tbl_report_platform").Insert().Rows(v).ToSQL()
	fmt.Println(query)
	_, err = tx.Exec(query)
	if err != nil {
		_ = tx.Rollback()
		common.Log("tbl_report_platform", "error : %v", err)
		return
	}

	_ = tx.Commit()
}
