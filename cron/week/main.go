package week

import (
	"context"
	"database/sql"
	"errors"
	"fmt"
	g "github.com/doug-martin/goqu/v9"
	_ "github.com/doug-martin/goqu/v9/dialect/mysql"
	"github.com/doug-martin/goqu/v9/exp"
	"github.com/go-redis/redis/v8"
	"github.com/jmoiron/sqlx"
	"github.com/shopspring/decimal"
	"strings"
	"task/common"
	"task/contrib/conn"
	"task/contrib/helper"
	"time"
)

type MetaTable struct {
	WalletMode string
}

var (
	loc, _                 = time.LoadLocation("America/Sao_Paulo")
	cli                    *redis.Client
	db                     *sqlx.DB
	ctx                    = context.Background()
	dialect                = g.Dialect("mysql")
	colsPromoWeeklyConfig  = helper.EnumFields(TblPromoWeekBetConfig{})
	colsPromoWeekbetRecord = helper.EnumFields(TblPromoWeekbetRecord{})
	configs                []TblPromoWeekBetConfig
	meta                   *MetaTable
)

type validData struct {
	Uid            string  `db:"uid"`
	ValidBetAmount float64 `db:"valid_bet_amount"`
}

// 账变表
type MemberTransaction struct {
	AfterAmount  string `db:"after_amount" json:"after_amount" cbor:"after_amount"`    //账变后的金额
	Amount       string `db:"amount" json:"amount" cbor:"amount"`                      //用户填写的转换金额
	BeforeAmount string `db:"before_amount" json:"before_amount" cbor:"before_amount"` //账变前的金额
	BillNo       string `db:"bill_no" json:"bill_no" cbor:"bill_no"`                   //转账|充值|提现ID
	CashType     int    `db:"cash_type" json:"cash_type" cbor:"cash_type"`             //0:转入1:转出2:转入失败补回3:转出失败扣除4:存款5:提现
	CreatedAt    int64  `db:"created_at" json:"created_at" cbor:"created_at"`          //
	ID           string `db:"id" json:"id" cbor:"id"`                                  //
	UID          string `db:"uid" json:"uid" cbor:"uid"`                               //用户ID
	Username     string `db:"username" json:"username" cbor:"username"`                //用户名
	Remark       string `db:"remark" json:"remark" cbor:"remark"`                      //备注
}

type TblPromoWeekbetRecord struct {
	Id              string  `json:"id" db:"id"`
	Uid             string  `json:"uid" db:"uid"`
	ReportTime      int64   `json:"report_time" db:"report_time"`
	ValidBetAmount  float64 `json:"valid_bet_amount" db:"valid_bet_amount"`
	WaitBonusAmount float64 `json:"wait_bonus_amount" db:"wait_bonus_amount"`
	BonusAmount     float64 `json:"bonus_amount" db:"bonus_amount"`
	State           int     `json:"state" db:"state"`
	UpdatedAt       int64   `json:"updated_at" db:"updated_at"`
	Remark          string  `json:"remark" db:"remark"`
	PayAt           int64   `json:"pay_at" db:"pay_at"`
}

type TblPromoWeekBetConfig struct {
	Id          string  `json:"id" db:"id" cbor:"id"`
	FlowAmount  float64 `json:"flow_amount" db:"flow_amount" cbor:"flow_amount"`
	BonusAmount float64 `json:"bonus_amount" db:"bonus_amount" cbor:"bonus_amount"`
	UpdatedAt   int64   `json:"updated_at" db:"updated_at" cbor:"updated_at"`
	UpdatedName string  `json:"updated_name" db:"updated_name" cbor:"updated_name"`
}

func Parse(conf common.Conf, args []string) {

	cli = conn.InitRedis(conf.Redis.Addr[0], conf.Redis.Password, 0)
	//cli = conn.InitRedisSentinel(conf.Redis.Addr, conf.Redis.Password, conf.Redis.Sentinel, conf.Redis.Db)
	db = conn.InitDB(conf.Db.Task, conf.Db.MaxIdleConn, conf.Db.MaxIdleConn)
	bean := conn.BeanNew(conf.Beanstalkd)
	common.InitBean(bean)
	common.InitRedis(cli)
	common.InitDb(db)

	fmt.Println("args[4]:", args[4])
	switch args[4] {
	case "week": //本周的待结算周投注活动
		weekStartAt := helper.WeekTST(0, loc).UnixMilli()
		weekEndAt := helper.WeekTET(0, loc).UnixMilli()

		weekGameRecord(weekStartAt, weekEndAt)
	case "lastWeek": //上周的待结算周投注活动
		weekStartAt := helper.WeekTST(helper.WeekTST(0, loc).Unix()-1, loc).UnixMilli()
		weekEndAt := helper.WeekTET(helper.WeekTST(0, loc).Unix()-1, loc).UnixMilli()
		weekGameRecord(weekStartAt, weekEndAt)
	case "weekTransaction": //发放上周待结算投注活动
		weekStartAt := helper.WeekTST(helper.WeekTST(0, loc).Unix()-1, loc).Unix()
		weekTransaction(weekStartAt, "")
	default: //发放本周的周投注奖金
		weekStartAt := helper.WeekTST(0, loc).Unix()
		weekTransaction(weekStartAt, args[4])
	}
}

// 将注单从数据库取出，写入redis list
func weekGameRecord(weekStartAt, weekEndAt int64) {

	var (
		data  []validData
		count uint
	)

	query, _, _ := dialect.From("tbl_promo_weekbet_config").Select(colsPromoWeeklyConfig...).Order(g.C("flow_amount").Desc()).ToSQL()
	err := db.Select(&configs, query)
	if err != nil && !errors.Is(err, sql.ErrNoRows) {
		return
	}

	ex := g.Ex{
		"flag":     1,
		"bet_time": g.Op{"between": exp.NewRangeVal(weekStartAt, weekEndAt)},
	}
	query, _, _ = dialect.From("tbl_game_record").Select(g.COUNT(g.DISTINCT("uid"))).Where(ex).Order(g.C("uid").Desc()).ToSQL()
	err = db.Get(&count, query)
	if err != nil {
		return
	}

	if count == 0 {
		return
	}

	p := count / 100
	l := count % 100
	if l > 0 {
		p += 1
	}

	for j := uint(1); j <= p; j++ {
		offset := (p - 1) * 100
		query, _, _ = dialect.From("tbl_game_record").
			Select(g.C("uid"), g.SUM("valid_bet_amount").As("valid_bet_amount")).Where(ex).GroupBy(g.C("uid")).Offset(offset).Limit(100).Order(g.C("uid").Desc()).ToSQL()
		fmt.Println(query)
		err = db.Select(&data, query)
		if err != nil {
			common.Log("week", "error : %v", err)
			return
		}
		weekGenRecord(weekStartAt/1000, data, configs)
	}

	data = nil
	configs = nil
}

// 生成记录
func weekGenRecord(reportTime int64, userList []validData, configs []TblPromoWeekBetConfig) {

	if len(userList) == 0 || len(configs) == 0 {
		return
	}
	fmt.Println("configs:", configs)
	sstr := helper.DayTST(reportTime, loc).Format("2006-01-02")
	estr := helper.WeekTET(reportTime, loc).Format("2006-01-02")
	var (
		insertList []TblPromoWeekbetRecord
		updateList []TblPromoWeekbetRecord
	)

	for _, v := range userList {
		var record TblPromoWeekbetRecord
		var ff decimal.Decimal //流水要求
		var ba decimal.Decimal //奖金
		//查之前是否有记录了
		ex := g.Ex{
			"uid":         v.Uid,
			"report_time": reportTime,
		}
		query, _, _ := dialect.From("tbl_promo_weekbet_record").Select(colsPromoWeekbetRecord...).Where(ex).Limit(1).ToSQL()
		fmt.Println(query)
		err := db.Get(&record, query)
		if err != nil && !errors.Is(err, sql.ErrNoRows) {
			return
		}
		if !errors.Is(err, sql.ErrNoRows) && record.State == 2 {
			return
		}
		if errors.Is(err, sql.ErrNoRows) {
			record = TblPromoWeekbetRecord{}
			record.Id = ""
		}
		va := decimal.NewFromFloat(v.ValidBetAmount)

		for _, cf := range configs {
			fa := decimal.NewFromFloat(cf.FlowAmount)
			if va.GreaterThan(fa) {
				ff = fa
				ba = decimal.NewFromFloat(cf.BonusAmount)
				break
			}
		}
		record.WaitBonusAmount, _ = ba.Float64()

		if ff.Equals(decimal.Zero) {
			fmt.Println("没有符合的流水")
			ff = decimal.NewFromFloat(configs[len(configs)-1].FlowAmount)
			ba = decimal.NewFromFloat(configs[len(configs)-1].BonusAmount)
		}
		record.ValidBetAmount = v.ValidBetAmount
		record.Uid = v.Uid
		record.BonusAmount = 0
		record.UpdatedAt = time.Now().Unix()
		record.State = 1
		record.ReportTime = reportTime
		record.Remark = fmt.Sprintf(`统计周期:[%s~%s],当时的配置:流水要求[%s],奖金[%s]`, sstr, estr, ff.StringFixed(2), ba.StringFixed(2))
		if record.Id == "" {
			record.Id = helper.GenId()
			insertList = append(insertList, record)
		} else {
			updateList = append(updateList, record)
		}
	}

	if len(insertList) > 0 {
		insertRecord(insertList)
	}
	if len(updateList) > 0 {
		updateRecord(updateList)
	}
}

func insertRecord(data []TblPromoWeekbetRecord) {

	query, _, _ := dialect.From("tbl_promo_weekbet_record").Insert().Rows(data).ToSQL()
	fmt.Println(query)
	_, err := db.Exec(query)
	if err != nil {
		fmt.Println(err)
		common.Log("week", "error : %v", err)
		return
	}
}

func updateRecord(data []TblPromoWeekbetRecord) {

	ids := ""
	for _, v := range data {
		ids += ","
		ids += "'" + v.Id + "'"
	}

	tx, err := db.Begin()
	if err != nil {
		common.Log("week", "error : %v", err)
		return
	}

	query := fmt.Sprintf("delete from tbl_promo_weekbet_record where id in (%s)", strings.TrimPrefix(ids, ","))
	//fmt.Println(query)
	_, err = tx.Exec(query)
	if err != nil {
		_ = tx.Rollback()
		common.Log("week", "error : %v", err)
		return
	}

	query, _, _ = dialect.From("tbl_promo_weekbet_record").Insert().Rows(data).ToSQL()
	//fmt.Println(query)
	_, err = tx.Exec(query)
	if err != nil {
		_ = tx.Rollback()
		common.Log("week", "error : %v", err)
		return
	}

	_ = tx.Commit()
}

// 写入帐变
func weekTransaction(reportTime int64, uid string) {

	var list []TblPromoWeekbetRecord
	ex := g.Ex{
		"report_time": reportTime,
		"state":       1,
	}

	if uid != "" {
		ex["uid"] = uid
	}
	query, _, _ := dialect.From("tbl_promo_weekbet_record").Select(colsPromoWeekbetRecord...).Where(ex).ToSQL()
	fmt.Println(query)
	err := db.Select(&list, query)
	if err != nil && !errors.Is(err, sql.ErrNoRows) {
		return
	}

	for _, v := range list {

		money := decimal.NewFromFloat(v.WaitBonusAmount)
		if money.LessThanOrEqual(decimal.Zero) {
			//record := g.Record{
			//	"state":  3,
			//	"pay_at": time.Now().Unix(),
			//}
			//query, _, _ = dialect.Update("tbl_promo_weekbet_record").Set(record).Where(g.Ex{"id": v.Id}).ToSQL()
			//// 2、更新订单状态
			//_, err = db.Exec(query)
			//if err != nil {
			//	return
			//}
			continue
		}
		// 1、查询用户额度
		brl, err := common.MemberBalance(v.Uid)
		if err != nil {
			return
		}
		member, err := common.MemberFindOneByUid(v.Uid)
		if err != nil {
			return
		}
		//禁止发放奖金
		if member.CanBonus == 2 {
			continue
		}
		balanceAfter := brl.Add(money)
		id := helper.GenId()

		// 开启事务
		tx, err := db.Begin()
		if err != nil {
			return
		}

		record := g.Record{
			"state":             2,
			"pay_at":            time.Now().Unix(),
			"wait_bonus_amount": 0,
			"bonus_amount":      v.WaitBonusAmount,
		}
		query, _, _ = dialect.Update("tbl_promo_weekbet_record").Set(record).Where(g.Ex{"id": v.Id}).ToSQL()
		fmt.Println(query)
		// 2、更新订单状态
		_, err = tx.Exec(query)
		if err != nil {
			_ = tx.Rollback()
			return
		}

		// 3、更新余额
		ex = g.Ex{
			"uid": v.Uid,
		}

		var br g.Record
		if meta.WalletMode == "1" {
			br = g.Record{
				"brl":                 g.L(fmt.Sprintf("brl+%s", money.String())),
				"deposit_lock_amount": g.L(fmt.Sprintf("deposit_lock_amount+%s", money.String())),
			}
		} else if meta.WalletMode == "2" {
			br = g.Record{
				"brl":           g.L(fmt.Sprintf("brl+%s", money.String())),
				"unlock_amount": g.L(fmt.Sprintf("unlock_amount+%s", money.String())),
			}
		}
		query, _, _ = dialect.Update("tbl_member_balance").Set(br).Where(ex).ToSQL()
		fmt.Println(query)
		_, err = tx.Exec(query)
		if err != nil {
			_ = tx.Rollback()
			return
		}

		// 4、新增账变记录
		mbTrans := MemberTransaction{
			AfterAmount:  balanceAfter.String(),
			Amount:       money.StringFixed(2),
			BeforeAmount: brl.StringFixed(2),
			BillNo:       v.Id,
			CreatedAt:    time.Now().UnixMilli(),
			ID:           id,
			CashType:     helper.TransactionWeeklyDividend,
			UID:          v.Uid,
			Username:     member.Username,
			Remark:       "week bet bonus",
		}

		query, _, _ = dialect.Insert("tbl_balance_transaction").Rows(mbTrans).ToSQL()
		fmt.Println(query)
		_, err = tx.Exec(query)
		if err != nil {
			_ = tx.Rollback()
			return
		}

		err = tx.Commit()
		if err != nil {
			return
		}

	}

}
