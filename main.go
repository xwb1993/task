package main

import (
	"embed"
	_ "embed"
	"fmt"
	"github.com/BurntSushi/toml"
	_ "go.uber.org/automaxprocs"
	"os"
	"strings"
	"task/common"
	"task/contrib/apollo"
	"task/cron/rebate"
	"task/cron/report"
	"task/cron/upgrade"
	"task/cron/week"
	"task/modules/reg"
	"task/modules/zincbeanstalk"
	"task/pkg"
	"task/pull"
)

// 后台任务
type fn func(common.Conf, []string)

//go:embed platform.toml
var configData embed.FS
var (
	cb = map[string]fn{
		"zincbeanstalk": zincbeanstalk.Parse, //从redis读取list队列,写到zinc
		"reg":           reg.Parse,           //从redis读取list队列,写到zinc
	}
	cronCb = map[string]fn{
		"report":  report.Parse,  //报表定时任务
		"upgrade": upgrade.Parse, //升级
		"rebate":  rebate.Parse,  //返水
		"week":    week.Parse,    //周投注活动
	}
)

func main() {

	argc := len(os.Args)
	if argc != 5 {
		fmt.Printf("%s <etcds> <cfgPath> []\n", os.Args[0])
		return
	}
	endpoints := strings.Split(os.Args[1], ",")

	cfg := common.Conf{}
	apollo.New(endpoints, pkg.ETCDName, pkg.ETCDPass)
	if _, err := toml.DecodeFile(os.Args[2], &cfg); err != nil {
		fmt.Printf("ParseTomlStruct error: %s", err.Error())
		return
	}
	apollo.Close()
	//cfg := common.ConfParse(endpoints, pkg.ETCDName, pkg.ETCDPass, os.Args[2])
	fmt.Printf("gitReversion = %s\r\nbuildGoVersion = %s\r\nbuildTime = %s\r\n", pkg.GitReversion, pkg.BuildGoVersion, pkg.BuildTime)

	mt := new(report.MetaTable)
	mt.WalletMode = cfg.WalletMode
	report.Constructor(mt)
	// 拉单
	config := common.PlatCfg_t{}
	if os.Args[3] == "pull" {
		// 读取嵌入在二进制文件中的配置文件
		data, err := configData.ReadFile("platform.toml")
		if err != nil {
			return
		}

		// 解析配置文件
		if _, err := toml.Decode(string(data), &config); err != nil {
			return
		}
		apollo.New(endpoints, pkg.ETCDName, pkg.ETCDPass)
		apollo.Close()
		//platCfg := common.ConfPlatParse(endpoints, pkg.ETCDName, pkg.ETCDPass, "/platform.toml")
		pull.Parse(cfg, config, os.Args[4])
	}

	// 定时任务
	if val, ok := cronCb[os.Args[3]]; ok {
		val(cfg, os.Args)
	}

	if val, ok := cb[os.Args[3]]; ok {
		val(cfg, os.Args)
	}

	fmt.Println(os.Args[3], "done")
}
