package pkg

type Conf struct {
	Prefix         string   `toml:"prefix"`
	Dev            bool     `toml:"dev"`
	Sock5          string   `toml:"sock5"`
	RPC            string   `toml:"rpc"`
	Ipdb           string   `toml:"ipdb"`
	Beanstalkd     string   `toml:"beanstalkd"`
	Rocketmq       []string `toml:"rocketmq"`
	Callback       string   `toml:"callback"`
	CallbackDomain string   `toml:"callback_domain"`
	WebURL         string   `toml:"web_url"`
	Contate        string   `toml:"contate"`
	Db             struct {
		Member      string `toml:"member"`
		Admin       string `toml:"admin"`
		Game        string `toml:"game"`
		Task        string `toml:"task"`
		MaxIdleConn int    `toml:"max_idle_conn"`
		MaxOpenConn int    `toml:"max_open_conn"`
	} `toml:"db"`
	Port struct {
		Member string `toml:"member"`
		Admin  string `toml:"admin"`
		Game   string `toml:"game"`
		RPC    string `toml:"rpc"`
	} `toml:"port"`
	Redis struct {
		Addr     []string `toml:"addr"`
		Password string   `toml:"password"`
		Sentinel string   `toml:"sentinel"`
		Db       int      `toml:"db"`
	} `toml:"redis"`
	Aws struct {
		Bucket          string `toml:"bucket"`
		AccessKeyID     string `toml:"accessKeyID"`
		SecretAccessKey string `toml:"secretAccessKey"`
	} `toml:"aws"`
	Email struct {
		URL      string `toml:"url"`
		Port     string `toml:"port"`
		Username string `toml:"username"`
		Password string `toml:"password"`
	} `toml:"email"`
	AwsEmail struct {
		AccessKeyID     string `toml:"accessKeyID"`
		SecretAccessKey string `toml:"secretAccessKey"`
		Region          string `toml:"region"`
		From            string `toml:"from"`
	} `toml:"awsemail"`
	Meilisearch struct {
		Host string `toml:"host"`
		Key  string `toml:"key"`
	} `toml:"meilisearch"`
	Zinc struct {
		URL      string `toml:"url"`
		Username string `toml:"username"`
		Password string `toml:"password"`
	} `toml:"zinc"`
	Tgpay struct {
		AppKey string `toml:"appKey"`
	} `toml:"tgpay"`
}
